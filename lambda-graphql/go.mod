module bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend

go 1.17

require (
	github.com/99designs/gqlgen v0.14.0
	github.com/aws/aws-lambda-go v1.23.0
	github.com/aws/aws-sdk-go-v2 v1.9.2
	github.com/aws/aws-sdk-go-v2/config v1.1.5
	github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue v1.0.6
	github.com/aws/aws-sdk-go-v2/internal/ini v1.1.1 // indirect
	github.com/aws/aws-sdk-go-v2/service/dynamodb v1.2.2
	github.com/aws/aws-sdk-go-v2/service/s3 v1.14.0
	github.com/aws/aws-sdk-go-v2/service/wafv2 v1.10.1
	github.com/awslabs/aws-lambda-go-api-proxy v0.10.0
	github.com/bearbin/go-age v0.0.0-20210220235509-f0fa00c278ce
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/gosimple/slug v1.10.0
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/jinzhu/now v1.1.2
	github.com/kr/pretty v0.1.0
	github.com/mailgun/mailgun-go/v4 v4.5.1
	github.com/matoous/go-nanoid/v2 v2.0.0
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/vektah/gqlparser/v2 v2.2.0
	github.com/vincent-petithory/dataurl v0.0.0-20191104211930-d1553a71de50
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
	golang.org/x/net v0.0.0-20210414194228-064579744ee0 // indirect
)

require (
	github.com/agnivade/levenshtein v1.1.0 // indirect
	github.com/aws/aws-sdk-go-v2/credentials v1.1.5 // indirect
	github.com/aws/aws-sdk-go-v2/feature/ec2/imds v1.0.6 // indirect
	github.com/aws/aws-sdk-go-v2/service/dynamodbstreams v1.1.5 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/accept-encoding v1.3.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/presigned-url v1.3.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/s3shared v1.6.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/sso v1.1.5 // indirect
	github.com/aws/aws-sdk-go-v2/service/sts v1.2.2 // indirect
	github.com/aws/smithy-go v1.8.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/gosimple/unidecode v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/kr/text v0.1.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	golang.org/x/sys v0.0.0-20210330210617-4fbd30eecc44 // indirect
)
