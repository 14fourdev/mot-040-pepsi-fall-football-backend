package main

import (
	"context"
	"errors"
	"net"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/generated"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/infrastructure"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/infrastructure/repository"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/admin"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/auth"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/banlist"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/consumer"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/dapper"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/entry"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/mailer"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/metrics"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/recaptcha"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/recipe"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/requester"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/salesforce"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/southernMade"
	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/awslabs/aws-lambda-go-api-proxy/handlerfunc"
	"github.com/dgrijalva/jwt-go"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/sirupsen/logrus"
	"github.com/vektah/gqlparser/v2/gqlerror"
)

var adapter *handlerfunc.HandlerFuncAdapterV2

func main() {
	lambda.Start(Handler)
}

func Handler(ctx context.Context, req events.APIGatewayV2HTTPRequest) (events.APIGatewayV2HTTPResponse, error) {
	logrus.Printf("Processing Lambda request %s\n", req.RequestContext.RequestID)

	if req.Headers["cloudfront-viewer-country"] != "US" {
		return events.APIGatewayV2HTTPResponse{
			StatusCode: 403,
		}, nil
	}

	ENV := req.StageVariables
	db := infrastructure.NewDynamodbClient(ctx, ENV)
	s3 := infrastructure.NewS3Client(ctx, ENV)
	loc, _ := time.LoadLocation(ENV["TIMEZONE"])

	adminRepo := repository.NewAdminDynamodb(db, ENV["DYNAMODB_TABLE"])
	adminService := admin.NewService(adminRepo, loc)

	authRepo := repository.NewAuthDynamodb(db, ENV["DYNAMODB_TABLE"])
	authService := auth.NewService(authRepo, loc, ENV["JWT_SECRET"])

	banlistRepo := repository.NewBanlistDynamodb(db, ENV["DYNAMODB_TABLE"])
	banlistService := banlist.NewService(banlistRepo, loc)

	consumerRepo := repository.NewConsumerDynamodb(db, ENV["DYNAMODB_TABLE"])
	consumerService := consumer.NewService(consumerRepo)

	dapperRepo := repository.NewDapperDynamodb(db, ENV["DYNAMODB_TABLE"])
	dapperService := dapper.NewService(dapperRepo)

	entryRepo := repository.NewEntryDynamodb(db, ENV["DYNAMODB_TABLE"])
	entryService := entry.NewService(entryRepo)

	var mailerService *mailer.Service
	if ENV["MAILER_PROVIDER"] == "mailgun" {
		mailerRepo := repository.NewMailerMailgun(ENV["MAILGUN_DOMAIN"], ENV["MAILGUN_API_KEY"], ENV["MAILER_BASE_URL"], ENV["MAILER_FROM_NAME"], ENV["MAILER_FROM_EMAIL"], ENV["MAILGUN_VALIDATION_KEY"])
		mailerService = mailer.NewService(mailerRepo, ENV["MAILER_BASE_URL"])
	} else {
		mailerRepo := repository.NewMailerLogger(ENV["MAILER_BASE_URL"])
		mailerService = mailer.NewService(mailerRepo, ENV["MAILER_BASE_URL"])
	}

	metricsRepo := repository.NewMetricsDynamodb(db, ENV["DYNAMODB_TABLE"])
	metricsService := metrics.NewService(metricsRepo)

	var recaptchaService *recaptcha.Service
	if ENV["RECAPTCHA_DISABLE"] == "true" {
		recaptchaRepo := repository.NewRecaptchaLogger(ENV["RECAPTCHA_SECRET"])
		recaptchaService = recaptcha.NewService(recaptchaRepo)
	} else {
		recaptchaRepo := repository.NewRecaptchaGoogle(ENV["RECAPTCHA_SECRET"])
		recaptchaService = recaptcha.NewService(recaptchaRepo)
	}

	recipeRepo := repository.NewRecipeDynamodb(db, ENV["DYNAMODB_TABLE"])
	recipeRepoS3 := repository.NewRecipeS3(s3, ENV["S3_UPLOADS_BUCKET"], ENV["S3_BASE_URL"])
	recipeService := recipe.NewService(recipeRepo, recipeRepoS3)

	requesterRepo := repository.NewRequesterDynamodb(db, ENV["DYNAMODB_TABLE"])
	requesterService := requester.NewService(requesterRepo, loc, ENV["TIMESTAMP_FORMAT"])

	// salesforceRepo := salesforce.NewRequesterDynamodb(db, ENV["DYNAMODB_TABLE"])
	salesforceService := salesforce.NewService(loc, ENV["SALESFORCE_AUTH_URL"], ENV["SALESFORCE_ACCOUNT"], ENV["SALESFORCE_CLIENT"], ENV["SALESFORCE_SECRET"], ENV["SALESFORCE_PROFILE"], ENV["SALESFORCE_SOURCE_ID"], ENV["SALESFORCE_SUBSCRIPTION_ID"])

	// salesforceRepo := salesforce.NewRequesterDynamodb(db, ENV["DYNAMODB_TABLE"])
	southernMadeService := southernMade.NewService(loc, ENV["API_BASE_URL"], ENV["API_TOKEN"], ENV["API_CAT_SOUTH"], ENV["API_CAT_BILLS"], ENV["API_CAT_BENGALS"], ENV["API_CAT_LIONS"], ENV["API_CAT_PATRIOTS"], ENV["API_CAT_GIANTS"], ENV["API_CAT_JETS"], ENV["API_CAT_EAGLES"], ENV["API_CAT_STEELERS"])

	gqlc := generated.Config{
		Resolvers: &graph.Resolver{
			AdminService:        adminService,
			AuthService:         authService,
			BaseURL:             ENV["MAILER_BASE_URL"],
			BanlistService:      banlistService,
			ConsumerService:     consumerService,
			DapperService:       dapperService,
			EntryService:        entryService,
			Loc:                 loc,
			MailerService:       mailerService,
			MetricsService:      metricsService,
			RecipeService:       recipeService,
			RecaptchaService:    recaptchaService,
			RequesterService:    requesterService,
			SalesforceService:   salesforceService,
			SecurityLevel:       2,
			SouthernMadeService: southernMadeService,
			TSFormat:            ENV["TIMESTAMP_FORMAT"],
		},
	}

	gqlc.Directives.HasRole = func(ctx context.Context, obj interface{}, next graphql.Resolver, role string) (interface{}, error) {
		if ctx.Value(entity.UserCtxKey) == nil {
			return nil, entity.ErrNoContent
		}

		rs := ctx.Value(entity.UserCtxKey).(graph.UserCtx).Role

		if role != rs {
			return nil, entity.ErrForbidden
		}

		return next(ctx)
	}

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(gqlc))

	srv.SetErrorPresenter(func(ctx context.Context, e error) *gqlerror.Error {
		ext := map[string]interface{}{
			"code":        entity.ErrInternalError.Code,
			"requestID":   req.RequestContext.RequestID,
			"requestTime": req.RequestContext.Time,
		}

		err := graphql.DefaultErrorPresenter(ctx, e)

		var CCFE *types.ConditionalCheckFailedException
		var AE *entity.AppError

		if errors.As(e, &CCFE) {
			ext["code"] = entity.ErrExists.Code
		} else if errors.As(e, &AE) {
			err.Message = AE.Message.Error()
			ext["code"] = AE.Code
		} else {
			errorItem := errors.Unwrap(e)
			//lint:ignore S1034 we need to do it this way because the validation library does not return error types properly
			switch errorItem.(type) {
			case validation.Errors:
				ext["code"] = entity.ErrInputViolation.Code
				var fields string
				for key := range errorItem.(validation.Errors) {
					fields = fields + "|" + key
				}
				ext["field"] = strings.Replace(fields, "|", "", 1)
			}
		}

		err.Extensions = ext

		logrus.Println(err)
		return err
	})

	var httpHandler http.HandlerFunc = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//
		// Start Referrer Middleware
		//
		ipAddress, _, _ := net.SplitHostPort(r.RemoteAddr)
		fwdAddress := req.Headers["x-forwarded-for"]
		referer := r.Referer()
		userAgent := r.UserAgent()

		if fwdAddress != "" {
			// Got X-Forwarded-For
			ipAddress = fwdAddress // If it's a single IP, then awesome!

			// If we got an array... grab the first IP
			ips := strings.Split(fwdAddress, ", ")
			if len(ips) > 1 {
				ipAddress = ips[0]
			}
		}

		refContext := context.WithValue(r.Context(), entity.RequesterCtxKey, entity.Requester{
			PK:        "REQUESTER#" + ipAddress + "#" + userAgent,
			SK:        "#DETAILS",
			IP:        ipAddress,
			UserAgent: userAgent,
		})

		//
		// Start JWT Middleware
		//
		token := req.Headers["authorization"]
		var reqContext context.Context

		if token != "" {
			token = strings.TrimPrefix(token, "Bearer ")
			pk, role, err := validateAndGetUser(token, ENV["JWT_SECRET"])
			if err != nil || pk == "" {
				http.Error(w, entity.ErrInvalidToken.Error(), http.StatusForbidden)
				return
			}

			UserCtx := graph.UserCtx{
				Role:    role,
				Referer: referer,
			}

			if role == entity.RoleAdminKey {
				Admin, err := adminService.GetAdmin(ctx, pk)
				if err != nil {
					http.Error(w, entity.ErrInvalidToken.Error(), http.StatusForbidden)
					return
				}
				UserCtx.Admin = Admin
			}

			if role == entity.RoleUserKey {
				Consumer, err := consumerService.GetConsumer(ctx, pk)
				if err != nil {
					http.Error(w, entity.ErrInvalidToken.Error(), http.StatusForbidden)
					return
				}
				UserCtx.Consumer = Consumer
			}

			reqContext = context.WithValue(refContext, entity.UserCtxKey, UserCtx)
		} else {
			reqContext = context.WithValue(refContext, entity.UserCtxKey, graph.UserCtx{
				Referer: referer,
			})
		}

		srv.ServeHTTP(w, r.WithContext(reqContext))
	})

	adapter = handlerfunc.NewV2(httpHandler)

	return adapter.Proxy(req)
}

func validateAndGetUser(headerToken string, jwt_secret string) (string, string, error) {
	token, err := jwt.Parse(headerToken, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, entity.ErrSigningMethod
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return []byte(jwt_secret), nil
	})

	if err != nil {
		return "", "", err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims["pk"].(string), claims["role"].(string), nil
	}

	return "", "", entity.ErrInvalidToken
}

// func printContextInternals(ctx interface{}, inner bool) {
// 	contextValues := reflect.ValueOf(ctx).Elem()
// 	contextKeys := reflect.TypeOf(ctx).Elem()

// 	if !inner {
// 		logrus.Printf("\nFields for %s.%s\n", contextKeys.PkgPath(), contextKeys.Name())
// 	}

// 	if contextKeys.Kind() == reflect.Struct {
// 		for i := 0; i < contextValues.NumField(); i++ {
// 			reflectValue := contextValues.Field(i)
// 			reflectValue = reflect.NewAt(reflectValue.Type(), unsafe.Pointer(reflectValue.UnsafeAddr())).Elem()

// 			reflectField := contextKeys.Field(i)

// 			if reflectField.Name == "Context" {
// 				printContextInternals(reflectValue.Interface(), true)
// 			} else {
// 				logrus.Printf("field name: %+v\n", reflectField.Name)
// 				logrus.Printf("value: %+v\n", reflectValue.Interface())
// 			}
// 		}
// 	} else {
// 		logrus.Printf("context is empty (int)\n")
// 	}
// }
