package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	"github.com/gofrs/uuid"
)

func (r *mutationResolver) DapperEnter(ctx context.Context, input model.DapperEntryItem) (*model.DapperEntryResponse, error) {
	req := currentRequester(ctx)
	err := r.RequesterService.StoreRequester(ctx, req)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckIPBan(ctx, req.IP)
	if err != nil {
		return nil, err
	}

	result, err := r.RecaptchaService.CheckRecaptcha(req.IP, input.CaptchaResponse)
	if err != nil || result == nil {
		return nil, entity.ErrInvalidCaptcha
	}

	entry, err := entity.CreateDapperEntry(input)
	if err != nil {
		return nil, err
	}

	err = entity.ConsumerValidateAge(entry.Birthdate)
	if err != nil {
		return nil, err
	}

	time := time.Now().In(r.Loc)
	// date := time.Format("2006-01-02")
	timestamp := time.Format(r.TSFormat)
	// dayOfWeek := time.Weekday().String()

	entry.CreatedAt = timestamp

	err = r.BanlistService.CheckDomainBan(ctx, entry.Email, req.IP)
	if err != nil {
		return nil, err
	}

	emailValidate, err := r.MailerService.ValidateEmail(ctx, entry.Email)
	if err != nil {
		return nil, err
	}

	if emailValidate.Risk == "medium" || emailValidate.Risk == "high" || emailValidate.Risk == "unknown" {
		return nil, entity.ErrBanned
	}

	if emailValidate != nil {
		emailValidate.PK = "VALIDATION"
		emailValidate.SK = "#" + emailValidate.Risk + "#" + emailValidate.Address
	}

	entry.EmailValidation = emailValidate

	success, message, err := r.SalesforceService.SubmitStrings(ctx, entry.OriginalEmail, entry.FirstName, entry.LastName, entry.Birthdate, entry.State.String(), entry.Zipcode, entry.Phone, entry.Newsletter)
	if err != nil {
		return nil, err
	}

	entry.Salesforce = *success
	entry.SalesforceMessage = *message

	prizeType, err := r.DapperService.Enter(ctx, entry)
	if err != nil {
		return nil, err
	}

	entry.Type = prizeType
	err = r.DapperService.Upsert(ctx, entry)
	if err != nil {
		return nil, err
	}

	err = r.MailerService.DapperWin(ctx, entry)
	if err != nil {
		return nil, err
	}

	return &model.DapperEntryResponse{
		Winner: true,
		Prize:  *entry.Type,
	}, nil
}

func (r *mutationResolver) DapperConfirm(ctx context.Context, input model.DapperEntryConfirmItem) (*model.SuccessMessage, error) {
	req := currentRequester(ctx)
	err := r.RequesterService.StoreRequester(ctx, req)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckIPBan(ctx, req.IP)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckDomainBan(ctx, input.Email, req.IP)
	if err != nil {
		return nil, err
	}

	result, err := r.RecaptchaService.CheckRecaptcha(req.IP, input.CaptchaResponse)
	if err != nil || result == nil {
		return nil, entity.ErrInvalidCaptcha
	}

	entry, err := r.DapperService.Get(ctx, input.Email)
	if err != nil {
		return nil, err
	}

	if *entry.Token != input.Token {
		return nil, entity.ErrForbidden
	}

	time := time.Now().In(r.Loc)
	timestamp := time.Format(r.TSFormat)

	entry.ConfirmedAt = timestamp
	entry.FirstName = input.FirstName
	entry.LastName = input.LastName
	entry.Address = input.Address
	entry.Address2 = input.Address2
	entry.City = input.City
	entry.State = input.State
	entry.Zipcode = input.Zipcode

	uuid, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}

	uuidString := uuid.String()
	entry.Token = &uuidString

	err = r.DapperService.Upsert(ctx, *entry)
	if err != nil {
		return nil, err
	}

	return &model.SuccessMessage{
		Success: true,
		Message: "Dapper Kit information updated.",
	}, nil
}

func (r *queryResolver) DapperAvailable(ctx context.Context) (bool, error) {
	kits, err := r.DapperService.Available(ctx)
	if err != nil {
		return false, err
	}

	if kits.AvailableS > 0 || kits.AvailableM > 0 || kits.AvailableL > 0 || kits.AvailableXL > 0 || kits.AvailableXXL > 0 {
		return true, nil
	}

	return false, nil
}
