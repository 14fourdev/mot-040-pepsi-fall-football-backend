package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"strings"
	"time"

	api "bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/api/utils"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	"github.com/gosimple/slug"
	"github.com/vincent-petithory/dataurl"
)

func (r *mutationResolver) FeaturedRecipeCreate(ctx context.Context, input model.FeaturedRecipeInput) (*model.FeaturedRecipe, error) {
	date, err := time.Parse("2006-01-02", input.Week)
	if err != nil {
		return nil, err
	}

	if int(date.Weekday()) != int(time.Thursday) {
		return nil, entity.ErrInputViolation
	}

	fr := entity.CreateFeaturedRecipeWeekly(input)

	err = r.RecipeService.InsertFeaturedRecipe(ctx, fr)
	if err != nil {
		return nil, err
	}

	return &model.FeaturedRecipe{
		Pk:   fr.PK,
		Sk:   fr.SK,
		Slug: fr.Slug,
		Week: fr.Week,
		Team: fr.Team,
	}, nil
}

func (r *mutationResolver) FeaturedRecipeUpdate(ctx context.Context, input model.FeaturedRecipeInput) (*model.FeaturedRecipe, error) {
	date, err := time.Parse("2006-01-02", input.Week)
	if err != nil {
		return nil, err
	}

	if int(date.Weekday()) != int(time.Thursday) {
		return nil, entity.ErrInputViolation
	}

	fr := entity.CreateFeaturedRecipeWeekly(input)

	err = r.RecipeService.UpsertFeaturedRecipe(ctx, fr)
	if err != nil {
		return nil, err
	}

	return &model.FeaturedRecipe{
		Pk:   fr.PK,
		Sk:   fr.SK,
		Slug: fr.Slug,
		Week: fr.Week,
		Team: fr.Team,
	}, nil
}

func (r *mutationResolver) RecipeUpdate(ctx context.Context, input model.RecipeItemInput) (*model.RecipeItem, error) {
	var fileUrl *string

	if input.Image != nil {
		if strings.HasPrefix(*input.Image, "https:") {
			fileUrl = input.Image
		} else {
			file, err := dataurl.DecodeString(*input.Image)
			if err != nil {
				return nil, err
			}

			imageName := "recipe-images/" + slug.Make(input.Slug) + "-image"
			switch file.MediaType.ContentType() {
			case "image/png":
				imageName = imageName + ".png"
			case "image/jpeg":
				imageName = imageName + ".jpg"
			default:
				return nil, entity.ErrInputViolation
			}

			fileUrl, err = r.RecipeService.PutImage(ctx, imageName, file.Data, file.MediaType.ContentType())
			if err != nil {
				return nil, err
			}
		}
	}

	recipe := entity.CreateRecipe(input, fileUrl)

	time := time.Now().In(r.Loc)
	timestamp := time.Format(r.TSFormat)
	recipe.CreatedAt = timestamp

	err := r.RecipeService.Upsert(ctx, recipe)
	if err != nil {
		return nil, err
	}

	out := &model.RecipeItem{
		Pk:     recipe.PK,
		Sk:     recipe.SK,
		Slug:   recipe.Slug,
		Active: recipe.Active,
		Teams:  recipe.Teams,
		Image:  recipe.Image,
	}

	if recipe.En != nil {
		out.En = &model.RecipeItemDetails{
			Title:        recipe.En.Title,
			DetailTitle:  recipe.En.DetailTitle,
			Tagline:      recipe.En.Tagline,
			Description:  recipe.En.Description,
			Ingredients:  recipe.En.Ingredients,
			Instructions: recipe.En.Instructions,
			Features:     recipe.En.Features,
		}
	}

	if recipe.Es != nil {
		out.Es = &model.RecipeItemDetails{
			Title:        recipe.Es.Title,
			DetailTitle:  recipe.Es.DetailTitle,
			Tagline:      recipe.Es.Tagline,
			Description:  recipe.Es.Description,
			Ingredients:  recipe.Es.Ingredients,
			Instructions: recipe.Es.Instructions,
			Features:     recipe.Es.Features,
		}
	}

	return out, nil
}

func (r *mutationResolver) RecipeDelete(ctx context.Context, input string) (*model.SuccessMessage, error) {
	err := r.RecipeService.Delete(ctx, input)
	if err != nil {
		return nil, err
	}

	return &model.SuccessMessage{
		Success: true,
		Message: "Recipe has been deleted.",
	}, nil
}

func (r *queryResolver) Recipes(ctx context.Context, input *model.EntryTeam) ([]*model.RecipeItem, error) {
	recipes, err := r.RecipeService.Get(ctx)
	if err != nil {
		return nil, err
	}

	var out []*model.RecipeItem

	for _, r := range recipes {
		add := false

		if input != nil {
			if api.ContainsTeam(*input, r.Teams) && r.Active {
				add = true
			}
		} else if r.Active {
			add = true
		}

		if add {
			out = append(out, &model.RecipeItem{
				Pk:     r.PK,
				Sk:     r.SK,
				Slug:   r.Slug,
				Active: r.Active,
				Teams:  r.Teams,
				Image:  r.Image,
				En: &model.RecipeItemDetails{
					Title:        r.En.Title,
					DetailTitle:  r.En.DetailTitle,
					Tagline:      r.En.Tagline,
					Description:  r.En.Description,
					Ingredients:  r.En.Ingredients,
					Instructions: r.En.Instructions,
					Features:     r.En.Features,
				},
				Es: &model.RecipeItemDetails{
					Title:        r.Es.Title,
					DetailTitle:  r.Es.DetailTitle,
					Tagline:      r.Es.Tagline,
					Description:  r.Es.Description,
					Ingredients:  r.Es.Ingredients,
					Instructions: r.Es.Instructions,
					Features:     r.Es.Features,
				},
				CreatedAt: r.CreatedAt,
			})
		}
	}

	return out, nil
}

func (r *queryResolver) FeaturedRecipe(ctx context.Context, input model.EntryTeam) (*model.FeaturedRecipe, error) {
	time := api.WeekStartDate(time.Now().In(r.Loc), int(time.Thursday))
	date := time.Format("2006-01-02")

	fr, err := r.RecipeService.GetFeaturedRecipe(ctx, date, input)
	if err != nil {
		return nil, err
	}

	return &model.FeaturedRecipe{
		Pk:   fr.PK,
		Sk:   fr.SK,
		Slug: fr.Slug,
		Week: fr.Week,
		Team: fr.Team,
	}, nil
}

func (r *queryResolver) AdminRecipes(ctx context.Context) ([]*model.RecipeItem, error) {
	recipes, err := r.RecipeService.Get(ctx)
	if err != nil {
		return nil, err
	}

	var out []*model.RecipeItem

	for _, r := range recipes {
		out = append(out, &model.RecipeItem{
			Pk:     r.PK,
			Sk:     r.SK,
			Slug:   r.Slug,
			Active: r.Active,
			Teams:  r.Teams,
			Image:  r.Image,
			En: &model.RecipeItemDetails{
				Title:        r.En.Title,
				DetailTitle:  r.En.DetailTitle,
				Tagline:      r.En.Tagline,
				Description:  r.En.Description,
				Ingredients:  r.En.Ingredients,
				Instructions: r.En.Instructions,
				Features:     r.En.Features,
			},
			Es: &model.RecipeItemDetails{
				Title:        r.Es.Title,
				DetailTitle:  r.Es.DetailTitle,
				Tagline:      r.Es.Tagline,
				Description:  r.Es.Description,
				Ingredients:  r.Es.Ingredients,
				Instructions: r.Es.Instructions,
				Features:     r.Es.Features,
			},
			CreatedAt: r.CreatedAt,
		})
	}

	return out, nil
}

func (r *queryResolver) AdminFeaturedRecipes(ctx context.Context) ([]*model.FeaturedRecipe, error) {
	recipes, err := r.RecipeService.GetAllFeaturedRecipes(ctx)
	if err != nil {
		return nil, err
	}

	var out []*model.FeaturedRecipe

	for _, r := range recipes {
		out = append(out, &model.FeaturedRecipe{
			Pk:   r.PK,
			Sk:   r.SK,
			Slug: r.Slug,
			Team: r.Team,
			Week: r.Week,
		})
	}

	return out, nil
}
