package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

func (r *mutationResolver) ConsumerCreate(ctx context.Context, input model.NewConsumer) (*model.ConsumerLogin, error) {
	cutoff := time.Date(2021, 10, 16, 23, 59, 59, 0, r.Loc)
	if time.Now().In(r.Loc).After(cutoff) {
		return nil, entity.ErrInputViolation
	}

	req := currentRequester(ctx)
	err := r.RequesterService.StoreRequester(ctx, req)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckIPBan(ctx, req.IP)
	if err != nil {
		return nil, err
	}

	if input.Password != input.PasswordConfirm {
		return nil, entity.ErrInvalidConfirmPassword
	}

	consumer, err := entity.CreateConsumer(input)
	if err != nil {
		return nil, err
	}

	err = entity.ConsumerValidateAge(consumer.Birthdate)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckDomainBan(ctx, consumer.Email, req.IP)
	if err != nil {
		return nil, err
	}

	result, err := r.RecaptchaService.CheckRecaptcha(req.IP, input.CaptchaResponse)
	if err != nil || result == nil {
		return nil, entity.ErrInvalidCaptcha
	}

	emailValidate, err := r.MailerService.ValidateEmail(ctx, consumer.Email)
	if err != nil {
		return nil, err
	}

	if emailValidate.Risk == "medium" || emailValidate.Risk == "high" || emailValidate.Risk == "unknown" {
		return nil, entity.ErrBanned
	}

	if emailValidate != nil {
		emailValidate.PK = "VALIDATION"
		emailValidate.SK = "#" + emailValidate.Risk + "#" + emailValidate.Address
	}

	consumer.EmailValidation = emailValidate

	time := time.Now().In(r.Loc)
	date := time.Format("2006-01-02")
	timestamp := time.Format(r.TSFormat)
	dayOfWeek := time.Weekday().String()

	consumer.CreatedAt = timestamp

	zip, err := r.ConsumerService.GetZipcode(ctx, input.Zipcode)
	if err != nil && err != entity.ErrNotFound {
		return nil, err
	}

	if err != entity.ErrNotFound {
		consumer.Division = zip.Division
	}

	consumer, _ = r.SalesforceService.SubmitConsumer(ctx, consumer)

	err = r.ConsumerService.PutConsumer(ctx, consumer)
	if err != nil {
		return nil, err
	}

	tokenString, err := r.AuthService.GenerateToken(14, consumer.PK, entity.RoleUserKey)
	if err != nil {
		return nil, err
	}

	err = r.MailerService.Registered(ctx, consumer)
	if err != nil {
		return nil, err
	}

	err = r.MetricsService.UpdateMetricsUser(ctx, date, dayOfWeek, consumer)
	if err != nil {
		logrus.Error(err)
		// return nil, err
	}

	validation := &model.MGValidationResponseItem{}
	if consumer.EmailValidation != nil {
		validation.Pk = consumer.EmailValidation.PK
		validation.Sk = consumer.EmailValidation.SK
		validation.Address = consumer.EmailValidation.Address
		validation.IsDisposable = consumer.EmailValidation.IsDisposable
		validation.IsRoleAddress = consumer.EmailValidation.IsRoleAddress
		validation.Reason = consumer.EmailValidation.Reason
		validation.Result = consumer.EmailValidation.Result
		validation.Risk = consumer.EmailValidation.Risk
	}

	return &model.ConsumerLogin{
		Token: tokenString,
		Consumer: &model.ConsumerItem{
			Pk:                consumer.PK,
			Sk:                consumer.SK,
			Email:             consumer.Email,
			FirstName:         consumer.FirstName,
			LastName:          consumer.LastName,
			Phone:             consumer.Phone,
			Address:           consumer.Address,
			Address2:          consumer.Address2,
			City:              consumer.City,
			State:             consumer.State,
			Zipcode:           consumer.Zipcode,
			Division:          consumer.Division,
			Birthdate:         consumer.Birthdate,
			Team:              consumer.Team,
			Rules:             consumer.Rules,
			Newsletter:        consumer.Newsletter,
			Salesforce:        consumer.Salesforce,
			SalesforceMessage: consumer.SalesforceMessage,
			CurrentPoints:     consumer.CurrentPoints,
			TotalPoints:       consumer.TotalPoints,
			Language:          consumer.Language,
			Status:            consumer.Status,
			EmailValidation:   validation,
			CreatedAt:         consumer.CreatedAt,
		},
	}, nil
}

func (r *mutationResolver) ConsumerLogin(ctx context.Context, input model.LoginConsumer) (*model.ConsumerLogin, error) {
	cutoff := time.Date(2021, 10, 30, 23, 59, 59, 0, r.Loc)
	if time.Now().In(r.Loc).After(cutoff) {
		return nil, entity.ErrInputViolation
	}

	req := currentRequester(ctx)
	err := r.RequesterService.StoreRequester(ctx, req)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckIPBan(ctx, req.IP)
	if err != nil {
		return nil, err
	}

	logrus.Println("Login IP:" + req.IP)
	logrus.Println("Login User:" + input.Email)

	result, err := r.RecaptchaService.CheckRecaptcha(req.IP, input.CaptchaResponse)
	if err != nil || result == nil {
		return nil, entity.ErrInvalidCaptcha
	}

	consumer, err := r.ConsumerService.GetConsumer(ctx, entity.CleanEmail(input.Email))
	if err != nil {
		return nil, entity.ErrInvalid
	}

	err = bcrypt.CompareHashAndPassword([]byte(consumer.Password), []byte(input.Password))
	if err != nil {
		return nil, entity.ErrUnauthorized
	}

	err = entity.ConsumerCheckStatus(*consumer)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckDomainBan(ctx, consumer.Email, req.IP)
	if err != nil {
		return nil, err
	}

	tokenString, err := r.AuthService.GenerateToken(14, consumer.PK, entity.RoleUserKey)
	if err != nil {
		return nil, err
	}

	weeklyEntries := []*model.WeeklySweepsEntryItem{}
	for _, e := range consumer.WeeklySweepsEntries.Entries {
		weeklyEntries = append(weeklyEntries, &model.WeeklySweepsEntryItem{
			WeekStartDate: e.WeekStartDate,
			Count:         e.Count,
		})
	}

	validation := &model.MGValidationResponseItem{}
	if consumer.EmailValidation != nil {
		validation.Pk = consumer.EmailValidation.PK
		validation.Sk = consumer.EmailValidation.SK
		validation.Address = consumer.EmailValidation.Address
		validation.IsDisposable = consumer.EmailValidation.IsDisposable
		validation.IsRoleAddress = consumer.EmailValidation.IsRoleAddress
		validation.Reason = consumer.EmailValidation.Reason
		validation.Result = consumer.EmailValidation.Result
		validation.Risk = consumer.EmailValidation.Risk
	}

	return &model.ConsumerLogin{
		Token: tokenString,
		Consumer: &model.ConsumerItem{
			Pk:                  consumer.PK,
			Sk:                  consumer.SK,
			Email:               consumer.OriginalEmail,
			FirstName:           consumer.FirstName,
			LastName:            consumer.LastName,
			Phone:               consumer.Phone,
			Address:             consumer.Address,
			Address2:            consumer.Address2,
			City:                consumer.City,
			State:               consumer.State,
			Zipcode:             consumer.Zipcode,
			Division:            consumer.Division,
			Birthdate:           consumer.Birthdate,
			Team:                consumer.Team,
			Rules:               consumer.Rules,
			Newsletter:          consumer.Newsletter,
			Salesforce:          consumer.Salesforce,
			SalesforceMessage:   consumer.SalesforceMessage,
			CurrentPoints:       consumer.CurrentPoints,
			TotalPoints:         consumer.TotalPoints,
			WeeklySweepsEntries: weeklyEntries,
			Language:            consumer.Language,
			Status:              consumer.Status,
			EmailValidation:     validation,
			CreatedAt:           consumer.CreatedAt,
		},
	}, nil
}

func (r *mutationResolver) ConsumerForgot(ctx context.Context, input model.NewConsumerForgot) (*model.SuccessMessage, error) {
	cutoff := time.Date(2021, 10, 30, 23, 59, 59, 0, r.Loc)
	if time.Now().In(r.Loc).After(cutoff) {
		return nil, entity.ErrInputViolation
	}

	req := currentRequester(ctx)
	err := r.RequesterService.StoreRequester(ctx, req)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckIPBan(ctx, req.IP)
	if err != nil {
		return nil, err
	}

	result, err := r.RecaptchaService.CheckRecaptcha(req.IP, input.CaptchaResponse)
	if err != nil || result == nil {
		return nil, entity.ErrInvalidCaptcha
	}

	sm := &model.SuccessMessage{Success: true}

	consumer, err := r.ConsumerService.GetConsumer(ctx, entity.CleanEmail(input.Email))
	if err != nil {
		if r.SecurityLevel == 1 {
			return nil, entity.ErrNotFound
		} else if r.SecurityLevel == 2 {
			sm.Message = "A reset link has been sent to your email, if it exists."
			return sm, nil
		}
	}

	err = entity.ConsumerCheckStatus(*consumer)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckDomainBan(ctx, consumer.Email, req.IP)
	if err != nil {
		return nil, err
	}

	reset, err := entity.CreateReset(consumer.PK, entity.RoleUserKey)
	if err != nil {
		return nil, err
	}

	reset.ExpiresAt = time.Now().In(r.Loc).Add(24 * time.Hour)
	reset.CreatedAt = time.Now().In(r.Loc).Format(r.TSFormat)

	err = r.AuthService.PutReset(ctx, reset)
	if err != nil {
		return nil, err
	}

	err = r.MailerService.ForgotPassword(ctx, *consumer, reset.Token)
	if err != nil {
		return nil, err
	}

	if r.SecurityLevel == 1 {
		sm.Message = "Please check your email for a link to reset your password."
	} else if r.SecurityLevel == 2 {
		sm.Message = "A reset link has been sent to your email, if it exists."
	}

	return sm, nil
}

func (r *mutationResolver) ConsumerReset(ctx context.Context, input model.NewConsumerReset) (*model.ConsumerLogin, error) {
	cutoff := time.Date(2021, 10, 30, 23, 59, 59, 0, r.Loc)
	if time.Now().In(r.Loc).After(cutoff) {
		return nil, entity.ErrInputViolation
	}

	req := currentRequester(ctx)
	err := r.RequesterService.StoreRequester(ctx, req)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckIPBan(ctx, req.IP)
	if err != nil {
		return nil, err
	}

	result, err := r.RecaptchaService.CheckRecaptcha(req.IP, input.CaptchaResponse)
	if err != nil || result == nil {
		return nil, entity.ErrInvalidCaptcha
	}

	if input.Password != input.PasswordConfirm {
		return nil, entity.ErrInvalidConfirmPassword
	}

	reset, err := r.AuthService.GetReset(ctx, input.Token, entity.RoleUserKey)
	if err != nil {
		return nil, err
	}

	if time.Now().In(r.Loc).After(reset.ExpiresAt) {
		return nil, entity.ErrInvalidToken
	} else {
		reset.ExpiresAt = time.Now().In(r.Loc)
		reset.Status = "used"
		err = r.AuthService.PutReset(ctx, reset)
		if err != nil {
			return nil, err
		}
	}

	cons, err := r.ConsumerService.GetConsumer(ctx, reset.User)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckDomainBan(ctx, cons.Email, req.IP)
	if err != nil {
		return nil, err
	}

	consumer := *cons
	consumer.Password, err = entity.HashPassword(input.Password)
	if err != nil {
		return nil, err
	}

	err = r.ConsumerService.UpsertConsumer(ctx, consumer)
	if err != nil {
		return nil, err
	}

	tokenString, err := r.AuthService.GenerateToken(14, consumer.PK, entity.RoleUserKey)
	if err != nil {
		return nil, err
	}

	weeklyEntries := []*model.WeeklySweepsEntryItem{}
	for _, e := range consumer.WeeklySweepsEntries.Entries {
		weeklyEntries = append(weeklyEntries, &model.WeeklySweepsEntryItem{
			WeekStartDate: e.WeekStartDate,
			Count:         e.Count,
		})
	}

	validation := &model.MGValidationResponseItem{}
	if consumer.EmailValidation != nil {
		validation.Pk = consumer.EmailValidation.PK
		validation.Sk = consumer.EmailValidation.SK
		validation.Address = consumer.EmailValidation.Address
		validation.IsDisposable = consumer.EmailValidation.IsDisposable
		validation.IsRoleAddress = consumer.EmailValidation.IsRoleAddress
		validation.Reason = consumer.EmailValidation.Reason
		validation.Result = consumer.EmailValidation.Result
		validation.Risk = consumer.EmailValidation.Risk
	}

	return &model.ConsumerLogin{
		Token: tokenString,
		Consumer: &model.ConsumerItem{
			Pk:                  consumer.PK,
			Sk:                  consumer.SK,
			Email:               consumer.OriginalEmail,
			FirstName:           consumer.FirstName,
			LastName:            consumer.LastName,
			Phone:               consumer.Phone,
			Address:             consumer.Address,
			Address2:            consumer.Address2,
			City:                consumer.City,
			State:               consumer.State,
			Zipcode:             consumer.Zipcode,
			Division:            consumer.Division,
			Birthdate:           consumer.Birthdate,
			Team:                consumer.Team,
			Rules:               consumer.Rules,
			Newsletter:          consumer.Newsletter,
			Salesforce:          consumer.Salesforce,
			SalesforceMessage:   consumer.SalesforceMessage,
			CurrentPoints:       consumer.CurrentPoints,
			TotalPoints:         consumer.TotalPoints,
			WeeklySweepsEntries: weeklyEntries,
			Language:            consumer.Language,
			Status:              consumer.Status,
			EmailValidation:     validation,
			CreatedAt:           consumer.CreatedAt,
		},
	}, nil
}

func (r *queryResolver) Me(ctx context.Context) (*model.ConsumerItem, error) {
	cutoff := time.Date(2021, 10, 30, 23, 59, 59, 0, r.Loc)
	if time.Now().In(r.Loc).After(cutoff) {
		return nil, entity.ErrInputViolation
	}

	req := currentRequester(ctx)
	err := r.RequesterService.StoreRequester(ctx, req)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckIPBan(ctx, req.IP)
	if err != nil {
		return nil, err
	}

	consumer := currentConsumer(ctx)

	err = entity.ConsumerCheckStatus(*consumer)
	if err != nil {
		return nil, err
	}

	weeklyEntries := []*model.WeeklySweepsEntryItem{}
	for _, e := range consumer.WeeklySweepsEntries.Entries {
		weeklyEntries = append(weeklyEntries, &model.WeeklySweepsEntryItem{
			WeekStartDate: e.WeekStartDate,
			Count:         e.Count,
		})
	}

	validation := &model.MGValidationResponseItem{}
	if consumer.EmailValidation != nil {
		validation.Pk = consumer.EmailValidation.PK
		validation.Sk = consumer.EmailValidation.SK
		validation.Address = consumer.EmailValidation.Address
		validation.IsDisposable = consumer.EmailValidation.IsDisposable
		validation.IsRoleAddress = consumer.EmailValidation.IsRoleAddress
		validation.Reason = consumer.EmailValidation.Reason
		validation.Result = consumer.EmailValidation.Result
		validation.Risk = consumer.EmailValidation.Risk
	}

	return &model.ConsumerItem{
		Pk:                  consumer.PK,
		Sk:                  consumer.SK,
		Email:               consumer.OriginalEmail,
		FirstName:           consumer.FirstName,
		LastName:            consumer.LastName,
		Phone:               consumer.Phone,
		Address:             consumer.Address,
		Address2:            consumer.Address2,
		City:                consumer.City,
		State:               consumer.State,
		Zipcode:             consumer.Zipcode,
		Division:            consumer.Division,
		Birthdate:           consumer.Birthdate,
		Team:                consumer.Team,
		Rules:               consumer.Rules,
		Newsletter:          consumer.Newsletter,
		Salesforce:          consumer.Salesforce,
		SalesforceMessage:   consumer.SalesforceMessage,
		CurrentPoints:       consumer.CurrentPoints,
		TotalPoints:         consumer.TotalPoints,
		WeeklySweepsEntries: weeklyEntries,
		Language:            consumer.Language,
		Status:              consumer.Status,
		EmailValidation:     validation,
		CreatedAt:           consumer.CreatedAt,
	}, nil
}

func (r *queryResolver) Transactions(ctx context.Context) ([]*model.TransactionItem, error) {
	cutoff := time.Date(2021, 10, 30, 23, 59, 59, 0, r.Loc)
	if time.Now().In(r.Loc).After(cutoff) {
		return nil, entity.ErrInputViolation
	}

	req := currentRequester(ctx)
	err := r.RequesterService.StoreRequester(ctx, req)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckIPBan(ctx, req.IP)
	if err != nil {
		return nil, err
	}

	consumer := currentConsumer(ctx)
	err = entity.ConsumerCheckStatus(*consumer)
	if err != nil {
		return nil, err
	}

	adminEntries, err := r.EntryService.GetAdminPointsEntries(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	pointsEntries, err := r.EntryService.GetPointsEntries(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	sweepsEntries, err := r.EntryService.GetSweepsEntries(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	redemptions, err := r.EntryService.GetCouponCodeRedemptions(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	var transactions []*model.TransactionItem

	for _, e := range adminEntries {
		transactions = append(transactions, &model.TransactionItem{
			Type:   "adminPoints",
			Points: e.Points,
			Date:   e.CreatedAt,
		})
	}

	for _, e := range pointsEntries {
		transactions = append(transactions, &model.TransactionItem{
			Type:      "points",
			Points:    e.Points,
			Winner:    &e.Winner,
			Prize:     &e.Prize,
			PrizeSlug: &e.PrizeSlug,
			Date:      e.CreatedAt,
		})
	}

	for _, e := range sweepsEntries {
		transactions = append(transactions, &model.TransactionItem{
			Type:    "sweeps",
			Entries: &e.Count,
			Points:  e.TotalCost,
			Date:    e.CreatedAt,
		})
	}

	for _, e := range redemptions {
		transactions = append(transactions, &model.TransactionItem{
			Type:       "redemption",
			Points:     e.Cost,
			CouponCode: &e.Code,
			CouponURL:  &e.URL,
			Date:       e.CreatedAt,
		})
	}

	return transactions, nil
}
