package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"strings"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	"github.com/jinzhu/now"
	"github.com/sirupsen/logrus"
)

func (r *mutationResolver) Enter(ctx context.Context, input model.NewEntry) (*model.EntryItem, error) {
	cutoff := time.Date(2021, 10, 30, 23, 59, 59, 0, r.Loc)
	if time.Now().In(r.Loc).After(cutoff) {
		return nil, entity.ErrInputViolation
	}

	req := currentRequester(ctx)
	err := r.RequesterService.StoreRequester(ctx, req)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckIPBan(ctx, req.IP)
	if err != nil {
		return nil, err
	}

	consumer := currentConsumer(ctx)
	err = entity.ConsumerCheckStatus(*consumer)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckDomainBan(ctx, consumer.Email, req.IP)
	if err != nil {
		return nil, err
	}

	time := time.Now().In(r.Loc)
	date := time.Format("2006-01-02")
	timestamp := time.Format(r.TSFormat)
	dayOfWeek := time.Weekday().String()

	var amoe *entity.AmoeCode
	if input.AmoeCode != nil {
		amoe, err = r.EntryService.GetAmoeCode(ctx, *input.AmoeCode)
		if err != nil {
			return nil, err
		}

		if amoe.Used {
			return nil, entity.ErrCodeLimit
		}
	}

	post, err := entity.CreatePostPointEntry(*consumer, input)
	if err != nil {
		return nil, err
	}

	resp, err := r.SouthernMadeService.SendPointEntries(post)
	if err != nil {
		return nil, err
	}

	isAmoe := false

	if input.AmoeCode != nil {
		amoe.Used = true
		amoe.UsedAt = timestamp
		amoe.Consumer = consumer.PK
		amoe.Requester = &req.PK

		err = r.EntryService.PutAmoeCode(ctx, *amoe)
		if err != nil {
			return nil, err
		}

		isAmoe = true
	}

	entry := entity.CreatePointsEntry(*consumer, timestamp, date, input, *resp)
	entry.Requester = &req.PK

	consumer.CurrentPoints = consumer.CurrentPoints + entry.Points
	consumer.TotalPoints = consumer.TotalPoints + entry.Points

	err = r.EntryService.PutPointEntry(ctx, entry, *consumer)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	err = r.MetricsService.UpdateMetricsEntry(ctx, 1, date, dayOfWeek, consumer.State, entry.Team, entry.Winner, consumer.Division, entry.Points, isAmoe)
	if err != nil {
		logrus.Error(err)
		// return nil, err
	}

	if entry.Winner {
		err = r.MailerService.InstantWin(ctx, *consumer, entry)
		if err != nil {
			return nil, err
		}
	}

	return &model.EntryItem{
		PointsGained:  entry.Points,
		CurrentPoints: consumer.CurrentPoints,
		PointsTotal:   consumer.TotalPoints,
		Winner:        entry.Winner,
		Prize:         entry.Prize,
		PrizeSlug:     entry.PrizeSlug,
	}, nil
}

func (r *mutationResolver) Redeem(ctx context.Context, input model.NewRedemption) (*model.RedemptionItem, error) {
	cutoff := time.Date(2021, 10, 30, 23, 59, 59, 0, r.Loc)
	if time.Now().In(r.Loc).After(cutoff) {
		return nil, entity.ErrInputViolation
	}

	req := currentRequester(ctx)
	err := r.RequesterService.StoreRequester(ctx, req)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckIPBan(ctx, req.IP)
	if err != nil {
		return nil, err
	}

	consumer := currentConsumer(ctx)
	err = entity.ConsumerCheckStatus(*consumer)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckDomainBan(ctx, consumer.Email, req.IP)
	if err != nil {
		return nil, err
	}

	time := time.Now().In(r.Loc)
	date := time.Format("2006-01-02")
	timestamp := time.Format(r.TSFormat)
	dayOfWeek := time.Weekday().String()

	redemption, err := entity.CreateCouponCodeRedemption(*consumer, timestamp, input)
	if err != nil {
		return nil, err
	}

	coupon, err := r.EntryService.GetUnclaimedCode(ctx, redemption.Tier)
	if err != nil {
		return nil, err
	}

	if coupon == nil {
		return nil, entity.ErrNotFound
	}

	url := coupon.URL
	if !strings.HasPrefix(url, "http") {
		url = "https://" + url
	}

	redemption.Code = coupon.Code
	redemption.URL = url
	redemption.Requester = &req.PK
	coupon.SK = "#CLAIMED#" + coupon.Code
	coupon.Consumer = consumer.PK
	coupon.Used = true
	coupon.UsedAt = timestamp
	coupon.Requester = &req.PK

	err = r.EntryService.PutCouponRedemption(ctx, *coupon, *redemption, *consumer)
	if err != nil {
		return nil, err
	}

	err = r.MetricsService.UpdateMetricsRedemption(ctx, date, dayOfWeek, consumer.State, consumer.Division, *redemption)
	if err != nil {
		logrus.Error(err)
		// return nil, err
	}

	err = r.MailerService.PointRedemption(ctx, *consumer, redemption.URL, redemption.Tier)
	if err != nil {
		return nil, err
	}

	return &model.RedemptionItem{
		URL:           redemption.URL,
		PointsSpent:   redemption.Cost,
		CurrentPoints: consumer.CurrentPoints - redemption.Cost,
	}, nil
}

func (r *mutationResolver) Sweeps(ctx context.Context, input model.NewSweeps) (*model.SweepsResponse, error) {
	cutoff := time.Date(2021, 10, 30, 23, 59, 59, 0, r.Loc)
	if time.Now().In(r.Loc).After(cutoff) {
		return nil, entity.ErrInputViolation
	}

	req := currentRequester(ctx)
	err := r.RequesterService.StoreRequester(ctx, req)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckIPBan(ctx, req.IP)
	if err != nil {
		return nil, err
	}

	consumer := currentConsumer(ctx)
	err = entity.ConsumerCheckStatus(*consumer)
	if err != nil {
		return nil, err
	}

	err = r.BanlistService.CheckDomainBan(ctx, consumer.Email, req.IP)
	if err != nil {
		return nil, err
	}

	if input.Count > 2 {
		return nil, entity.ErrRateLimit
	}

	time := time.Now().In(r.Loc)
	date := time.Format("2006-01-02")
	timestamp := time.Format(r.TSFormat)
	dayOfWeek := time.Weekday().String()
	firstDayOfWeek := now.With(time).BeginningOfWeek().Format("2006-01-02")

	entry := entity.CreateSweepsEntry(*consumer, timestamp, input)
	entry.Requester = &req.PK

	found := false

	if consumer.WeeklySweepsEntries.Entries != nil {
		for _, e := range consumer.WeeklySweepsEntries.Entries {
			if e.WeekStartDate == firstDayOfWeek {
				e.Count = e.Count + entry.Count
				found = true
			}
		}
		if !found {
			consumer.WeeklySweepsEntries.Entries = append(consumer.WeeklySweepsEntries.Entries, &entity.WeeklySweepsEntry{
				WeekStartDate: firstDayOfWeek,
				Count:         entry.Count,
			})
		}
	} else {
		entries := []*entity.WeeklySweepsEntry{}
		entries = append(entries, &entity.WeeklySweepsEntry{
			WeekStartDate: firstDayOfWeek,
			Count:         entry.Count,
		})

		consumer.WeeklySweepsEntries.Entries = entries
	}

	err = r.EntryService.PutSweepsEntry(ctx, entry, *consumer)
	if err != nil {
		return nil, err
	}

	post, err := entity.CreatePostWeeklySweepstakesEntry(*consumer, input)
	if err != nil {
		return nil, err
	}

	_, err = r.SouthernMadeService.SendSweepsEntries(post)
	if err != nil {
		r.EntryService.RevertSweepsEntry(ctx, entry, *consumer)
		return nil, err
	}

	err = r.MetricsService.UpdateMetricsSweeps(ctx, entry.Count, date, dayOfWeek)
	if err != nil {
		logrus.Error(err)
		// return nil, err
	}

	weeklyEntries := []*model.WeeklySweepsEntryItem{}
	for _, e := range consumer.WeeklySweepsEntries.Entries {
		weeklyEntries = append(weeklyEntries, &model.WeeklySweepsEntryItem{
			WeekStartDate: e.WeekStartDate,
			Count:         e.Count,
		})
	}

	return &model.SweepsResponse{
		Entries:             entry.Count,
		PointsSpent:         entry.TotalCost,
		CurrentPoints:       consumer.CurrentPoints - entry.TotalCost,
		WeeklySweepsEntries: weeklyEntries,
	}, nil
}
