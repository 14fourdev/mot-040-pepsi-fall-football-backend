package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"strings"
	"time"

	api "bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/api/utils"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	"github.com/gofrs/uuid"
	"github.com/gosimple/slug"
	"github.com/vincent-petithory/dataurl"
)

func (r *mutationResolver) FsbCreate(ctx context.Context, input model.FSBInput) (*model.FSBItem, error) {
	var fileUrl string

	if strings.HasPrefix(input.Image, "https:") {
		fileUrl = input.Image
	} else {
		file, err := dataurl.DecodeString(input.Image)
		if err != nil {
			return nil, err
		}

		uuid, err := uuid.NewV4()
		if err != nil {
			return nil, err
		}

		imageName := "content-images/" + slug.Make(uuid.String())
		switch file.MediaType.ContentType() {
		case "image/png":
			imageName = imageName + ".png"
		case "image/jpeg":
			imageName = imageName + ".jpg"
		default:
			return nil, entity.ErrInputViolation
		}

		fileThing, err := r.RecipeService.PutImage(ctx, imageName, file.Data, file.MediaType.ContentType())
		if err != nil {
			return nil, err
		}

		fileUrl = *fileThing
	}

	fsb := entity.CreateFoodServiceBanner(input, fileUrl)

	err := r.RecipeService.InsertFSB(ctx, fsb)
	if err != nil {
		return nil, err
	}

	return &model.FSBItem{
		Pk:     fsb.PK,
		Sk:     fsb.SK,
		Active: fsb.Active,
		Team:   fsb.Team,
		Image:  fsb.Image,
		URL:    fsb.URL,
	}, nil
}

func (r *mutationResolver) FsbUpdate(ctx context.Context, input model.FSBInput) (*model.FSBItem, error) {
	var fileUrl string

	if strings.HasPrefix(input.Image, "https:") {
		fileUrl = input.Image
	} else {
		file, err := dataurl.DecodeString(input.Image)
		if err != nil {
			return nil, err
		}

		uuid, err := uuid.NewV4()
		if err != nil {
			return nil, err
		}

		imageName := "content-images/" + slug.Make(uuid.String())
		switch file.MediaType.ContentType() {
		case "image/png":
			imageName = imageName + ".png"
		case "image/jpeg":
			imageName = imageName + ".jpg"
		default:
			return nil, entity.ErrInputViolation
		}

		fileThing, err := r.RecipeService.PutImage(ctx, imageName, file.Data, file.MediaType.ContentType())
		if err != nil {
			return nil, err
		}

		fileUrl = *fileThing
	}

	fsb := entity.CreateFoodServiceBanner(input, fileUrl)

	err := r.RecipeService.UpsertFSB(ctx, fsb)
	if err != nil {
		return nil, err
	}

	return &model.FSBItem{
		Pk:     fsb.PK,
		Sk:     fsb.SK,
		Active: fsb.Active,
		Team:   fsb.Team,
		Image:  fsb.Image,
		URL:    fsb.URL,
	}, nil
}

func (r *mutationResolver) FsbDelete(ctx context.Context, input model.EntryTeam) (*model.SuccessMessage, error) {
	err := r.RecipeService.DeleteFSB(ctx, input)
	if err != nil {
		return nil, err
	}

	return &model.SuccessMessage{
		Success: true,
		Message: "Food Service Banner has been deleted.",
	}, nil
}

func (r *mutationResolver) ArmCreate(ctx context.Context, input model.ARMInput) (*model.ARMItem, error) {
	date, err := time.Parse("2006-01-02", input.Week)
	if err != nil {
		return nil, err
	}

	if int(date.Weekday()) != int(time.Thursday) {
		return nil, entity.ErrInputViolation
	}

	var fileUrl string
	var qrCode string

	if strings.HasPrefix(input.Image, "https:") {
		fileUrl = input.Image
	} else {
		file, err := dataurl.DecodeString(input.Image)
		if err != nil {
			return nil, err
		}

		uuid, err := uuid.NewV4()
		if err != nil {
			return nil, err
		}

		imageName := "content-images/arm/" + slug.Make(input.Team.String()) + "/" + uuid.String()
		switch file.MediaType.ContentType() {
		case "image/png":
			imageName = imageName + ".png"
		case "image/jpeg":
			imageName = imageName + ".jpg"
		default:
			return nil, entity.ErrInputViolation
		}

		fileThing, err := r.RecipeService.PutImage(ctx, imageName, file.Data, file.MediaType.ContentType())
		if err != nil {
			return nil, err
		}

		fileUrl = *fileThing
	}

	if strings.HasPrefix(input.Qr, "https:") {
		qrCode = input.Qr
	} else {
		file, err := dataurl.DecodeString(input.Qr)
		if err != nil {
			return nil, err
		}

		uuid, err := uuid.NewV4()
		if err != nil {
			return nil, err
		}

		imageName := "content-images/arm/qr/" + slug.Make(input.Team.String()) + "/" + uuid.String()
		switch file.MediaType.ContentType() {
		case "image/png":
			imageName = imageName + ".png"
		case "image/jpeg":
			imageName = imageName + ".jpg"
		default:
			return nil, entity.ErrInputViolation
		}

		fileThing, err := r.RecipeService.PutImage(ctx, imageName, file.Data, file.MediaType.ContentType())
		if err != nil {
			return nil, err
		}

		qrCode = *fileThing
	}

	arm := entity.CreateARModule(input, fileUrl, qrCode)

	err = r.RecipeService.InsertARM(ctx, arm)
	if err != nil {
		return nil, err
	}

	return &model.ARMItem{
		Pk:     arm.PK,
		Sk:     arm.SK,
		Active: arm.Active,
		Image:  arm.Image,
		Qr:     arm.QR,
		Team:   arm.Team,
		URL:    arm.URL,
		Week:   arm.Week,
	}, nil
}

func (r *mutationResolver) ArmUpdate(ctx context.Context, input model.ARMInput) (*model.ARMItem, error) {
	date, err := time.Parse("2006-01-02", input.Week)
	if err != nil {
		return nil, err
	}

	if int(date.Weekday()) != int(time.Thursday) {
		return nil, entity.ErrInputViolation
	}

	var fileUrl string
	var qrCode string

	if strings.HasPrefix(input.Image, "https:") {
		fileUrl = input.Image
	} else {
		file, err := dataurl.DecodeString(input.Image)
		if err != nil {
			return nil, err
		}

		uuid, err := uuid.NewV4()
		if err != nil {
			return nil, err
		}

		imageName := "content-images/arm/" + slug.Make(input.Team.String()) + "/" + uuid.String()
		switch file.MediaType.ContentType() {
		case "image/png":
			imageName = imageName + ".png"
		case "image/jpeg":
			imageName = imageName + ".jpg"
		default:
			return nil, entity.ErrInputViolation
		}

		fileThing, err := r.RecipeService.PutImage(ctx, imageName, file.Data, file.MediaType.ContentType())
		if err != nil {
			return nil, err
		}

		fileUrl = *fileThing
	}

	if strings.HasPrefix(input.Qr, "https:") {
		qrCode = input.Qr
	} else {
		file, err := dataurl.DecodeString(input.Qr)
		if err != nil {
			return nil, err
		}

		uuid, err := uuid.NewV4()
		if err != nil {
			return nil, err
		}

		imageName := "content-images/arm/qr/" + slug.Make(input.Team.String()) + "/" + uuid.String()
		switch file.MediaType.ContentType() {
		case "image/png":
			imageName = imageName + ".png"
		case "image/jpeg":
			imageName = imageName + ".jpg"
		default:
			return nil, entity.ErrInputViolation
		}

		fileThing, err := r.RecipeService.PutImage(ctx, imageName, file.Data, file.MediaType.ContentType())
		if err != nil {
			return nil, err
		}

		qrCode = *fileThing
	}

	arm := entity.CreateARModule(input, fileUrl, qrCode)

	err = r.RecipeService.UpsertARM(ctx, arm)
	if err != nil {
		return nil, err
	}

	return &model.ARMItem{
		Pk:     arm.PK,
		Sk:     arm.SK,
		Active: arm.Active,
		Image:  arm.Image,
		Qr:     arm.QR,
		Team:   arm.Team,
		URL:    arm.URL,
		Week:   arm.Week,
	}, nil
}

func (r *mutationResolver) ArmDelete(ctx context.Context, input model.DeleteWeekTeam) (*model.SuccessMessage, error) {
	err := r.RecipeService.DeleteARM(ctx, input.Team, input.Week)
	if err != nil {
		return nil, err
	}

	return &model.SuccessMessage{
		Success: true,
		Message: "AR Module has been deleted.",
	}, nil
}

func (r *queryResolver) FoodServiceBanner(ctx context.Context, input model.EntryTeam) (*model.FSBItem, error) {
	fsb, err := r.RecipeService.GetFSB(ctx, input)
	if err != nil {
		return nil, err
	}

	if !fsb.Active {
		return nil, entity.ErrNotFound
	}

	return &model.FSBItem{
		Pk:     fsb.PK,
		Sk:     fsb.SK,
		Active: fsb.Active,
		Image:  fsb.Image,
		Team:   fsb.Team,
		URL:    fsb.URL,
	}, nil
}

func (r *queryResolver) ArModule(ctx context.Context, input model.EntryTeam) (*model.ARMItem, error) {
	time := api.WeekStartDate(time.Now().In(r.Loc), int(time.Thursday))
	date := time.Format("2006-01-02")

	arm, err := r.RecipeService.GetARM(ctx, input, date)
	if err != nil {
		return nil, err
	}

	return &model.ARMItem{
		Pk:     arm.PK,
		Sk:     arm.SK,
		Active: arm.Active,
		Image:  arm.Image,
		Qr:     arm.QR,
		Team:   arm.Team,
		URL:    arm.URL,
		Week:   arm.Week,
	}, nil
}

func (r *queryResolver) AdminFoodServiceBanners(ctx context.Context) ([]*model.FSBItem, error) {
	fsbs, err := r.RecipeService.GetAllFSB(ctx)
	if err != nil {
		return nil, err
	}

	var out []*model.FSBItem

	for _, r := range fsbs {
		out = append(out, &model.FSBItem{
			Pk:     r.PK,
			Sk:     r.SK,
			Active: r.Active,
			Team:   r.Team,
			Image:  r.Image,
			URL:    r.URL,
		})
	}

	return out, nil
}

func (r *queryResolver) AdminArm(ctx context.Context) ([]*model.ARMItem, error) {
	arms, err := r.RecipeService.GetAllARM(ctx)
	if err != nil {
		return nil, err
	}

	var out []*model.ARMItem

	for _, arm := range arms {
		out = append(out, &model.ARMItem{
			Pk:     arm.PK,
			Sk:     arm.SK,
			Active: arm.Active,
			Image:  arm.Image,
			Qr:     arm.QR,
			Team:   arm.Team,
			URL:    arm.URL,
			Week:   arm.Week,
		})
	}

	return out, nil
}
