package scalars

import (
	"fmt"
	"io"
	"strconv"

	"github.com/99designs/gqlgen/graphql"
	"github.com/gofrs/uuid"
)

// MarshalID Lets redefine the base ID type to use a uuid as string
func MarshalID(id uuid.UUID) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		io.WriteString(w, strconv.Quote(id.String()))
	})
}

// UnmarshalID And the same for the unmarshaler
func UnmarshalID(v interface{}) (uuid.UUID, error) {
	switch v := v.(type) {
	case string:
		return uuid.Must(uuid.FromString(v)), nil
	default:
		return uuid.Nil, fmt.Errorf("%s is not a string", v)
	}
}
