package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"strconv"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/generated"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	gonanoid "github.com/matoous/go-nanoid/v2"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

func (r *mutationResolver) Ac(ctx context.Context, input model.Na) (*model.SuccessMessage, error) {
	if input.Password != input.PasswordConfirm {
		return nil, entity.ErrInvalidConfirmPassword
	}

	admin, err := entity.CreateAdmin(input)
	if err != nil {
		return nil, err
	}

	admin.CreatedAt = time.Now().In(r.Loc).Format(r.TSFormat)

	err = r.AdminService.PutAdmin(ctx, admin)
	if err != nil {
		return nil, entity.ErrExists
	}

	return &model.SuccessMessage{
		Success: true,
		Message: "admin created",
	}, nil
}

func (r *mutationResolver) ProcessDapper(ctx context.Context) (*model.SuccessMessage, error) {
	dapperEntries, err := r.DapperService.GetAll(ctx)
	if err != nil {
		return nil, entity.ErrInvalid
	}

	i := 0

	for _, e := range dapperEntries {
		if e.Token == nil || *e.Token == "" {
			token, err := gonanoid.New()
			if err != nil {
				return nil, entity.ErrInvalid
			}

			e.Token = &token

			err = r.DapperService.Upsert(ctx, *e)
			if err != nil {
				return nil, entity.ErrInvalid
			}

			err = r.MailerService.DapperUpdate(ctx, *e)
			if err != nil {
				return nil, entity.ErrInvalid
			}

			i++
		}
	}

	return &model.SuccessMessage{
		Success: true,
		Message: strconv.Itoa(i) + " dapper entries processed",
	}, nil
}

func (r *mutationResolver) Al(ctx context.Context, input model.Al) (*model.AdminLogin, error) {
	admin, err := r.AdminService.GetAdmin(ctx, entity.CleanEmail(input.Email))
	if err != nil {
		return nil, entity.ErrInvalid
	}

	err = bcrypt.CompareHashAndPassword([]byte(admin.Password), []byte(input.Password))
	if err != nil {
		return nil, entity.ErrUnauthorized
	}

	tokenString, err := r.AuthService.GenerateToken(1, admin.PK, entity.RoleAdminKey)
	if err != nil {
		return nil, err
	}

	return &model.AdminLogin{
		Token:     tokenString,
		Email:     admin.Email,
		FirstName: admin.FirstName,
		LastName:  admin.LastName,
	}, nil
}

func (r *mutationResolver) BanUser(ctx context.Context, input model.SearchConsumer) (*model.SuccessMessage, error) {
	consumer, err := r.ConsumerService.GetConsumer(ctx, entity.CleanEmail(input.Email))
	if err != nil || consumer == nil {
		return nil, entity.ErrNotFound
	}

	consumer.Status = "banned"

	r.ConsumerService.UpsertConsumer(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	return &model.SuccessMessage{
		Success: true,
		Message: "User has been banned.",
	}, nil
}

func (r *mutationResolver) GenerateUserReset(ctx context.Context, input model.SearchConsumer) (*model.ResetItem, error) {
	consumer, err := r.ConsumerService.GetConsumer(ctx, entity.CleanEmail(input.Email))
	if err != nil {
		return nil, entity.ErrNotFound
	}

	err = entity.ConsumerCheckStatus(*consumer)
	if err != nil {
		return nil, err
	}

	reset, err := entity.CreateReset(consumer.PK, entity.RoleUserKey)
	if err != nil {
		return nil, err
	}

	reset.ExpiresAt = time.Now().In(r.Loc).Add(30 * time.Minute)
	reset.CreatedAt = time.Now().In(r.Loc).Format(r.TSFormat)

	err = r.AuthService.PutReset(ctx, reset)
	if err != nil {
		return nil, err
	}

	resetURL := r.BaseURL + "/reset-password?token=" + reset.Token

	return &model.ResetItem{
		URL: resetURL,
	}, nil
}

func (r *mutationResolver) GrantPoints(ctx context.Context, input model.GrantPointsInput) (*model.AdminConsumerSearch, error) {
	if input.Points < 0 {
		return nil, entity.ErrInputViolation
	}

	admin := currentAdmin(ctx)

	consumer, err := r.ConsumerService.GetConsumer(ctx, entity.CleanEmail(input.Email))
	if err != nil {
		return nil, entity.ErrNotFound
	}

	err = entity.ConsumerCheckStatus(*consumer)
	if err != nil {
		return nil, err
	}

	time := time.Now().In(r.Loc)
	timestamp := time.Format(r.TSFormat)
	// dayOfWeek := time.Weekday().String()

	entry := entity.CreateAdminPointsEntry(*consumer, timestamp, input.Points)
	entry.Requester = &admin.Email

	consumer.CurrentPoints = consumer.CurrentPoints + entry.Points
	consumer.TotalPoints = consumer.TotalPoints + entry.Points

	err = r.EntryService.PutAdminPointEntry(ctx, entry, *consumer)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	err = r.MetricsService.UpdateMetricsAdminPoints(ctx, entry.Points)
	if err != nil {
		logrus.Error(err)
		// return nil, err
	}

	weeklyEntries := []*model.WeeklySweepsEntryItem{}
	for _, e := range consumer.WeeklySweepsEntries.Entries {
		weeklyEntries = append(weeklyEntries, &model.WeeklySweepsEntryItem{
			WeekStartDate: e.WeekStartDate,
			Count:         e.Count,
		})
	}

	adminEntries, err := r.EntryService.GetAdminPointsEntries(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	pointsEntries, err := r.EntryService.GetPointsEntries(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	sweepsEntries, err := r.EntryService.GetSweepsEntries(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	redemptions, err := r.EntryService.GetCouponCodeRedemptions(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	var transactions []*model.TransactionItem

	for _, e := range adminEntries {
		transactions = append(transactions, &model.TransactionItem{
			Type:   "adminPoints",
			Points: e.Points,
			Date:   e.CreatedAt,
		})
	}

	for _, e := range pointsEntries {
		transactions = append(transactions, &model.TransactionItem{
			Type:      "points",
			Points:    e.Points,
			Winner:    &e.Winner,
			Prize:     &e.Prize,
			PrizeSlug: &e.PrizeSlug,
			Date:      e.CreatedAt,
		})
	}

	for _, e := range sweepsEntries {
		transactions = append(transactions, &model.TransactionItem{
			Type:    "sweeps",
			Entries: &e.Count,
			Points:  e.TotalCost,
			Date:    e.CreatedAt,
		})
	}

	for _, e := range redemptions {
		transactions = append(transactions, &model.TransactionItem{
			Type:       "redemption",
			Points:     e.Cost,
			CouponCode: &e.Code,
			CouponURL:  &e.URL,
			Date:       e.CreatedAt,
		})
	}

	validation := &model.MGValidationResponseItem{}
	if consumer.EmailValidation != nil {
		validation.Pk = consumer.EmailValidation.PK
		validation.Sk = consumer.EmailValidation.SK
		validation.Address = consumer.EmailValidation.Address
		validation.IsDisposable = consumer.EmailValidation.IsDisposable
		validation.IsRoleAddress = consumer.EmailValidation.IsRoleAddress
		validation.Reason = consumer.EmailValidation.Reason
		validation.Result = consumer.EmailValidation.Result
		validation.Risk = consumer.EmailValidation.Risk
	}

	return &model.AdminConsumerSearch{
		Consumer: &model.ConsumerItem{
			Pk:                  consumer.PK,
			Sk:                  consumer.SK,
			Email:               consumer.OriginalEmail,
			FirstName:           consumer.FirstName,
			LastName:            consumer.LastName,
			Phone:               consumer.Phone,
			Address:             consumer.Address,
			Address2:            consumer.Address2,
			City:                consumer.City,
			State:               consumer.State,
			Zipcode:             consumer.Zipcode,
			Division:            consumer.Division,
			Birthdate:           consumer.Birthdate,
			Team:                consumer.Team,
			Rules:               consumer.Rules,
			Newsletter:          consumer.Newsletter,
			Salesforce:          consumer.Salesforce,
			SalesforceMessage:   consumer.SalesforceMessage,
			CurrentPoints:       consumer.CurrentPoints,
			TotalPoints:         consumer.TotalPoints,
			WeeklySweepsEntries: weeklyEntries,
			Language:            consumer.Language,
			Status:              consumer.Status,
			EmailValidation:     validation,
			CreatedAt:           consumer.CreatedAt,
		},
		Transactions: transactions,
	}, nil
}

func (r *mutationResolver) ModifyUser(ctx context.Context, input model.ModifyUserItem) (*model.AdminConsumerSearch, error) {
	consumer, err := r.ConsumerService.GetConsumer(ctx, entity.CleanEmail(input.Email))
	if err != nil {
		return nil, entity.ErrNotFound
	}

	consumer.FirstName = input.FirstName
	consumer.LastName = input.LastName
	consumer.Phone = input.Phone
	consumer.Address = input.Address
	consumer.Address2 = input.Address2
	consumer.City = input.City
	consumer.State = input.State
	consumer.Birthdate = input.Birthdate

	err = consumer.Validate()
	if err != nil {
		return nil, err
	}

	err = r.ConsumerService.UpsertConsumer(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	weeklyEntries := []*model.WeeklySweepsEntryItem{}
	for _, e := range consumer.WeeklySweepsEntries.Entries {
		weeklyEntries = append(weeklyEntries, &model.WeeklySweepsEntryItem{
			WeekStartDate: e.WeekStartDate,
			Count:         e.Count,
		})
	}

	pointsEntries, err := r.EntryService.GetPointsEntries(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	sweepsEntries, err := r.EntryService.GetSweepsEntries(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	redemptions, err := r.EntryService.GetCouponCodeRedemptions(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	var transactions []*model.TransactionItem

	for _, e := range pointsEntries {
		transactions = append(transactions, &model.TransactionItem{
			Type:      "points",
			Points:    e.Points,
			Winner:    &e.Winner,
			Prize:     &e.Prize,
			PrizeSlug: &e.PrizeSlug,
			Date:      e.CreatedAt,
		})
	}

	for _, e := range sweepsEntries {
		transactions = append(transactions, &model.TransactionItem{
			Type:    "sweeps",
			Entries: &e.Count,
			Points:  e.TotalCost,
			Date:    e.CreatedAt,
		})
	}

	for _, e := range redemptions {
		transactions = append(transactions, &model.TransactionItem{
			Type:       "redemption",
			Points:     e.Cost,
			CouponCode: &e.Code,
			CouponURL:  &e.URL,
			Date:       e.CreatedAt,
		})
	}

	validation := &model.MGValidationResponseItem{}
	if consumer.EmailValidation != nil {
		validation.Pk = consumer.EmailValidation.PK
		validation.Sk = consumer.EmailValidation.SK
		validation.Address = consumer.EmailValidation.Address
		validation.IsDisposable = consumer.EmailValidation.IsDisposable
		validation.IsRoleAddress = consumer.EmailValidation.IsRoleAddress
		validation.Reason = consumer.EmailValidation.Reason
		validation.Result = consumer.EmailValidation.Result
		validation.Risk = consumer.EmailValidation.Risk
	}

	return &model.AdminConsumerSearch{
		Consumer: &model.ConsumerItem{
			Pk:                  consumer.PK,
			Sk:                  consumer.SK,
			Email:               consumer.OriginalEmail,
			FirstName:           consumer.FirstName,
			LastName:            consumer.LastName,
			Phone:               consumer.Phone,
			Address:             consumer.Address,
			Address2:            consumer.Address2,
			City:                consumer.City,
			State:               consumer.State,
			Zipcode:             consumer.Zipcode,
			Division:            consumer.Division,
			Birthdate:           consumer.Birthdate,
			Team:                consumer.Team,
			Rules:               consumer.Rules,
			Newsletter:          consumer.Newsletter,
			Salesforce:          consumer.Salesforce,
			SalesforceMessage:   consumer.SalesforceMessage,
			CurrentPoints:       consumer.CurrentPoints,
			TotalPoints:         consumer.TotalPoints,
			WeeklySweepsEntries: weeklyEntries,
			Language:            consumer.Language,
			Status:              consumer.Status,
			EmailValidation:     validation,
			CreatedAt:           consumer.CreatedAt,
		},
		Transactions: transactions,
	}, nil
}

func (r *mutationResolver) UnbanUser(ctx context.Context, input model.SearchConsumer) (*model.SuccessMessage, error) {
	consumer, err := r.ConsumerService.GetConsumer(ctx, entity.CleanEmail(input.Email))
	if err != nil || consumer == nil {
		return nil, entity.ErrNotFound
	}

	consumer.Status = "active"

	r.ConsumerService.UpsertConsumer(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	return &model.SuccessMessage{
		Success: true,
		Message: "User has been unbanned.",
	}, nil
}

func (r *queryResolver) FindConsumer(ctx context.Context, input model.SearchConsumer) (*model.AdminConsumerSearch, error) {
	consumer, err := r.ConsumerService.GetConsumer(ctx, entity.CleanEmail(input.Email))
	if err != nil {
		return nil, entity.ErrNotFound
	}

	weeklyEntries := []*model.WeeklySweepsEntryItem{}
	for _, e := range consumer.WeeklySweepsEntries.Entries {
		weeklyEntries = append(weeklyEntries, &model.WeeklySweepsEntryItem{
			WeekStartDate: e.WeekStartDate,
			Count:         e.Count,
		})
	}

	adminEntries, err := r.EntryService.GetAdminPointsEntries(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	pointsEntries, err := r.EntryService.GetPointsEntries(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	sweepsEntries, err := r.EntryService.GetSweepsEntries(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	redemptions, err := r.EntryService.GetCouponCodeRedemptions(ctx, *consumer)
	if err != nil {
		return nil, err
	}

	var transactions []*model.TransactionItem

	for _, e := range adminEntries {
		transactions = append(transactions, &model.TransactionItem{
			Type:   "adminPoints",
			Points: e.Points,
			Date:   e.CreatedAt,
		})
	}

	for _, e := range pointsEntries {
		transactions = append(transactions, &model.TransactionItem{
			Type:      "points",
			Points:    e.Points,
			Winner:    &e.Winner,
			Prize:     &e.Prize,
			PrizeSlug: &e.PrizeSlug,
			Date:      e.CreatedAt,
		})
	}

	for _, e := range sweepsEntries {
		transactions = append(transactions, &model.TransactionItem{
			Type:    "sweeps",
			Entries: &e.Count,
			Points:  e.TotalCost,
			Date:    e.CreatedAt,
		})
	}

	for _, e := range redemptions {
		transactions = append(transactions, &model.TransactionItem{
			Type:       "redemption",
			Points:     e.Cost,
			CouponCode: &e.Code,
			CouponURL:  &e.URL,
			Date:       e.CreatedAt,
		})
	}

	validation := &model.MGValidationResponseItem{}
	if consumer.EmailValidation != nil {
		validation.Pk = consumer.EmailValidation.PK
		validation.Sk = consumer.EmailValidation.SK
		validation.Address = consumer.EmailValidation.Address
		validation.IsDisposable = consumer.EmailValidation.IsDisposable
		validation.IsRoleAddress = consumer.EmailValidation.IsRoleAddress
		validation.Reason = consumer.EmailValidation.Reason
		validation.Result = consumer.EmailValidation.Result
		validation.Risk = consumer.EmailValidation.Risk
	}

	return &model.AdminConsumerSearch{
		Consumer: &model.ConsumerItem{
			Pk:                  consumer.PK,
			Sk:                  consumer.SK,
			Email:               consumer.OriginalEmail,
			FirstName:           consumer.FirstName,
			LastName:            consumer.LastName,
			Phone:               consumer.Phone,
			Address:             consumer.Address,
			Address2:            consumer.Address2,
			City:                consumer.City,
			State:               consumer.State,
			Zipcode:             consumer.Zipcode,
			Division:            consumer.Division,
			Birthdate:           consumer.Birthdate,
			Team:                consumer.Team,
			Rules:               consumer.Rules,
			Newsletter:          consumer.Newsletter,
			Salesforce:          consumer.Salesforce,
			SalesforceMessage:   consumer.SalesforceMessage,
			CurrentPoints:       consumer.CurrentPoints,
			TotalPoints:         consumer.TotalPoints,
			WeeklySweepsEntries: weeklyEntries,
			Language:            consumer.Language,
			Status:              consumer.Status,
			EmailValidation:     validation,
			CreatedAt:           consumer.CreatedAt,
		},
		Transactions: transactions,
	}, nil
}

func (r *queryResolver) FindCoupon(ctx context.Context, input model.SearchCoupon) (*model.CouponItem, error) {
	if input.Tier != nil && *input.Tier != 0 {
		coupon, err := r.EntryService.GetCouponCode(ctx, *input.Tier, "CLAIMED", input.Code)
		if err != nil && err != entity.ErrNotFound {
			return nil, err
		}

		if coupon != nil {
			return &model.CouponItem{
				Pk:        coupon.PK,
				Sk:        coupon.SK,
				Tier:      coupon.Tier,
				Code:      coupon.Code,
				URL:       coupon.URL,
				Consumer:  coupon.Consumer,
				Used:      coupon.Used,
				UsedAt:    coupon.UsedAt,
				Requester: coupon.Requester,
				Source:    coupon.Source,
			}, nil
		}

		coupon, err = r.EntryService.GetCouponCode(ctx, *input.Tier, "UNCLAIMED", input.Code)
		if err != nil && err != entity.ErrNotFound {
			return nil, err
		}

		if coupon != nil {
			return &model.CouponItem{
				Pk:        coupon.PK,
				Sk:        coupon.SK,
				Tier:      coupon.Tier,
				Code:      coupon.Code,
				URL:       coupon.URL,
				Consumer:  coupon.Consumer,
				Used:      coupon.Used,
				UsedAt:    coupon.UsedAt,
				Requester: coupon.Requester,
				Source:    coupon.Source,
			}, nil
		}

		return nil, entity.ErrNotFound
	}

	var tiers []int = []int{1, 2, 3, 4, 5, 6, 7}
	for _, v := range tiers {
		coupon, err := r.EntryService.GetCouponCode(ctx, v, "CLAIMED", input.Code)
		if err != nil && err != entity.ErrNotFound {
			return nil, err
		}

		if coupon != nil {
			return &model.CouponItem{
				Pk:        coupon.PK,
				Sk:        coupon.SK,
				Tier:      coupon.Tier,
				Code:      coupon.Code,
				URL:       coupon.URL,
				Consumer:  coupon.Consumer,
				Used:      coupon.Used,
				UsedAt:    coupon.UsedAt,
				Requester: coupon.Requester,
				Source:    coupon.Source,
			}, nil
		}

		coupon, err = r.EntryService.GetCouponCode(ctx, v, "UNCLAIMED", input.Code)
		if err != nil && err != entity.ErrNotFound {
			return nil, err
		}

		if coupon != nil {
			return &model.CouponItem{
				Pk:        coupon.PK,
				Sk:        coupon.SK,
				Tier:      coupon.Tier,
				Code:      coupon.Code,
				URL:       coupon.URL,
				Consumer:  coupon.Consumer,
				Used:      coupon.Used,
				UsedAt:    coupon.UsedAt,
				Requester: coupon.Requester,
				Source:    coupon.Source,
			}, nil
		}
	}

	return nil, entity.ErrNotFound
}

func (r *queryResolver) ListValidations(ctx context.Context) ([]*model.MGValidationResponseItem, error) {
	validations, err := r.MetricsService.GetValidationsList(ctx)
	if err != nil {
		return nil, err
	}

	validationsList := []*model.MGValidationResponseItem{}

	for _, v := range validations {
		validationsList = append(validationsList, &model.MGValidationResponseItem{
			Pk:            v.PK,
			Sk:            v.SK,
			Address:       v.Address,
			IsDisposable:  v.IsDisposable,
			IsRoleAddress: v.IsRoleAddress,
			Reason:        v.Reason,
			Result:        v.Result,
			Risk:          v.Risk,
		})
	}

	return validationsList, nil
}

func (r *queryResolver) ListWinners(ctx context.Context) ([]*model.WinnerItem, error) {
	winners, err := r.EntryService.GetWinnersList(ctx)
	if err != nil {
		return nil, err
	}

	winList := []*model.WinnerItem{}

	for _, w := range winners {
		winList = append(winList, &model.WinnerItem{
			Pk:        w.PK,
			Sk:        w.SK,
			Email:     w.Email,
			FirstName: w.FirstName,
			LastName:  w.LastName,
			Phone:     w.Phone,
			Address:   w.Address,
			Address2:  w.Address2,
			City:      w.City,
			State:     w.State.String(),
			Zipcode:   w.Zipcode,
			Division:  w.Division,
			Birthdate: w.Birthdate,
			AmoeCode:  w.AmoeCode,
			Team:      w.Team.String(),
			Points:    w.Points,
			Winner:    w.Winner,
			Prize:     w.Prize,
			PrizeSlug: w.PrizeSlug,
			Language:  w.Language.String(),
			CreatedAt: w.CreatedAt,
		})
	}

	return winList, nil
}

func (r *queryResolver) ReportsMetricsDaily(ctx context.Context, input model.DateRange) ([]*entity.MetricsDaily, error) {
	metrics, err := r.MetricsService.GetMetricsDaily(ctx, input.Start, input.End)
	if err != nil {
		return nil, err
	}

	return metrics, nil
}

func (r *queryResolver) ReportsMetricsDomains(ctx context.Context, input model.DateRange) ([]*entity.MetricsDomains, error) {
	metrics, err := r.MetricsService.GetMetricsDomains(ctx, input.Start, input.End)
	if err != nil {
		return nil, err
	}

	return metrics, nil
}

func (r *queryResolver) ReportsMetricsTotals(ctx context.Context) (*entity.MetricsTotals, error) {
	metrics, err := r.MetricsService.GetMetricsTotals(ctx)
	if err != nil {
		return nil, err
	}

	return metrics, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
