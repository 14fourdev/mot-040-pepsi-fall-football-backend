package graph

import (
	"context"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/admin"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/auth"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/banlist"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/consumer"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/dapper"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/entry"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/mailer"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/metrics"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/recaptcha"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/recipe"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/requester"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/salesforce"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/usecase/southernMade"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	AdminService        *admin.Service
	AuthService         *auth.Service
	BaseURL             string
	BanlistService      *banlist.Service
	ConsumerService     *consumer.Service
	DapperService       *dapper.Service
	EntryService        *entry.Service
	Loc                 *time.Location
	MailerService       *mailer.Service
	MetricsService      *metrics.Service
	RecaptchaService    *recaptcha.Service
	RecipeService       *recipe.Service
	RequesterService    *requester.Service
	SalesforceService   *salesforce.Service
	SecurityLevel       int
	SouthernMadeService *southernMade.Service
	TSFormat            string
}

// UserCtx exported
type UserCtx struct {
	Admin    *entity.Admin
	Consumer *entity.Consumer
	Role     string
	Referer  string
}

func currentAdmin(ctx context.Context) *entity.Admin {
	return ctx.Value(entity.UserCtxKey).(UserCtx).Admin
}

func currentConsumer(ctx context.Context) *entity.Consumer {
	return ctx.Value(entity.UserCtxKey).(UserCtx).Consumer
}

func currentRequester(ctx context.Context) entity.Requester {
	return ctx.Value(entity.RequesterCtxKey).(entity.Requester)
}
