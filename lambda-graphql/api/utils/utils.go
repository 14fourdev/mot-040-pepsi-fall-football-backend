package api

import (
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
)

// Contains exported
func Contains(x string, a []string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

// ContainsTeam exported
func ContainsTeam(x model.EntryTeam, a []*model.EntryTeam) bool {
	for _, n := range a {
		if x.String() == n.String() {
			return true
		}
	}
	return false
}

// WeekStartDate
func WeekStartDate(date time.Time, weekday int) time.Time {
	offset := (weekday - int(date.Weekday()) - 7) % 7
	result := date.Add(time.Duration(offset*24) * time.Hour)
	return result
}

// Compare string arrays
func Compare(a []string, b []string) bool {
	for _, r := range a {
		for _, v := range b {
			if r == v {
				return true
			}
		}
	}
	return false
}
