package dapper

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

// Service consumer usecase
type Service struct {
	repo Repository
}

// NewService create new use case
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}

// Get
func (s *Service) Get(ctx context.Context, email string) (*entity.DapperEntry, error) {
	return s.repo.Get(ctx, email)
}

// GetAll
func (s *Service) GetAll(ctx context.Context) ([]*entity.DapperEntry, error) {
	return s.repo.GetAll(ctx)
}

// Available
func (s *Service) Available(ctx context.Context) (*entity.DapperKits, error) {
	return s.repo.Available(ctx)
}

// Enter
func (s *Service) Enter(ctx context.Context, dapper entity.DapperEntry) (*string, error) {
	return s.repo.Enter(ctx, dapper)
}

// Upsert
func (s *Service) Upsert(ctx context.Context, dapper entity.DapperEntry) error {
	return s.repo.Upsert(ctx, dapper)
}
