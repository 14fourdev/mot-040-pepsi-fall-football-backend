package dapper

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

//Reader interface
type Reader interface {
	Get(ctx context.Context, email string) (*entity.DapperEntry, error)
	GetAll(ctx context.Context) ([]*entity.DapperEntry, error)
	Available(ctx context.Context) (*entity.DapperKits, error)
}

//Writer interface
type Writer interface {
	Enter(ctx context.Context, dapper entity.DapperEntry) (*string, error)
	Upsert(ctx context.Context, dapper entity.DapperEntry) error
}

//Repository interface
type Repository interface {
	Reader
	Writer
}
