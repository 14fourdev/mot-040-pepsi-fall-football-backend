package southernMade

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	"github.com/sirupsen/logrus"
)

// Service admin usecase
type Service struct {
	loc      *time.Location
	baseURL  string
	token    string
	south    string
	bills    string
	bengals  string
	lions    string
	patriots string
	giants   string
	jets     string
	eagles   string
	steelers string
}

// NewService create new use case
func NewService(loc *time.Location, baseURL string, token string, south string, bills string, bengals string, lions string, patriots string, giants string, jets string, eagles string, steelers string) *Service {
	return &Service{
		loc:      loc,
		baseURL:  baseURL,
		token:    token,
		south:    south,
		bills:    bills,
		bengals:  bengals,
		lions:    lions,
		patriots: patriots,
		giants:   giants,
		jets:     jets,
		eagles:   eagles,
		steelers: steelers,
	}
}

// SendPointEntries sends a request to the API for points entries
func (s *Service) SendPointEntries(entry entity.PostPointEntry) (*entity.ResponsePointEntry, error) {
	cat := ""

	if entry.Entry.CampaignCategoryID == "SOUTH" {
		cat = s.south
	} else if entry.Entry.CampaignCategoryID != "" {
		switch entry.Entry.CampaignCategoryID {
		case model.EntryTeamBuffaloBills.String():
			cat = s.bills
		case model.EntryTeamCincinnatiBengals.String():
			cat = s.bengals
		case model.EntryTeamDetroitLions.String():
			cat = s.lions
		case model.EntryTeamNewEnglandPatriots.String():
			cat = s.patriots
		case model.EntryTeamNewYorkGiants.String():
			cat = s.giants
		case model.EntryTeamNewYorkJets.String():
			cat = s.jets
		case model.EntryTeamPhiladelphiaEagles.String():
			cat = s.eagles
		case model.EntryTeamPittsburghSteelers.String():
			cat = s.steelers
		}
	}

	entry.Entry.CampaignCategoryID = cat

	reqBody, err := json.Marshal(entry)
	if err != nil {
		return nil, err
	}

	logrus.Infoln("Request body.", string(reqBody))

	endpoint := s.baseURL + "/point_entries"

	req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(reqBody))
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	req.Header.Set("Authorization", "Bearer "+s.token)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: time.Second * 30}

	resp, err := client.Do(req)
	if err != nil {
		logrus.Error("Error reading response. ", err)
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.Error("Error reading body. ", err)
		return nil, err
	}

	logrus.Infoln("Response body.", string(body))

	smResp := &entity.ResponsePointEntry{}

	err = json.Unmarshal(body, smResp)
	if err != nil {
		return nil, err
	}

	smResp.Status = resp.StatusCode
	if smResp.Status != 200 {
		if len(smResp.Errors.Error) > 0 {
			if strings.Contains(smResp.Errors.Error[0], "Max") {
				return smResp, entity.ErrRateLimit
			}

			return smResp, errors.New(smResp.Errors.Error[0])
		}

		return smResp, entity.ErrInternalError
	}

	return smResp, nil
}

// SendSweepsEntries sends a request to the API for sweeps entries
func (s *Service) SendSweepsEntries(entry entity.PostWeeklySweepstakesEntry) (*entity.ResponseWeeklySweepstakesEntry, error) {
	reqBody, err := json.Marshal(entry)
	if err != nil {
		return nil, err
	}

	logrus.Infoln("Request body.", string(reqBody))

	endpoint := s.baseURL + "/weekly_sweepstakes_entries"

	req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(reqBody))
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	req.Header.Set("Authorization", "Bearer "+s.token)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: time.Second * 30}

	resp, err := client.Do(req)
	if err != nil {
		logrus.Error("Error reading response. ", err)
		return nil, err
	}
	defer resp.Body.Close()

	smResp := &entity.ResponseWeeklySweepstakesEntry{}

	smResp.Status = resp.StatusCode
	if smResp.Status > 299 {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			logrus.Error("Error reading body. ", err)
			return nil, err
		}

		err = json.Unmarshal(body, smResp)
		if err != nil {
			return nil, err
		}

		if len(smResp.Errors.Error) > 0 {
			if strings.Contains(smResp.Errors.Error[0], "Max") {
				return smResp, entity.ErrRateLimit
			}

			return smResp, errors.New(smResp.Errors.Error[0])
		}

		return smResp, entity.ErrInternalError
	}

	return smResp, nil
}
