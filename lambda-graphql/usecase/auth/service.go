package auth

import (
	"context"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/dgrijalva/jwt-go"
)

//Service recaptcha usecase
type Service struct {
	repo   Repository
	loc    *time.Location
	secret string
}

//NewService create new use case
func NewService(r Repository, loc *time.Location, secret string) *Service {
	return &Service{
		repo:   r,
		loc:    loc,
		secret: secret,
	}
}

// GenerateToken generates a token based on inputs
func (s *Service) GenerateToken(days time.Duration, pk string, key string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp":  time.Now().Add(time.Hour * 24 * days).Unix(),
		"pk":   pk,
		"role": key,
	})

	tokenString, err := token.SignedString([]byte(s.secret))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// PutReset stores the reset in the database
func (s *Service) PutReset(ctx context.Context, reset entity.Reset) error {
	return s.repo.PutResetToken(ctx, reset)
}

// GetReset stores the reset in the database
func (s *Service) GetReset(ctx context.Context, token string, role string) (entity.Reset, error) {
	return s.repo.GetResetByToken(ctx, token, role)
}
