package auth

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

//Reader interface
type Reader interface {
	GetResetByToken(ctx context.Context, token string, role string) (entity.Reset, error)
}

//Writer interface
type Writer interface {
	PutResetToken(ctx context.Context, reset entity.Reset) error
}

//Repository interface
type Repository interface {
	Reader
	Writer
}
