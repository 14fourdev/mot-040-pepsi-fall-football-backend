package entry

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

//Reader interface
type Reader interface {
	GetAmoeCode(ctx context.Context, code string) (*entity.AmoeCode, error)
	GetUnclaimedCode(ctx context.Context, tier int) (*entity.CouponCode, error)
	GetAdminPointsEntries(ctx context.Context, consumer entity.Consumer) ([]*entity.AdminPointsEntry, error)
	GetPointsEntries(ctx context.Context, consumer entity.Consumer) ([]*entity.PointsEntry, error)
	GetSweepsEntries(ctx context.Context, consumer entity.Consumer) ([]*entity.SweepsEntry, error)
	GetCouponCodeRedemptions(ctx context.Context, consumer entity.Consumer) ([]*entity.CouponCodeRedemption, error)
	GetWinnersList(ctx context.Context) ([]*entity.Winner, error)
	GetCouponCode(ctx context.Context, tier int, status string, code string) (*entity.CouponCode, error)
}

//Writer interface
type Writer interface {
	PutAmoeCode(ctx context.Context, entry entity.AmoeCode) error
	PutCouponRedemption(ctx context.Context, coupon entity.CouponCode, redemption entity.CouponCodeRedemption, consumer entity.Consumer) error
	PutAdminPointEntry(ctx context.Context, entry entity.AdminPointsEntry, consumer entity.Consumer) error
	PutPointEntry(ctx context.Context, entry entity.PointsEntry, consumer entity.Consumer) error
	PutSweepsEntry(ctx context.Context, entry entity.SweepsEntry, consumer entity.Consumer) error
	RevertSweepsEntry(ctx context.Context, entry entity.SweepsEntry, consumer entity.Consumer) error
}

//Repository interface
type Repository interface {
	Reader
	Writer
}
