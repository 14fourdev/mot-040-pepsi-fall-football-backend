package entry

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

// Service consumer usecase
type Service struct {
	repo Repository
}

// NewService create new use case
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}

// PutAmoeCode stores the amoe code redemption in the database
func (s *Service) PutAmoeCode(ctx context.Context, entry entity.AmoeCode) error {
	return s.repo.PutAmoeCode(ctx, entry)
}

// PutCouponRedemption stores the coupon code redemption in the database
func (s *Service) PutCouponRedemption(ctx context.Context, coupon entity.CouponCode, redemption entity.CouponCodeRedemption, consumer entity.Consumer) error {
	return s.repo.PutCouponRedemption(ctx, coupon, redemption, consumer)
}

// PutAdminPointEntry stores the entry in the database and adds the points to the consumer
func (s *Service) PutAdminPointEntry(ctx context.Context, entry entity.AdminPointsEntry, consumer entity.Consumer) error {
	return s.repo.PutAdminPointEntry(ctx, entry, consumer)
}

// PutPointEntry stores the entry in the database and adds the points to the consumer
func (s *Service) PutPointEntry(ctx context.Context, entry entity.PointsEntry, consumer entity.Consumer) error {
	return s.repo.PutPointEntry(ctx, entry, consumer)
}

// PutSweepsEntry stores the entry in the database and adds the points to the consumer
func (s *Service) PutSweepsEntry(ctx context.Context, entry entity.SweepsEntry, consumer entity.Consumer) error {
	return s.repo.PutSweepsEntry(ctx, entry, consumer)
}

// RevertSweepsEntry reverts the entry in the database and restores points to the consumer
func (s *Service) RevertSweepsEntry(ctx context.Context, entry entity.SweepsEntry, consumer entity.Consumer) error {
	return s.repo.RevertSweepsEntry(ctx, entry, consumer)
}

// GetAmoeCode gets the amoe code from the database
func (s *Service) GetAmoeCode(ctx context.Context, code string) (*entity.AmoeCode, error) {
	return s.repo.GetAmoeCode(ctx, code)
}

// GetUnclaimedCode gets an unclaimed coupon code from the database
func (s *Service) GetUnclaimedCode(ctx context.Context, tier int) (*entity.CouponCode, error) {
	return s.repo.GetUnclaimedCode(ctx, tier)
}

// GetAdminPointsEntries gets the points from the database for the consumer
func (s *Service) GetAdminPointsEntries(ctx context.Context, consumer entity.Consumer) ([]*entity.AdminPointsEntry, error) {
	return s.repo.GetAdminPointsEntries(ctx, consumer)
}

// GetPointsEntries gets the points from the database for the consumer
func (s *Service) GetPointsEntries(ctx context.Context, consumer entity.Consumer) ([]*entity.PointsEntry, error) {
	return s.repo.GetPointsEntries(ctx, consumer)
}

// GetSweepsEntries gets the points from the database for the consumer
func (s *Service) GetSweepsEntries(ctx context.Context, consumer entity.Consumer) ([]*entity.SweepsEntry, error) {
	return s.repo.GetSweepsEntries(ctx, consumer)
}

// GetCouponCodeRedemptions gets the points from the database for the consumer
func (s *Service) GetCouponCodeRedemptions(ctx context.Context, consumer entity.Consumer) ([]*entity.CouponCodeRedemption, error) {
	return s.repo.GetCouponCodeRedemptions(ctx, consumer)
}

// GetWinnersList gets the points from the database for the consumer
func (s *Service) GetWinnersList(ctx context.Context) ([]*entity.Winner, error) {
	return s.repo.GetWinnersList(ctx)
}

// GetCouponCode gets a coupon code from the database
func (s *Service) GetCouponCode(ctx context.Context, tier int, status string, code string) (*entity.CouponCode, error) {
	return s.repo.GetCouponCode(ctx, tier, status, code)
}
