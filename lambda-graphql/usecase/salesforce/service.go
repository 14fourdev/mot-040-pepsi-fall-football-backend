package salesforce

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/sirupsen/logrus"
)

// Service admin usecase
type Service struct {
	loc            *time.Location
	authURL        string
	baseURL        string
	profile        string
	token          string
	expires        time.Time
	clientID       string
	clientSecret   string
	accountID      string
	source         string
	subscriptionID string
}

// NewService create new use case
func NewService(loc *time.Location, authUrl string, accountID string, clientID string, clientSecret string, profile string, source string, subscriptionID string) *Service {
	return &Service{
		loc:            loc,
		authURL:        authUrl,
		accountID:      accountID,
		clientID:       clientID,
		clientSecret:   clientSecret,
		profile:        profile,
		expires:        time.Now().In(loc),
		source:         source,
		subscriptionID: subscriptionID,
	}
}

func (s *Service) SubmitStrings(ctx context.Context, email string, firstName string, lastName string, birthdate string, state string, zipcode string, phone string, newsletter bool) (*bool, *string, error) {
	sfRequests := entity.SFRequests{}

	optIn := entity.OptIns{
		FirstName:     firstName,
		LastName:      lastName,
		Birthdate:     birthdate,
		StateProvince: state,
		ZipPostalCode: zipcode,
		Country:       "US",
		PhoneNumber:   phone,
	}

	if newsletter {
		optIn.OptIns = s.subscriptionID
	}

	req := s.newRequest(email, optIn)

	sfRequests = append(sfRequests, req)

	responses, err := s.submit(ctx, sfRequests)
	if err != nil {
		return nil, nil, err
	}

	success := true
	timestamp := ""

	for _, r := range responses {
		timestamp = r.SFKeys.TimeStamp
	}

	return &success, &timestamp, nil
}

func (s *Service) SubmitConsumer(ctx context.Context, consumer entity.Consumer) (entity.Consumer, error) {
	sfRequests := entity.SFRequests{}

	optIn := entity.OptIns{
		FirstName: consumer.FirstName,
		LastName:  consumer.LastName,
		Birthdate: consumer.Birthdate,
		// StateProvince: string(consumer.State),
		ZipPostalCode: consumer.Zipcode,
		Country:       "US",
		PhoneNumber:   consumer.Phone,
	}

	if consumer.Newsletter {
		optIn.OptIns = s.subscriptionID
	}

	req := s.newRequest(consumer.Email, optIn)

	sfRequests = append(sfRequests, req)

	responses, err := s.submit(ctx, sfRequests)
	if err != nil {
		return consumer, err
	}

	for _, r := range responses {
		consumer.Salesforce = true
		consumer.SalesforceMessage = r.SFKeys.TimeStamp
	}

	return consumer, nil
}

// submit posts the given reqest to salesforce
func (s *Service) submit(ctx context.Context, body entity.SFRequests) (entity.SFResponses, error) {
	client := &http.Client{
		Timeout: time.Second * 5,
	}
	if time.Now().After(s.expires) {
		err := s.authorize()
		if err != nil {
			return entity.SFResponses{}, err
		}
	}

	jsonStr, err := json.Marshal(body)
	if err != nil {
		return entity.SFResponses{}, err
	}

	postUrl := s.baseURL + "hub/v1/dataevents/key:" + s.profile + "/rowset"

	req, err := http.NewRequest("POST", postUrl, bytes.NewBuffer(jsonStr))
	if err != nil {
		logrus.Error("Saleforce, submitting", err)
		return entity.SFResponses{}, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+s.token)
	resp, err := client.Do(req)
	if err != nil {
		logrus.Error("Saleforce, submitting", err)
		return entity.SFResponses{}, err
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		logrus.Error("Saleforce, reading body", err)
		return entity.SFResponses{}, err
	}
	resp.Body.Close()
	if resp.StatusCode > 299 {
		errResponse := &entity.SFErrorResponse{}

		err = json.Unmarshal(respBody, errResponse)
		if err != nil {
			return entity.SFResponses{}, err
		}

		return entity.SFResponses{}, errors.New("SF ERROR: " + errResponse.Message)
	}

	response := &entity.SFResponses{}
	err = json.Unmarshal(respBody, response)
	if err != nil {
		return entity.SFResponses{}, err
	}

	return *response, nil
}

// authorize passes credentials to saleforce and retrieves the proper JWT server to server token
func (s *Service) authorize() error {
	form := url.Values{}
	form.Add("grant_type", "client_credentials")
	form.Add("scope", "data_extensions_read data_extensions_write")
	form.Add("client_id", s.clientID)
	form.Add("client_secret", s.clientSecret)
	form.Add("account_id", s.accountID)

	resp, err := http.PostForm(s.authURL+"/v2/token", form)
	if err != nil {
		logrus.Error("Salesforce Authorizing token", err)
		return err
	}
	defer resp.Body.Close()

	response := entity.AuthResponse{}
	json.NewDecoder(resp.Body).Decode(&response)

	s.token = response.Token
	s.baseURL = response.RestURL
	s.expires = time.Now().Add(time.Duration(response.Expires) * time.Second).Add(-1 * time.Minute)

	return nil
}

// newRequest Wraps the values with a valid request, the proper SourceID and timestamp
func (s Service) newRequest(email string, value entity.SFValue) entity.SFRequest {
	return entity.SFRequest{
		Keys: entity.SFKeys{
			EmailAddress: email,
			SourceID:     s.source,
			TimeStamp:    time.Now().Format("2006-01-02 3:04:05 PM"),
		},
		Values: value,
	}
}
