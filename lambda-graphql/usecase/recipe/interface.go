package recipe

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
)

//Reader interface
type Reader interface {
	Get(ctx context.Context) ([]*entity.Recipe, error)
	GetFeaturedRecipe(ctx context.Context, date string, team model.EntryTeam) (*entity.FeaturedRecipeWeekly, error)
	GetAllFeaturedRecipes(ctx context.Context) ([]*entity.FeaturedRecipeWeekly, error)
	GetFSB(ctx context.Context, team model.EntryTeam) (*entity.FoodServiceBanner, error)
	GetAllFSB(ctx context.Context) ([]*entity.FoodServiceBanner, error)
	GetARM(ctx context.Context, team model.EntryTeam, week string) (*entity.ARModule, error)
	GetAllARM(ctx context.Context) ([]*entity.ARModule, error)
}

//Writer interface
type Writer interface {
	Upsert(ctx context.Context, recipe entity.Recipe) error
	Delete(ctx context.Context, slug string) error
	DeleteFSB(ctx context.Context, team model.EntryTeam) error
	DeleteARM(ctx context.Context, team model.EntryTeam, week string) error
	UpsertFeaturedRecipe(ctx context.Context, fr entity.FeaturedRecipeWeekly) error
	InsertFeaturedRecipe(ctx context.Context, fr entity.FeaturedRecipeWeekly) error
	UpsertFSB(ctx context.Context, fr entity.FoodServiceBanner) error
	InsertFSB(ctx context.Context, fr entity.FoodServiceBanner) error
	UpsertARM(ctx context.Context, arm entity.ARModule) error
	InsertARM(ctx context.Context, arm entity.ARModule) error
}

//Repository interface
type Repository interface {
	Reader
	Writer
}

//Writer interface
type StorageWriter interface {
	PutImage(ctx context.Context, filename string, file []byte, contentType string) (*string, error)
}

//StorageRepository interface
type StorageRepository interface {
	StorageWriter
}
