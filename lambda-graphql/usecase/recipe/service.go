package recipe

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
)

// Service consumer usecase
type Service struct {
	repo    Repository
	storage StorageRepository
}

// NewService create new use case
func NewService(r Repository, s StorageRepository) *Service {
	return &Service{
		repo:    r,
		storage: s,
	}
}

// Get
func (s *Service) Get(ctx context.Context) ([]*entity.Recipe, error) {
	return s.repo.Get(ctx)
}

// GetFeaturedRecipe
func (s *Service) GetFeaturedRecipe(ctx context.Context, date string, team model.EntryTeam) (*entity.FeaturedRecipeWeekly, error) {
	return s.repo.GetFeaturedRecipe(ctx, date, team)
}

// GetFeaturedRecipe
func (s *Service) GetAllFeaturedRecipes(ctx context.Context) ([]*entity.FeaturedRecipeWeekly, error) {
	return s.repo.GetAllFeaturedRecipes(ctx)
}

// GetFSB
func (s *Service) GetFSB(ctx context.Context, team model.EntryTeam) (*entity.FoodServiceBanner, error) {
	return s.repo.GetFSB(ctx, team)
}

// GetAllFSB
func (s *Service) GetAllFSB(ctx context.Context) ([]*entity.FoodServiceBanner, error) {
	return s.repo.GetAllFSB(ctx)
}

// GetARM
func (s *Service) GetARM(ctx context.Context, team model.EntryTeam, week string) (*entity.ARModule, error) {
	return s.repo.GetARM(ctx, team, week)
}

// GetAllARM
func (s *Service) GetAllARM(ctx context.Context) ([]*entity.ARModule, error) {
	return s.repo.GetAllARM(ctx)
}

// Upsert
func (s *Service) Upsert(ctx context.Context, recipe entity.Recipe) error {
	return s.repo.Upsert(ctx, recipe)
}

// Delete
func (s *Service) Delete(ctx context.Context, slug string) error {
	return s.repo.Delete(ctx, slug)
}

// Delete
func (s *Service) DeleteFSB(ctx context.Context, team model.EntryTeam) error {
	return s.repo.DeleteFSB(ctx, team)
}

// Delete
func (s *Service) DeleteARM(ctx context.Context, team model.EntryTeam, week string) error {
	return s.repo.DeleteARM(ctx, team, week)
}

// PutImage
func (s *Service) PutImage(ctx context.Context, filename string, file []byte, contentType string) (*string, error) {
	return s.storage.PutImage(ctx, filename, file, contentType)
}

// UpsertFeaturedRecipe
func (s *Service) UpsertFeaturedRecipe(ctx context.Context, fr entity.FeaturedRecipeWeekly) error {
	return s.repo.UpsertFeaturedRecipe(ctx, fr)
}

// InsertFeaturedRecipe
func (s *Service) InsertFeaturedRecipe(ctx context.Context, fr entity.FeaturedRecipeWeekly) error {
	return s.repo.InsertFeaturedRecipe(ctx, fr)
}

// InsertFSB
func (s *Service) InsertFSB(ctx context.Context, fsb entity.FoodServiceBanner) error {
	return s.repo.InsertFSB(ctx, fsb)
}

// UpsertFSB
func (s *Service) UpsertFSB(ctx context.Context, fsb entity.FoodServiceBanner) error {
	return s.repo.UpsertFSB(ctx, fsb)
}

// InsertARM
func (s *Service) InsertARM(ctx context.Context, arm entity.ARModule) error {
	return s.repo.InsertARM(ctx, arm)
}

// UpsertARM
func (s *Service) UpsertARM(ctx context.Context, arm entity.ARModule) error {
	return s.repo.UpsertARM(ctx, arm)
}
