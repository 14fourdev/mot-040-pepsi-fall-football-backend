package banlist

import (
	"bufio"
	"context"
	"embed"
	"encoding/json"
	"regexp"
	"strings"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/sirupsen/logrus"
)

//go:embed "resources/*"
var f embed.FS

// Service requester usecase
type Service struct {
	repo Repository
	loc  *time.Location
}

// NewService create new use case
func NewService(r Repository, loc *time.Location) *Service {
	return &Service{
		repo: r,
		loc:  loc,
	}
}

// AddBanlistItem stores the requester in the data store
func (s *Service) AddBanlistItem(ctx context.Context, bantype string, banobject string) error {
	err := s.repo.Update(ctx, bantype, banobject)
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) CheckIPBan(ctx context.Context, ip string) error {
	banlist, err := f.Open("resources/ip-banlist.txt")
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(banlist)
	for scanner.Scan() {
		if ip == strings.TrimSpace(scanner.Text()) {
			err = s.AddBanlistItem(ctx, "IP", ip)
			if err != nil {
				return err
			}

			return entity.ErrBanned
		}
	}

	return nil
}

func (s *Service) CheckDomainBan(ctx context.Context, email string, IP string) error {
	patternBan, err := regexp.MatchString(`^(\d{4,}\w+\.\w+)`, email)
	if err != nil {
		return err
	}

	if patternBan {
		err = s.AddBanlistItem(ctx, "EMAIL", strings.TrimSpace(email))
		if err != nil {
			return err
		}

		err = s.AddBanlistItem(ctx, "IP", strings.TrimSpace(IP))
		if err != nil {
			return err
		}

		return entity.ErrBanned
	}

	patternBan, err = regexp.MatchString(`^(\w+\.\w+\d{4,})`, email)
	if err != nil {
		return err
	}

	if patternBan {
		err = s.AddBanlistItem(ctx, "EMAIL", strings.TrimSpace(email))
		if err != nil {
			return err
		}

		err = s.AddBanlistItem(ctx, "IP", strings.TrimSpace(IP))
		if err != nil {
			return err
		}

		return entity.ErrBanned
	}
	patternBan, err = regexp.MatchString(`^(nfl\d{2,}@)`, email)
	if err != nil {
		return err
	}

	if patternBan {
		err = s.AddBanlistItem(ctx, "EMAIL", strings.TrimSpace(email))
		if err != nil {
			return err
		}

		err = s.AddBanlistItem(ctx, "IP", strings.TrimSpace(IP))
		if err != nil {
			return err
		}

		return entity.ErrBanned
	}

	customBanlist, err := f.Open("resources/domain-custom.txt")
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(customBanlist)
	for scanner.Scan() {
		if strings.HasSuffix(email, strings.TrimSpace(scanner.Text())) {
			logrus.Error("IP ADDRESS OF BANNED EMAIL: ", IP, " - ", email)
			err = s.AddBanlistItem(ctx, "DOMAIN", strings.TrimSpace(scanner.Text()))
			if err != nil {
				return err
			}

			return entity.ErrBanned
		}
	}

	banlist, err := f.ReadFile("resources/domain-banlist.json")
	if err != nil {
		return err
	}

	var arr []string

	err = json.Unmarshal([]byte(banlist), &arr)
	if err != nil {
		return err
	}

	for _, v := range arr {
		if strings.HasSuffix(email, strings.TrimSpace(v)) {
			logrus.Error("IP ADDRESS OF BANNED EMAIL: ", IP, " - ", email)
			err = s.AddBanlistItem(ctx, "DOMAIN", strings.TrimSpace(v))
			if err != nil {
				return err
			}

			return entity.ErrBanned
		}
	}

	return nil
}
