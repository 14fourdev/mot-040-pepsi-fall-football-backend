package banlist

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/service/wafv2/types"
)

//Writer book writer
type Writer interface {
	Update(ctx context.Context, bantype string, banobject string) error
}

//Repository interface
type Repository interface {
	Writer
}

//Repository interface
type WAFRepository interface {
	ReaderWAF
	WriterWAF
}

type ReaderWAF interface {
	GetIPSet(ctx context.Context, id *string, name *string) (*types.IPSet, *string, error)
}

type WriterWAF interface {
	UpdateIPSet(ctx context.Context, id *string, name *string, token *string, addresses []string) (*string, error)
}
