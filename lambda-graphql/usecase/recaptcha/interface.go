package recaptcha

import "bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"

//Reader recapcha reader
type Reader interface {
	Confirm(ip string, captcha string) (*entity.RecaptchaResponse, error)
}

//Repository interface
type Repository interface {
	Reader
}
