package recaptcha

import "bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"

//Service recaptcha usecase
type Service struct {
	repo Repository
}

//NewService create new use case
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}

// PutRequester stores the requester in the data store
func (s *Service) CheckRecaptcha(ip string, captcha string) (*entity.RecaptchaResponse, error) {
	return s.repo.Confirm(ip, captcha)
}
