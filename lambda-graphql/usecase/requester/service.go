package requester

import (
	"context"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

// Service requester usecase
type Service struct {
	repo     Repository
	loc      *time.Location
	tsFormat string
}

// NewService create new use case
func NewService(r Repository, loc *time.Location, tsFormat string) *Service {
	return &Service{
		repo:     r,
		loc:      loc,
		tsFormat: tsFormat,
	}
}

// StoreRequester stores the requester in the data store
func (s *Service) StoreRequester(ctx context.Context, requester entity.Requester) error {
	timestamp := time.Now().In(s.loc).Format(s.tsFormat)

	requester.CreatedAt = timestamp

	err := s.repo.Put(ctx, requester)
	if err != nil {
		return err
	}

	return nil
}
