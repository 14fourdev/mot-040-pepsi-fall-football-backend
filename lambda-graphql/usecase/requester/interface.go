package requester

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

//Writer requester writer
type Writer interface {
	Put(ctx context.Context, requester entity.Requester) error
}

//Repository interface
type Repository interface {
	Writer
}
