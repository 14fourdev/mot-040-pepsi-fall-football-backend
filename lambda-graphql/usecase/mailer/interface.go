package mailer

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

//Sender mailer writer
type Sender interface {
	Send(ctx context.Context, email string, template string, subject string, data map[string]interface{}) error
	ValidateEmail(ctx context.Context, email string) (*entity.MGValidationResponse, error)
}

//Repository interface
type Repository interface {
	Sender
}
