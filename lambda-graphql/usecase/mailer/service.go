package mailer

import (
	"context"
	"embed"
	"encoding/csv"
	"io"
	"strings"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/sirupsen/logrus"
)

//go:embed "resources/*"
var f embed.FS

// Service requester usecase
type Service struct {
	repo    Repository
	baseUrl string
}

// NewService create new use case
func NewService(r Repository, baseUrl string) *Service {
	return &Service{
		repo:    r,
		baseUrl: baseUrl,
	}
}

// ValidateEmail validates an email address
func (s *Service) ValidateEmail(ctx context.Context, email string) (*entity.MGValidationResponse, error) {
	return s.repo.ValidateEmail(ctx, email)
}

// Registered sends the post-registration email
func (s *Service) Registered(ctx context.Context, consumer entity.Consumer) error {
	template := "registration"
	subject := "Welcome to Pepsi® Made For Football Watching"

	if consumer.Language == "es" {
		template = "es-" + template
		subject = "Bienvenido a Made for Football Watching"
	}

	return s.repo.Send(ctx, consumer.Email, template, subject, map[string]interface{}{})
}

// ForgotPassword sends the forgot password email
func (s *Service) ForgotPassword(ctx context.Context, consumer entity.Consumer, token string) error {
	template := "reset-password"
	subject := "Forget Your Password?"

	if consumer.Language == "es" {
		template = "es-" + template
		subject = "¿Olvidaste tu contraseña?"
	}

	return s.repo.Send(ctx, consumer.Email, template, subject, map[string]interface{}{
		"actionUrl": s.baseUrl + "/reset-password?token=" + token,
	})
}

// InstantWin sends the instant win email
func (s *Service) InstantWin(ctx context.Context, consumer entity.Consumer, entry entity.PointsEntry) error {
	base := "instant-win"
	subject := "You Won! Made For Football Watching Prize Details Enclosed"

	if consumer.Language == "es" {
		subject = "¡Ganaste!"
	}

	template, prizeName, err := s.translatePrizes(consumer, &base, &entry)
	if err != nil {
		logrus.Error(err)
		logrus.Info(template, prizeName)
	}

	return s.repo.Send(ctx, consumer.Email, *template, subject, map[string]interface{}{
		"actionUrl": s.baseUrl,
		"prize":     prizeName,
		"prizeSlug": entry.PrizeSlug,
		"points":    entry.Points,
	})
}

// DapperWin sends the instant win email
func (s *Service) DapperWin(ctx context.Context, dapper entity.DapperEntry) error {
	template := "dapper-dan"
	subject := "SCORE! You secured The Pepsi x Dapper Dan Football Watching Capsule Collection."
	prizeName := "Pepsi x Dapper Dan Football Watching Capsule Collection"

	return s.repo.Send(ctx, dapper.OriginalEmail, template, subject, map[string]interface{}{
		"actionUrl": s.baseUrl,
		"prizeName": prizeName,
	})
}

// DapperUpdate
func (s *Service) DapperUpdate(ctx context.Context, dapper entity.DapperEntry) error {
	template := "dapper-confirm-address"
	subject := "Let's try this again-please confirm your address"

	return s.repo.Send(ctx, dapper.OriginalEmail, template, subject, map[string]interface{}{
		"actionUrl": s.baseUrl,
		"token":     dapper.Token,
	})
}

// PointRedemption sends the point redemption email
func (s *Service) PointRedemption(ctx context.Context, consumer entity.Consumer, url string, tier int) error {
	template := "redemption"
	subject := "Gear Up! Redeem Your NFLShop.com Points Now!"
	prizeText := "prizes"

	switch tier {
	case 1:
		prizeText = "\"DECAL OR CAN COOLER\""
	case 2:
		prizeText = "\"PINT GLASS OR BLANKET\""
	case 3:
		prizeText = "\"SHORT SLEEVED T-SHIRT\""
	case 4:
		prizeText = "\"HAT OR BEANIE\""
	case 5:
		prizeText = "\"LONG SLEEVED T-SHIRT\""
	case 6:
		prizeText = "\"HOODIE\""
	case 7:
		prizeText = "\"NIKE GAME JERSEY\""
	}

	if consumer.Language == "es" {
		template = "es-" + template
		subject = "Canje de puntos de Pepsi Made For Football Watching"
		prizeText = "premios"

		switch tier {
		case 1:
			prizeText = "\"CALCOMANÍA O ENFRIADOR DE LATAS\""
		case 2:
			prizeText = "\"VASO TIPO PINTA O MANTA\""
		case 3:
			prizeText = "\"CAMISETA MANGA CORTA\""
		case 4:
			prizeText = "\"GORRA O BOINA\""
		case 5:
			prizeText = "\"CAMISETA MANGA LARGA\""
		case 6:
			prizeText = "\"SUDADERA CON CAPUCHA\""
		case 7:
			prizeText = "\"CAMISETA DEL JUEGO NIKE\""
		}
	}

	return s.repo.Send(ctx, consumer.Email, template, subject, map[string]interface{}{
		"actionUrl": url,
		"prizeName": prizeText,
	})
}

func (s *Service) translatePrizes(consumer entity.Consumer, template *string, entry *entity.PointsEntry) (*string, *string, error) {
	newTemplate := *template
	newPrize := entry.Prize

	if consumer.Language == "es" {
		newTemplate = "es-" + newTemplate
	}

	file, err := f.Open("resources/prizing-transformed.csv")
	if err != nil {
		return nil, nil, err
	}

	defer file.Close()

	reader := csv.NewReader(file)

	for {
		row, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				err = nil
			}
			return nil, nil, err
		}

		if entry.PrizeSlug == row[2] {
			if consumer.Language == "es" {
				newPrize = strings.Replace(row[3], "\r", "\n", -1)
			} else {
				newPrize = strings.Replace(row[1], "\r", "\n", -1)
			}

			return &newTemplate, &newPrize, nil
		}
	}
}
