package metrics

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
)

//Reader interface
type Reader interface {
	GetMetricsTotals(ctx context.Context) (*entity.MetricsTotals, error)
	GetMetricsDaily(ctx context.Context, start string, end string) ([]*entity.MetricsDaily, error)
	GetMetricsDomains(ctx context.Context, start string, end string) ([]*entity.MetricsDomains, error)
	GetValidationsList(ctx context.Context) ([]*entity.MGValidationResponse, error)
}

//Writer interface
type Writer interface {
	UpdateMetricsAdminPoints(ctx context.Context, points int) error
	UpdateMetricsEntry(ctx context.Context, count int, date string, weekday string, state model.State, team model.EntryTeam, winner bool, division string, points int, amoe bool) error
	UpdateMetricsUser(ctx context.Context, date string, weekday string, consumer entity.Consumer) error
	UpdateMetricsRedemption(ctx context.Context, date string, weekday string, state model.State, division string, redemption entity.CouponCodeRedemption) error
	UpdateMetricsSweeps(ctx context.Context, count int, date string, weekday string) error
}

//Repository interface
type Repository interface {
	Reader
	Writer
}
