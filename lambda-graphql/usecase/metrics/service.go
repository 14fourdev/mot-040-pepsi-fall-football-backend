package metrics

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
)

// Service consumer usecase
type Service struct {
	repo Repository
}

// NewService create new use case
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}

// GetMetricsTotals gets the MetricsTotals from the db
func (s *Service) GetMetricsTotals(ctx context.Context) (*entity.MetricsTotals, error) {
	return s.repo.GetMetricsTotals(ctx)
}

// GetMetricsDaily gets the MetricsDaily from the db
func (s *Service) GetMetricsDaily(ctx context.Context, start string, end string) ([]*entity.MetricsDaily, error) {
	return s.repo.GetMetricsDaily(ctx, start, end)
}

// GetMetricsDaily gets the MetricsDaily from the db
func (s *Service) GetMetricsDomains(ctx context.Context, start string, end string) ([]*entity.MetricsDomains, error) {
	return s.repo.GetMetricsDomains(ctx, start, end)
}

// GetValidationsList gets the Validations from the db
func (s *Service) GetValidationsList(ctx context.Context) ([]*entity.MGValidationResponse, error) {
	return s.repo.GetValidationsList(ctx)
}

// UpdateMetricsAdminPoints updates all the items related to an entry in the metrics
func (s *Service) UpdateMetricsAdminPoints(ctx context.Context, points int) error {
	return s.repo.UpdateMetricsAdminPoints(ctx, points)
}

// UpdateMetricsEntry updates all the items related to an entry in the metrics
func (s *Service) UpdateMetricsEntry(ctx context.Context, count int, date string, weekday string, state model.State, team model.EntryTeam, winner bool, division string, points int, amoe bool) error {
	return s.repo.UpdateMetricsEntry(ctx, count, date, weekday, state, team, winner, division, points, amoe)
}

// UpdateMetricsUser updates all the items related to a user in the metrics
func (s *Service) UpdateMetricsUser(ctx context.Context, date string, weekday string, consumer entity.Consumer) error {
	return s.repo.UpdateMetricsUser(ctx, date, weekday, consumer)
}

// UpdateMetricsRedemption updates all the items related to a redemption in the metrics
func (s *Service) UpdateMetricsRedemption(ctx context.Context, date string, weekday string, state model.State, division string, redemption entity.CouponCodeRedemption) error {
	return s.repo.UpdateMetricsRedemption(ctx, date, weekday, state, division, redemption)
}

// UpdateMetricsSweeps updates all the items related to a sweeps entry in the metrics
func (s *Service) UpdateMetricsSweeps(ctx context.Context, count int, date string, weekday string) error {
	return s.repo.UpdateMetricsSweeps(ctx, count, date, weekday)
}
