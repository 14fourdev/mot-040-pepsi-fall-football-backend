package admin

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

//Reader interface
type Reader interface {
	GetByEmail(ctx context.Context, email string) (*entity.Admin, error)
}

//Writer interface
type Writer interface {
	Put(ctx context.Context, admin entity.Admin) error
}

//Repository interface
type Repository interface {
	Reader
	Writer
}
