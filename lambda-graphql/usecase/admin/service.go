package admin

import (
	"context"
	"strings"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

// Service admin usecase
type Service struct {
	repo Repository
	loc  *time.Location
}

// NewService create new use case
func NewService(r Repository, loc *time.Location) *Service {
	return &Service{
		repo: r,
		loc:  loc,
	}
}

// GetAdmin returns the consumer from the email
func (s *Service) GetAdmin(ctx context.Context, key string) (*entity.Admin, error) {
	return s.repo.GetByEmail(ctx, strings.TrimPrefix(key, "ADMIN#"))
}

// PutAdmin stores the admin in the database
func (s *Service) PutAdmin(ctx context.Context, admin entity.Admin) error {
	return s.repo.Put(ctx, admin)
}
