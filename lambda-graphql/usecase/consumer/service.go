package consumer

import (
	"context"
	"strings"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

// Service consumer usecase
type Service struct {
	repo Repository
}

// NewService create new use case
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}

// GetZipcode returns the consumer from the email
func (s *Service) GetZipcode(ctx context.Context, key string) (*entity.Zipcode, error) {
	return s.repo.GetZip(ctx, strings.TrimPrefix(key, "ZIP#"))
}

// GetConsumer returns the consumer from the email
func (s *Service) GetConsumer(ctx context.Context, key string) (*entity.Consumer, error) {
	return s.repo.GetByEmail(ctx, strings.TrimPrefix(key, "CONSUMER#"))
}

// PutConsumer stores the consumer in the database
func (s *Service) PutConsumer(ctx context.Context, consumer entity.Consumer) error {
	return s.repo.Put(ctx, consumer)
}

// UpsertConsumer stores the consumer in the database
func (s *Service) UpsertConsumer(ctx context.Context, consumer entity.Consumer) error {
	return s.repo.Upsert(ctx, consumer)
}
