package consumer

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
)

//Reader interface
type Reader interface {
	GetZip(ctx context.Context, zip string) (*entity.Zipcode, error)
	GetByEmail(ctx context.Context, email string) (*entity.Consumer, error)
}

//Writer interface
type Writer interface {
	Put(ctx context.Context, consumer entity.Consumer) error
	Upsert(ctx context.Context, consumer entity.Consumer) error
}

//Repository interface
type Repository interface {
	Reader
	Writer
}
