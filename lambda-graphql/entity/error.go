package entity

import (
	"errors"
	"fmt"
)

type AppError struct {
	Code    string
	Message error
}

func (e *AppError) Error() string {
	return fmt.Sprintf("%s: %v", e.Code, e.Message)
}

func (e *AppError) ErrCode() string {
	return e.Code
}

func Wrap(err error, code string) *AppError {
	return &AppError{
		Code:    code,
		Message: err,
	}
}

var (
	// ErrBanned exported
	ErrBanned = Wrap(errors.New("domain, ip, or user banned"), "BANNED")
	// ErrCodeLimit exported
	ErrCodeLimit = Wrap(errors.New("code limit reached"), "CODE_LIMIT")
	// ErrDatabase exported
	ErrDatabase = Wrap(errors.New("database error"), "DATABASE")
	// ErrExists exported
	ErrExists = Wrap(errors.New("document already exists"), "RECORD_EXISTS")
	// ErrForbidden exported
	ErrForbidden = Wrap(errors.New("access to this resource is forbidden"), "FORBIDDEN")
	// ErrInputViolation exported
	ErrInputViolation = Wrap(errors.New("input violation"), "INPUT_VIOLATION")
	// ErrInternalError exported
	ErrInternalError = Wrap(errors.New("internal server error"), "INTERNAL_ERROR")
	// ErrInvalid exported
	ErrInvalid = Wrap(errors.New("invalid entry"), "INVALID_ENTRY")
	// ErrInvalidCaptcha exported
	ErrInvalidCaptcha = Wrap(errors.New("invalid CAPTCHA response"), "INVALID_CAPTCHA")
	// ErrInvalidConfirmPassword exported
	ErrInvalidConfirmPassword = Wrap(errors.New("password fields do not match"), "INVALID_PASSWORD")
	// ErrInvalidToken exported
	ErrInvalidToken = Wrap(errors.New("invalid token"), "INVALID_TOKEN")
	// ErrNoContent exported
	ErrNoContent = Wrap(errors.New("no content"), "NO_CONTENT")
	// ErrNotFound exported
	ErrNotFound = Wrap(errors.New("document not found"), "NOT_FOUND")
	// ErrRateLimit exported
	ErrRateLimit = Wrap(errors.New("limit reached"), "RATE_LIMIT")
	// ErrRuleViolation exported
	ErrRuleViolation = Wrap(errors.New("program rules not met"), "RULE_VIOLATION")
	// ErrSigningMethod exported
	ErrSigningMethod = Wrap(errors.New("invalid signing method"), "INVALID_SIGNING_METHOD")
	// ErrSuspended exported
	ErrSuspended = Wrap(errors.New("user account suspended"), "SUSPENDED")
	// ErrUnauthorized exported
	ErrUnauthorized = Wrap(errors.New("you are not allowed to perform this action"), "UNAUTHORIZED")
	// ErrWinRateLimit exported
	ErrWinRateLimit = Wrap(errors.New("weekly win rate reached"), "WIN_RATE_LIMIT")
)
