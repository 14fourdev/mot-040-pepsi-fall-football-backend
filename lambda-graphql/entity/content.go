package entity

import "bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"

type FoodServiceBanner struct {
	PK     string          `json:"PK"`
	SK     string          `json:"SK"`
	Active bool            `json:"active"`
	URL    string          `json:"url"`
	Team   model.EntryTeam `json:"team"`
	Image  string          `json:"image"`
}

type ARModule struct {
	PK     string          `json:"PK"`
	SK     string          `json:"SK"`
	Active bool            `json:"active"`
	Image  string          `json:"image"`
	QR     string          `json:"qr"`
	Team   model.EntryTeam `json:"team"`
	URL    string          `json:"url"`
	Week   string          `json:"week"`
}

// CreateFoodServiceBanner
func CreateFoodServiceBanner(input model.FSBInput, fileUrl string) FoodServiceBanner {
	fsb := FoodServiceBanner{
		PK:     "FSB",
		SK:     "#" + input.Team.String(),
		Active: input.Active,
		Image:  fileUrl,
		Team:   input.Team,
		URL:    input.URL,
	}

	return fsb
}

// CreateARModule
func CreateARModule(input model.ARMInput, fileUrl string, qr string) ARModule {
	fsb := ARModule{
		PK:     "ARM",
		SK:     "#" + input.Team.String() + "#" + input.Week,
		Active: input.Active,
		Image:  fileUrl,
		QR:     qr,
		Team:   input.Team,
		URL:    input.URL,
		Week:   input.Week,
	}

	return fsb
}
