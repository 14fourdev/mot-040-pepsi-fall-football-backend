package entity

import (
	"regexp"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type DapperEntry struct {
	PK                string                `json:"PK"`
	SK                string                `json:"SK"`
	Email             string                `json:"email"`
	OriginalEmail     string                `json:"originalEmail"`
	FirstName         string                `json:"firstName"`
	LastName          string                `json:"lastName"`
	Phone             string                `json:"phone"`
	Address           string                `json:"address"`
	Address2          *string               `json:"address2"`
	City              string                `json:"city"`
	State             model.State           `json:"state"`
	Zipcode           string                `json:"zipcode"`
	Birthdate         string                `json:"birthdate"`
	Size              model.Size            `json:"size"`
	Type              *string               `json:"type"`
	Rules             bool                  `json:"rules"`
	Newsletter        bool                  `json:"newsletter"`
	Salesforce        bool                  `json:"salesforce"`
	SalesforceMessage string                `json:"salesforceMessage"`
	Password          string                `json:"password"`
	Language          model.Language        `json:"language"`
	EmailValidation   *MGValidationResponse `json:"emailValidation"`
	ConfirmedAt       string                `json:"confirmedAt"`
	Token             *string               `json:"token"`
	CreatedAt         string                `json:"createdAt"`
}

type DapperKits struct {
	PK           string `json:"PK"`
	SK           string `json:"SK"`
	AvailableS   int    `json:"availableS"`
	AvailableM   int    `json:"availableM"`
	AvailableL   int    `json:"availableL"`
	AvailableXL  int    `json:"availableXL"`
	AvailableXXL int    `json:"availableXXL"`
}

// CreateDapperEntry creates a model from input
func CreateDapperEntry(input model.DapperEntryItem) (DapperEntry, error) {
	cleanEmail := CleanEmail(input.Email)

	dapperEntry := DapperEntry{
		PK:                "DAPPER",
		SK:                "#" + cleanEmail,
		Email:             cleanEmail,
		OriginalEmail:     input.Email,
		FirstName:         input.FirstName,
		LastName:          input.LastName,
		Phone:             input.Phone,
		Address:           input.Address,
		Address2:          input.Address2,
		City:              input.City,
		State:             input.State,
		Zipcode:           input.Zipcode,
		Birthdate:         input.Birthdate,
		Rules:             input.Rules,
		Size:              input.Size,
		Newsletter:        input.Newsletter,
		Salesforce:        false,
		SalesforceMessage: "not attempted",
		Language:          input.Language,
	}

	return dapperEntry, dapperEntry.Validate()
}

// Validate ensures that the contents of the structure are valid
func (d DapperEntry) Validate() error {
	return validation.ValidateStruct(&d,
		validation.Field(&d.OriginalEmail, validation.Required, validation.Match(regexp.MustCompile(`(?i)^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$`))),
		validation.Field(&d.FirstName, validation.Required, validation.Match(regexp.MustCompile(`(?i)^[A-zÀ-ÿ\s-]{2,50}$`))),
		validation.Field(&d.LastName, validation.Required, validation.Match(regexp.MustCompile(`(?i)^[A-zÀ-ÿ\s-]{2,50}$`))),
		validation.Field(&d.Phone, validation.Required, validation.Match(regexp.MustCompile(`(?i)^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$`))),
		validation.Field(&d.Zipcode, validation.Required, validation.Match(regexp.MustCompile("^[0-9]{5}$"))),
		validation.Field(&d.Birthdate, validation.Required, validation.Date("2006-01-02")),
		validation.Field(&d.Rules, validation.Required),
	)
}
