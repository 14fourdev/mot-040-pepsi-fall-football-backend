package entity

import (
	"regexp"
	"strings"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type Consumer struct {
	PK                  string                `json:"PK"`
	SK                  string                `json:"SK"`
	Email               string                `json:"email"`
	OriginalEmail       string                `json:"originalEmail"`
	FirstName           string                `json:"firstName"`
	LastName            string                `json:"lastName"`
	Phone               string                `json:"phone"`
	Address             string                `json:"address"`
	Address2            *string               `json:"address2"`
	City                string                `json:"city"`
	State               model.State           `json:"state"`
	Zipcode             string                `json:"zipcode"`
	Division            string                `json:"division"`
	Birthdate           string                `json:"birthdate"`
	Team                *model.Team           `json:"team"`
	CurrentPoints       int                   `json:"currentPoints"`
	TotalPoints         int                   `json:"totalPoints"`
	WeeklySweepsEntries WeeklySweepsEntryList `json:"WeeklySweepsEntries"`
	Rules               bool                  `json:"rules"`
	Newsletter          bool                  `json:"newsletter"`
	Salesforce          bool                  `json:"salesforce"`
	SalesforceMessage   string                `json:"salesforceMessage"`
	Password            string                `json:"password"`
	EmailConfirmed      bool                  `json:"emailConfirmed"`
	Status              string                `json:"status"`
	Language            model.Language        `json:"language"`
	EmailValidation     *MGValidationResponse `json:"emailValidation"`
	CreatedAt           string                `json:"createdAt"`
}

type Zipcode struct {
	PK       string      `json:"PK"`
	SK       string      `json:"SK"`
	Zipcode  string      `json:"zipcode"`
	State    model.State `json:"state"`
	Division string      `json:"division"`
	Source   string      `json:"source"`
}

type WeeklySweepsEntryList struct {
	Entries []*WeeklySweepsEntry `json:"entries"`
}

type WeeklySweepsEntry struct {
	WeekStartDate string `json:"weekStartDate"`
	Count         int    `json:"count"`
}

// CreateConsumer creates a consumer model from input
func CreateConsumer(input model.NewConsumer) (Consumer, error) {
	cleanEmail := CleanEmail(input.Email)
	pass, err := HashPassword(input.Password)
	if err != nil {
		return Consumer{}, err
	}

	consumer := Consumer{
		PK:                "CONSUMER#" + cleanEmail,
		SK:                "#PROFILE",
		Email:             cleanEmail,
		OriginalEmail:     input.Email,
		FirstName:         input.FirstName,
		LastName:          input.LastName,
		Phone:             input.Phone,
		Address:           input.Address,
		Address2:          input.Address2,
		City:              input.City,
		State:             input.State,
		Zipcode:           input.Zipcode,
		Birthdate:         input.Birthdate,
		Team:              input.Team,
		CurrentPoints:     0,
		TotalPoints:       0,
		Rules:             input.Rules,
		Newsletter:        input.Newsletter,
		Salesforce:        false,
		SalesforceMessage: "not attempted",
		EmailConfirmed:    false,
		Status:            "active",
		Language:          input.Language,
		Password:          pass,
	}

	return consumer, consumer.Validate()
}

// Validate ensures that the contents of the structure are valid
func (c Consumer) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.OriginalEmail, validation.Required, validation.Match(regexp.MustCompile(`(?i)^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$`))),
		validation.Field(&c.FirstName, validation.Required, validation.Match(regexp.MustCompile(`(?i)^[A-zÀ-ÿ\s-]{2,50}$`))),
		validation.Field(&c.LastName, validation.Required, validation.Match(regexp.MustCompile(`(?i)^[A-zÀ-ÿ\s-]{2,50}$`))),
		validation.Field(&c.Phone, validation.Required, validation.Match(regexp.MustCompile(`(?i)^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$`))),
		validation.Field(&c.Zipcode, validation.Required, validation.Match(regexp.MustCompile("^[0-9]{5}$"))),
		validation.Field(&c.Birthdate, validation.Required, validation.Date("2006-01-02")),
		validation.Field(&c.Rules, validation.Required),
		validation.Field(&c.Password, validation.Required, validation.Length(8, 100)),
	)
}

// CleanEmail removed google specific separations such as + or . from an email address
func CleanEmail(email string) string {
	email = strings.ToLower(email)

	if strings.HasSuffix(email, "@gmail.com") {
		offset := strings.Index(email, "@")
		address := email[:offset]
		address = strings.Replace(address, ".", "", -1)
		i := strings.Index(address, "+")
		if i > -1 {
			address = address[:i]
		}
		email = address + email[offset:]
	}

	return email
}

// ConsumerValidateAge confirms the supplied birthdate meets project guidelines
func ConsumerValidateAge(birthday string) error {
	birthdate, err := time.Parse("2006-01-02", birthday)
	if err != nil {
		return err
	}

	yearsOld := diffYears(time.Now(), birthdate)
	yearsRequired := 18

	if yearsOld < yearsRequired {
		return ErrRuleViolation
	}

	return nil
}

// ConsumerCheckStatus returns the appropriate error if the user has a special status
func ConsumerCheckStatus(consumer Consumer) error {
	switch consumer.Status {
	case "active":
		return nil
	case "banned":
		return ErrBanned
	case "suspended":
		return ErrSuspended
	}

	return nil
}
