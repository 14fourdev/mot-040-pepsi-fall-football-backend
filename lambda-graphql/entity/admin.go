package entity

import (
	"regexp"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type Admin struct {
	PK        string `json:"PK"`
	SK        string `json:"SK"`
	Email     string `json:"email"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Password  string `json:"password"`
	Role      string `json:"role"`
	CreatedAt string `json:"createdAt"`
}

// CreateAdmin creates a consumer model from input
func CreateAdmin(input model.Na) (Admin, error) {
	pass, err := HashPassword(input.Password)
	if err != nil {
		return Admin{}, err
	}

	admin := Admin{
		PK:        "ADMIN#" + input.Email,
		SK:        "#PROFILE",
		Email:     input.Email,
		FirstName: input.FirstName,
		LastName:  input.LastName,
		Role:      "ADMIN",
		Password:  string(pass),
	}

	return admin, admin.Validate()
}

// Validate ensures that the contents of the structure are valid
func (a Admin) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Email, validation.Required, validation.Match(regexp.MustCompile(`(?i)^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$`))),
		validation.Field(&a.FirstName, validation.Required, validation.Match(regexp.MustCompile(`(?i)^[A-zÀ-ÿ\s-]{2,50}$`))),
		validation.Field(&a.LastName, validation.Required, validation.Match(regexp.MustCompile(`(?i)^[A-zÀ-ÿ\s-]{2,50}$`))),
	)
}

// Role exported
type Role struct {
	ID          uint `gorm:"primary_key"`
	Name        string
	Permissions []*Permission `gorm:"many2many:role_permissions;"`
}

// Permission exported
type Permission struct {
	ID          uint `gorm:"primary_key"`
	Handle      string
	Description string
}
