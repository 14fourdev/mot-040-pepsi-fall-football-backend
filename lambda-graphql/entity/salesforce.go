package entity

type AuthResponse struct {
	Token   string `json:"access_token"`
	Expires int    `json:"expires_in"`
	RestURL string `json:"rest_instance_url"`
}

type SFValue interface {
	isValue()
}

type SFRequests []SFRequest

type SFRequest struct {
	Keys   SFKeys  `json:"keys"`
	Values SFValue `json:"values"`
}

type SFKeys struct {
	EmailAddress string
	SourceID     string
	TimeStamp    string
}

type OptIns struct {
	FirstName     string
	LastName      string
	Birthdate     string
	StateProvince string
	ZipPostalCode string
	Country       string
	PhoneNumber   string
	OptIns        string
}

type SFErrorResponse struct {
	Documentation string `json:"documentation"`
	ErrorCode     int    `json:"errorCode"`
	Message       string `json:"message"`
}

type SFResponses []SFResponse

type SFResponse struct {
	SFKeys keyWrapper `json:"keys"`
}

type keyWrapper struct {
	EmailAddress string `json:"emailAddress"`
	SourceID     string `json:"sourceID"`
	TimeStamp    string `json:"timeStamp"`
}

func (OptIns) isValue() {}
