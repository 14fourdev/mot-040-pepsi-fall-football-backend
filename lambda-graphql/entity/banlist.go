package entity

// BanlistItem exported
type BanlistItem struct {
	PK         string `json:"PK"`
	SK         string `json:"SK"`
	BlockCount int    `json:"blockCount"`
}
