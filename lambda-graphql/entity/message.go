package entity

import "time"

type Message struct {
	Type      string                            `json:"type"`
	FromEmail string                            `json:"fromEmail"`
	FromName  string                            `json:"fromName"`
	Subject   string                            `json:"subject"`
	SendTime  time.Time                         `json:"sendTime"`
	Data      map[string]interface{}            `json:"data"`
	UserData  map[string]map[string]interface{} `json:"userData"`
}

// AddRecipientAndData adds recipient and data to the message object
func (m *Message) AddRecipientAndData(to string, data map[string]interface{}) {
	if m.UserData == nil {
		m.UserData = make(map[string]map[string]interface{})
	}

	m.UserData[to] = data
}
