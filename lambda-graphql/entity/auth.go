package entity

import (
	"time"

	gonanoid "github.com/matoous/go-nanoid/v2"
	"golang.org/x/crypto/bcrypt"
)

type Reset struct {
	PK        string    `json:"PK"`
	SK        string    `json:"SK"`
	Status    string    `json:"status"`
	Token     string    `json:"token"`
	User      string    `json:"user"`
	ExpiresAt time.Time `json:"expiresAt"`
	CreatedAt string    `json:"createdAt"`
}

// CreateReset generates a reset token
func CreateReset(pk string, role string) (Reset, error) {
	token, err := gonanoid.New()
	if err != nil {
		return Reset{}, err
	}

	return Reset{
		PK:     "RESET#" + token,
		SK:     "#ROLE#" + role,
		Token:  token,
		User:   pk,
		Status: "new",
	}, nil
}

// HashPassword hashes the password on the consumer
func HashPassword(password string) (string, error) {
	pass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		return "", ErrInvalid
	}

	return string(pass), nil
}
