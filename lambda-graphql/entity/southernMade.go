package entity

import (
	"encoding/json"
	"strconv"
	"strings"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
)

type PostPointEntry struct {
	Entry PointEntry `json:"point_entry"`
}

type PointEntry struct {
	CampaignCategoryID string  `json:"campaign_category_id,omitempty"`
	Email              string  `json:"email,omitempty"`
	Name               string  `json:"full_name,omitempty"`
	Address            string  `json:"address,omitempty"`
	Address2           *string `json:"address2,omitempty"`
	City               string  `json:"city,omitempty"`
	State              string  `json:"state,omitempty"`
	Zipcode            string  `json:"postal_code,omitempty"`
}

type PostWeeklySweepstakesEntry struct {
	Sweeps WeeklySweepstakesEntry `json:"weekly_sweepstakes_entry"`
}

type WeeklySweepstakesEntry struct {
	Count    int     `json:"creation_count"`
	Email    string  `json:"email,omitempty"`
	Name     string  `json:"full_name,omitempty"`
	Address  string  `json:"address,omitempty"`
	Address2 *string `json:"address2,omitempty"`
	City     string  `json:"city,omitempty"`
	State    string  `json:"state,omitempty"`
	Zipcode  string  `json:"postal_code,omitempty"`
}

type ResponsePointEntry struct {
	Entry  ResponsePointEntryItem `json:"point_entry"`
	Errors errorWrapper           `json:"errors"`
	Status int                    `json:"status"`
}

type ResponseWeeklySweepstakesEntry struct {
	Errors errorWrapper `json:"errors"`
	Status int          `json:"status"`
}

type ResponsePointEntryItem struct {
	ID                 string                 `json:"id"`
	CampaignCategoryID string                 `json:"campaign_category_id"`
	Email              string                 `json:"email"`
	Name               string                 `json:"full_name"`
	Zipcode            string                 `json:"postal_code"`
	Points             int                    `json:"points"`
	InstantWin         ResponseInstantWinItem `json:"instant_win"`
}

type ResponseInstantWinItem struct {
	CampaignCategoryID string `json:"campaign_category_id"`
	Prize              string `json:"prize"`
	Winner             bool   `json:"winner"`
}

type errorWrapper struct {
	Error []string
}

func (w *errorWrapper) UnmarshalJSON(data []byte) error {
	dataString := string(data)

	if !strings.Contains(dataString, "[") {
		addString, err := strconv.Unquote(dataString)
		if err != nil {
			return err
		}

		w.Error = append(w.Error, addString)
		return nil
	}

	return json.Unmarshal(data, &w.Error)
}

// CreatePostPointEntry creates a CreatePostPointEntry model from input
func CreatePostPointEntry(consumer Consumer, input model.NewEntry) (PostPointEntry, error) {
	cat := ""

	if consumer.Division == "SOUTH" {
		cat = "SOUTH"
	} else if consumer.Division == "NORTH" {
		cat = input.Team.String()
	}

	return PostPointEntry{
		Entry: PointEntry{
			CampaignCategoryID: cat,
			Email:              consumer.Email,
			Name:               consumer.FirstName + " " + consumer.LastName,
			Address:            consumer.Address,
			Address2:           consumer.Address2,
			City:               consumer.City,
			State:              consumer.State.String(),
			Zipcode:            consumer.Zipcode,
		},
	}, nil
}

// CreatePostWeeklySweepstakesEntry creates a PostWeeklySweepstakesEntry model from input
func CreatePostWeeklySweepstakesEntry(consumer Consumer, input model.NewSweeps) (PostWeeklySweepstakesEntry, error) {
	return PostWeeklySweepstakesEntry{
		Sweeps: WeeklySweepstakesEntry{
			Count:    input.Count,
			Email:    consumer.Email,
			Name:     consumer.FirstName + " " + consumer.LastName,
			Address:  consumer.Address,
			Address2: consumer.Address2,
			City:     consumer.City,
			State:    consumer.State.String(),
			Zipcode:  consumer.Zipcode,
		},
	}, nil
}
