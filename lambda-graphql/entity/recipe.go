package entity

import (
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	"github.com/gosimple/slug"
)

type Recipe struct {
	PK        string             `json:"PK"`
	SK        string             `json:"SK"`
	Slug      string             `json:"slug"`
	Active    bool               `json:"active"`
	Teams     []*model.EntryTeam `json:"teams"`
	Image     *string            `json:"image"`
	En        *RecipeDetails     `json:"en"`
	Es        *RecipeDetails     `json:"es"`
	CreatedAt string             `json:"createdAt"`
}

type RecipeDetails struct {
	Title        string  `json:"title"`
	DetailTitle  *string `json:"detailTitle"`
	Tagline      *string `json:"tagline"`
	Description  *string `json:"description"`
	Ingredients  *string `json:"ingredients"`
	Instructions *string `json:"instructions"`
	Features     *string `json:"features"`
}

type FeaturedRecipeWeekly struct {
	PK   string          `json:"PK"`
	SK   string          `json:"SK"`
	Slug string          `json:"slug"`
	Team model.EntryTeam `json:"team"`
	Week string          `json:"week"`
}

// CreateRecipe
func CreateRecipe(input model.RecipeItemInput, fileUrl *string) Recipe {
	recipe := Recipe{
		PK:     "RECIPE",
		SK:     "#" + slug.Make(input.Slug),
		Slug:   slug.Make(input.Slug),
		Active: input.Active,
		Teams:  input.Teams,
		Image:  fileUrl,
	}

	if input.En != nil {
		recipe.En = &RecipeDetails{
			Title:        input.En.Title,
			DetailTitle:  input.En.DetailTitle,
			Tagline:      input.En.Tagline,
			Description:  input.En.Description,
			Ingredients:  input.En.Ingredients,
			Instructions: input.En.Instructions,
			Features:     input.En.Features,
		}
	} else {
		recipe.En = &RecipeDetails{}
	}

	if input.Es != nil {
		recipe.Es = &RecipeDetails{
			Title:        input.Es.Title,
			DetailTitle:  input.Es.DetailTitle,
			Tagline:      input.Es.Tagline,
			Description:  input.Es.Description,
			Ingredients:  input.Es.Ingredients,
			Instructions: input.Es.Instructions,
			Features:     input.Es.Features,
		}
	} else {
		recipe.Es = &RecipeDetails{}
	}

	return recipe
}

// CreateFeaturedRecipeWeekly
func CreateFeaturedRecipeWeekly(input model.FeaturedRecipeInput) FeaturedRecipeWeekly {
	fr := FeaturedRecipeWeekly{
		PK:   "FEATUREDRECIPE",
		SK:   "#" + input.Team.String() + "#" + input.Week,
		Slug: slug.Make(input.Slug),
		Week: input.Week,
		Team: input.Team,
	}

	return fr
}
