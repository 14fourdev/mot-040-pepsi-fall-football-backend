package entity

import (
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	"github.com/gosimple/slug"
)

type AmoeCode struct {
	PK        string  `json:"PK"`
	SK        string  `json:"SK"`
	Code      string  `json:"code"`
	Used      bool    `json:"used"`
	Source    string  `json:"source"`
	Consumer  string  `json:"consumer"`
	Requester *string `json:"-"`
	UsedAt    string  `json:"usedAt"`
}

type AdminPointsEntry struct {
	PK        string  `json:"PK"`
	SK        string  `json:"SK"`
	Email     string  `json:"email"`
	Points    int     `json:"points"`
	Requester *string `json:"-"`
	CreatedAt string  `json:"createdAt"`
}

type PointsEntry struct {
	PK        string          `json:"PK"`
	SK        string          `json:"SK"`
	Email     string          `json:"email"`
	AmoeCode  string          `json:"amoeCode"`
	Team      model.EntryTeam `json:"team"`
	Points    int             `json:"points"`
	Winner    bool            `json:"winner"`
	Prize     string          `json:"prize"`
	PrizeSlug string          `json:"prizeSlug"`
	Requester *string         `json:"-"`
	CreatedAt string          `json:"createdAt"`
}

type SweepsEntry struct {
	PK        string  `json:"PK"`
	SK        string  `json:"SK"`
	Count     int     `json:"count"`
	TotalCost int     `json:"totalCost"`
	Requester *string `json:"-"`
	CreatedAt string  `json:"createdAt"`
}

type CouponCode struct {
	PK        string  `json:"PK"`
	SK        string  `json:"SK"`
	Tier      int     `json:"tier"`
	Code      string  `json:"code"`
	URL       string  `json:"url"`
	Consumer  string  `json:"consumer"`
	Used      bool    `json:"used"`
	UsedAt    string  `json:"usedAt"`
	Requester *string `json:"-"`
	Source    string  `json:"source"`
}

type CouponCodeRedemption struct {
	PK        string  `json:"PK"`
	SK        string  `json:"SK"`
	Cost      int     `json:"cost"`
	Tier      int     `json:"tier"`
	Code      string  `json:"code"`
	URL       string  `json:"url"`
	Requester *string `json:"-"`
	CreatedAt string  `json:"createdAt"`
}

type Winner struct {
	PK        string          `json:"PK"`
	SK        string          `json:"SK"`
	Email     string          `json:"email"`
	FirstName string          `json:"firstName"`
	LastName  string          `json:"lastName"`
	Phone     string          `json:"phone"`
	Address   string          `json:"address"`
	Address2  *string         `json:"address2"`
	City      string          `json:"city"`
	State     model.State     `json:"state"`
	Zipcode   string          `json:"zipcode"`
	Division  string          `json:"division"`
	Birthdate string          `json:"birthdate"`
	AmoeCode  string          `json:"amoeCode"`
	Team      model.EntryTeam `json:"team"`
	Points    int             `json:"points"`
	Winner    bool            `json:"winner"`
	Prize     string          `json:"prize"`
	PrizeSlug string          `json:"prizeSlug"`
	Language  model.Language  `json:"language"`
	CreatedAt string          `json:"createdAt"`
}

// CreateAdminPointsEntry creates an entry model from input
func CreateAdminPointsEntry(consumer Consumer, timestamp string, points int) AdminPointsEntry {
	return AdminPointsEntry{
		PK:        consumer.PK,
		SK:        "#ADMINENTRY#" + timestamp,
		Email:     consumer.Email,
		Points:    points,
		CreatedAt: timestamp,
	}
}

// CreatePointsEntry creates an entry model from input
func CreatePointsEntry(consumer Consumer, time string, date string, input model.NewEntry, response ResponsePointEntry) PointsEntry {
	code := ""

	if input.AmoeCode != nil {
		code = *input.AmoeCode
	}

	points := response.Entry.Points
	if points == 0 {
		points = 100
	}

	return PointsEntry{
		PK:        consumer.PK,
		SK:        "#ENTRY#" + date + "#" + response.Entry.ID,
		Email:     consumer.Email,
		AmoeCode:  code,
		Team:      input.Team,
		Points:    points,
		Winner:    response.Entry.InstantWin.Winner,
		Prize:     response.Entry.InstantWin.Prize,
		PrizeSlug: slug.Make(response.Entry.InstantWin.Prize),
		CreatedAt: time,
	}
}

// CreateSweepsEntry creates an entry model from input
func CreateSweepsEntry(consumer Consumer, time string, input model.NewSweeps) SweepsEntry {
	return SweepsEntry{
		PK:        consumer.PK,
		SK:        "#SWEEPS#" + time,
		Count:     input.Count,
		TotalCost: input.Count * 100,
		CreatedAt: time,
	}
}

// CreateCouponCodeRedemption creates an entry model from input
func CreateCouponCodeRedemption(consumer Consumer, time string, input model.NewRedemption) (*CouponCodeRedemption, error) {
	cost := 0

	switch input.Tier {
	case 1:
		cost = 14500
	case 2:
		cost = 30000
	case 3:
		cost = 45000
	case 4:
		cost = 50000
	case 5:
		cost = 50000
	case 6:
		cost = 100000
	case 7:
		cost = 150000
	}

	if cost == 0 {
		return nil, ErrInputViolation
	}

	return &CouponCodeRedemption{
		PK:        consumer.PK,
		SK:        "#REDEMPTION#" + time,
		Tier:      input.Tier,
		Cost:      cost,
		CreatedAt: time,
	}, nil
}
