package repository

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
)

// RequesterDynamodb DynamoDB repo
type RequesterDynamodb struct {
	db    *dynamodb.Client
	table string
}

// NewRequesterDynamodb create new repository
func NewRequesterDynamodb(db *dynamodb.Client, table string) *RequesterDynamodb {
	return &RequesterDynamodb{
		db:    db,
		table: table,
	}
}

// Put stores the requester in DynamoDB
func (r *RequesterDynamodb) Put(ctx context.Context, requester entity.Requester) error {
	av, err := attributevalue.MarshalMap(requester)

	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(r.table),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}
