package repository

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/service/wafv2"
	"github.com/aws/aws-sdk-go-v2/service/wafv2/types"
)

// BanlistWAFv2 DynamoDB repo
type BanlistWAFv2 struct {
	waf   *wafv2.Client
	table string
}

// NewBanlistWAFv2 create new repository
func NewBanlistWAFv2(waf *wafv2.Client, table string) *BanlistWAFv2 {
	return &BanlistWAFv2{
		waf:   waf,
		table: table,
	}
}

// GetIPSet
func (r *BanlistWAFv2) GetIPSet(ctx context.Context, id *string, name *string) (*types.IPSet, *string, error) {
	params := wafv2.GetIPSetInput{
		Id:   id,
		Name: name,
	}

	out, err := r.waf.GetIPSet(ctx, &params)
	if err != nil {
		return nil, nil, err
	}

	return out.IPSet, out.LockToken, nil
}

// UpdateIPSet
func (r *BanlistWAFv2) UpdateIPSet(ctx context.Context, id *string, name *string, token *string, addresses []string) (*string, error) {
	params := wafv2.UpdateIPSetInput{
		Id:        id,
		Name:      name,
		LockToken: token,
		Addresses: addresses,
	}

	out, err := r.waf.UpdateIPSet(ctx, &params)
	if err != nil {
		return nil, err
	}

	return out.NextLockToken, nil
}
