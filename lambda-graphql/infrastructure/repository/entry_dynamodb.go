package repository

import (
	"context"
	"math/rand"
	"strconv"
	"strings"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/sirupsen/logrus"
)

// EntryDynamodb DynamoDB repo
type EntryDynamodb struct {
	db    *dynamodb.Client
	table string
}

// NewEntryDynamodb create new repository
func NewEntryDynamodb(db *dynamodb.Client, table string) *EntryDynamodb {
	return &EntryDynamodb{
		db:    db,
		table: table,
	}
}

// PutAmoeCode stores consumer in DynamoDB if they don't already exist
func (r *EntryDynamodb) PutAmoeCode(ctx context.Context, amoe entity.AmoeCode) error {
	av, err := attributevalue.MarshalMap(amoe)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(r.table),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// PutCouponRedemption stores all items relevant to redemption
func (r *EntryDynamodb) PutCouponRedemption(ctx context.Context, coupon entity.CouponCode, redemption entity.CouponCodeRedemption, consumer entity.Consumer) error {
	var writeRequests []types.TransactWriteItem
	table := aws.String(r.table)

	// Create the coupon delete
	deleteKeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: coupon.PK,
		SK: "#UNCLAIMED#" + coupon.Code,
	})
	if err != nil {
		return err
	}

	requestItem := types.TransactWriteItem{
		Delete: &types.Delete{
			Key:       deleteKeys,
			TableName: table,
		},
	}

	writeRequests = append(writeRequests, requestItem)

	// Create the coupon put
	av, err := attributevalue.MarshalMap(coupon)
	if err != nil {
		return err
	}

	requestItem = types.TransactWriteItem{
		Put: &types.Put{
			Item:      av,
			TableName: table,
		},
	}

	writeRequests = append(writeRequests, requestItem)

	// Create the redemption put
	av, err = attributevalue.MarshalMap(redemption)
	if err != nil {
		return err
	}

	requestItem = types.TransactWriteItem{
		Put: &types.Put{
			Item:      av,
			TableName: table,
		},
	}

	writeRequests = append(writeRequests, requestItem)

	// Create the update for the Consumer
	Updatekeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: consumer.PK,
		SK: consumer.SK,
	})
	if err != nil {
		return err
	}

	ean := make(map[string]string)
	ean["#cp"] = "CurrentPoints"

	eav := make(map[string]types.AttributeValue)
	eav[":points"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(redemption.Cost)),
	}
	eav[":pointsMinus"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(redemption.Cost * -1)),
	}

	requestItem = types.TransactWriteItem{
		Update: &types.Update{
			Key:                       Updatekeys,
			TableName:                 table,
			UpdateExpression:          aws.String("ADD #cp :pointsMinus"),
			ExpressionAttributeNames:  ean,
			ExpressionAttributeValues: eav,
			ConditionExpression:       aws.String("#cp >= :points"),
		},
	}

	writeRequests = append(writeRequests, requestItem)

	// Transact them to the DB
	items := &dynamodb.TransactWriteItemsInput{
		TransactItems: writeRequests,
	}

	_, err = r.db.TransactWriteItems(ctx, items)
	if err != nil {
		if strings.Contains(err.Error(), "ConditionalCheckFailed") {
			return entity.ErrInvalid
		}

		return err
	}

	return nil
}

// PutAdminPointEntry stores data
func (r *EntryDynamodb) PutAdminPointEntry(ctx context.Context, entry entity.AdminPointsEntry, consumer entity.Consumer) error {
	var writeRequests []types.TransactWriteItem
	table := aws.String(r.table)

	// Create the entry put
	av, err := attributevalue.MarshalMap(entry)
	if err != nil {
		return err
	}

	requestItem := types.TransactWriteItem{
		Put: &types.Put{
			Item:                av,
			TableName:           table,
			ConditionExpression: aws.String("attribute_not_exists(PK)"),
		},
	}

	writeRequests = append(writeRequests, requestItem)

	// Create the update for the Consumer
	Updatekeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: consumer.PK,
		SK: consumer.SK,
	})
	if err != nil {
		return err
	}

	ean := make(map[string]string)
	ean["#cp"] = "CurrentPoints"
	ean["#tp"] = "TotalPoints"

	eav := make(map[string]types.AttributeValue)
	eav[":points"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(entry.Points)),
	}

	requestItem = types.TransactWriteItem{
		Update: &types.Update{
			Key:                       Updatekeys,
			TableName:                 table,
			ExpressionAttributeNames:  ean,
			ExpressionAttributeValues: eav,
			UpdateExpression:          aws.String("SET #cp = #cp + :points, #tp = #tp + :points"),
		},
	}

	writeRequests = append(writeRequests, requestItem)

	// Transact them to the DB
	items := &dynamodb.TransactWriteItemsInput{
		TransactItems: writeRequests,
	}

	_, err = r.db.TransactWriteItems(ctx, items)
	if err != nil {
		if strings.Contains(err.Error(), "ConditionalCheckFailed") {
			return entity.ErrRateLimit
		}

		return err
	}

	return nil
}

// PutPointEntry stores consumer in DynamoDB if they don't already exist
func (r *EntryDynamodb) PutPointEntry(ctx context.Context, entry entity.PointsEntry, consumer entity.Consumer) error {
	var writeRequests []types.TransactWriteItem
	table := aws.String(r.table)

	// Create the entry put
	av, err := attributevalue.MarshalMap(entry)
	if err != nil {
		return err
	}

	requestItem := types.TransactWriteItem{
		Put: &types.Put{
			Item:                av,
			TableName:           table,
			ConditionExpression: aws.String("attribute_not_exists(PK)"),
		},
	}

	writeRequests = append(writeRequests, requestItem)

	if entry.Winner {
		// Create the Winner put
		av, err = attributevalue.MarshalMap(entity.Winner{
			PK:        "WINNER",
			SK:        entry.SK,
			Email:     consumer.Email,
			FirstName: consumer.FirstName,
			LastName:  consumer.LastName,
			Phone:     consumer.Phone,
			Address:   consumer.Address,
			Address2:  consumer.Address2,
			City:      consumer.City,
			State:     consumer.State,
			Zipcode:   consumer.Zipcode,
			Division:  consumer.Division,
			Birthdate: consumer.Birthdate,
			AmoeCode:  entry.AmoeCode,
			Team:      entry.Team,
			Points:    entry.Points,
			Winner:    entry.Winner,
			Prize:     entry.Prize,
			PrizeSlug: entry.PrizeSlug,
			Language:  consumer.Language,
			CreatedAt: entry.CreatedAt,
		})
		if err != nil {
			return err
		}

		requestItem = types.TransactWriteItem{
			Put: &types.Put{
				Item:                av,
				TableName:           table,
				ConditionExpression: aws.String("attribute_not_exists(PK)"),
			},
		}

		writeRequests = append(writeRequests, requestItem)
	}

	// Create the update for the Consumer
	Updatekeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: consumer.PK,
		SK: consumer.SK,
	})
	if err != nil {
		return err
	}

	ean := make(map[string]string)
	ean["#cp"] = "CurrentPoints"
	ean["#tp"] = "TotalPoints"

	eav := make(map[string]types.AttributeValue)
	eav[":points"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(entry.Points)),
	}

	requestItem = types.TransactWriteItem{
		Update: &types.Update{
			Key:                       Updatekeys,
			TableName:                 table,
			ExpressionAttributeNames:  ean,
			ExpressionAttributeValues: eav,
			UpdateExpression:          aws.String("SET #cp = #cp + :points, #tp = #tp + :points"),
		},
	}

	writeRequests = append(writeRequests, requestItem)

	// Transact them to the DB
	items := &dynamodb.TransactWriteItemsInput{
		TransactItems: writeRequests,
	}

	_, err = r.db.TransactWriteItems(ctx, items)
	if err != nil {
		if strings.Contains(err.Error(), "ConditionalCheckFailed") {
			return entity.ErrRateLimit
		}

		return err
	}

	return nil
}

// PutSweepsEntry stores consumer in DynamoDB if they don't already exist
func (r *EntryDynamodb) PutSweepsEntry(ctx context.Context, entry entity.SweepsEntry, consumer entity.Consumer) error {
	var writeRequests []types.TransactWriteItem
	table := aws.String(r.table)

	// Create the entry put
	av, err := attributevalue.MarshalMap(entry)
	if err != nil {
		return err
	}

	requestItem := types.TransactWriteItem{
		Put: &types.Put{
			Item:                av,
			TableName:           table,
			ConditionExpression: aws.String("attribute_not_exists(PK)"),
		},
	}

	writeRequests = append(writeRequests, requestItem)

	// Create the update for the Consumer
	Updatekeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: consumer.PK,
		SK: consumer.SK,
	})
	if err != nil {
		return err
	}

	sweepsEntries, err := attributevalue.MarshalMap(consumer.WeeklySweepsEntries)
	if err != nil {
		return err
	}

	ean := make(map[string]string)
	ean["#cp"] = "CurrentPoints"
	ean["#swe"] = "WeeklySweepsEntries"

	eav := make(map[string]types.AttributeValue)
	eav[":points"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(entry.TotalCost)),
	}
	eav[":sweeps"] = &types.AttributeValueMemberM{
		Value: sweepsEntries,
	}

	if len(sweepsEntries) == 0 {
		logrus.Error("failed to generate sweepsEntries", eav, consumer)
		return entity.ErrDatabase
	}

	requestItem = types.TransactWriteItem{
		Update: &types.Update{
			Key:                       Updatekeys,
			TableName:                 table,
			UpdateExpression:          aws.String("SET #cp = #cp - :points, #swe = :sweeps"),
			ExpressionAttributeNames:  ean,
			ExpressionAttributeValues: eav,
			ConditionExpression:       aws.String("#cp >= :points"),
		},
	}

	writeRequests = append(writeRequests, requestItem)

	// Transact them to the DB
	items := &dynamodb.TransactWriteItemsInput{
		TransactItems: writeRequests,
	}

	_, err = r.db.TransactWriteItems(ctx, items)
	if err != nil {
		if strings.Contains(err.Error(), "ConditionalCheckFailed") {
			return entity.ErrInvalid
		}

		return err
	}

	return nil
}

// RevertSweepsEntry stores consumer in DynamoDB if they don't already exist
func (r *EntryDynamodb) RevertSweepsEntry(ctx context.Context, entry entity.SweepsEntry, consumer entity.Consumer) error {
	table := aws.String(r.table)

	// Delete entry
	deleteKeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: entry.PK,
		SK: entry.SK,
	})
	if err != nil {
		return err
	}

	deleteItem := &dynamodb.DeleteItemInput{
		Key:       deleteKeys,
		TableName: table,
	}

	_, err = r.db.DeleteItem(ctx, deleteItem)
	if err != nil {
		return err
	}

	// Create the update for the Consumer
	Updatekeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: consumer.PK,
		SK: consumer.SK,
	})
	if err != nil {
		return err
	}

	eav := make(map[string]types.AttributeValue)
	eav[":points"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(entry.TotalCost)),
	}

	updateItem := &dynamodb.UpdateItemInput{
		Key:                       Updatekeys,
		TableName:                 table,
		UpdateExpression:          aws.String("ADD CurrentPoints :points"),
		ExpressionAttributeValues: eav,
	}

	_, err = r.db.UpdateItem(ctx, updateItem)
	if err != nil {
		return err
	}

	return nil
}

// GetAmoeCode returns the amoe code from DynamoDB
func (r *EntryDynamodb) GetAmoeCode(ctx context.Context, code string) (*entity.AmoeCode, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "AMOE#" + code,
		SK: "#DETAILS",
	})

	if err != nil {
		return nil, entity.ErrInvalid
	}

	item := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	amoeItem, err := r.db.GetItem(ctx, item)
	if amoeItem.Item == nil || err != nil {
		return nil, entity.ErrNotFound
	}

	var decoded entity.AmoeCode

	err = attributevalue.UnmarshalMap(amoeItem.Item, &decoded)
	if err != nil {
		return nil, entity.ErrInvalid
	}

	return &decoded, nil
}

// GetUnclaimedCode returns a random unclaimed coupon code from DynamoDB
func (r *EntryDynamodb) GetUnclaimedCode(ctx context.Context, tier int) (*entity.CouponCode, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String("COUPON#" + strconv.Itoa(tier)),
	}
	eav[":sk"] = &types.AttributeValueMemberS{
		Value: *aws.String("#UNCLAIMED#"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk and begins_with(SK, :sk)"),
		ExpressionAttributeValues: eav,
		Limit:                     aws.Int32(10),
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.CouponCode
	var item *entity.CouponCode

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	for _, i := range rand.Perm(len(decoded)) {
		item = decoded[i]
	}

	return item, nil
}

// GetAdminPointsEntries returns the amoe code from DynamoDB
func (r *EntryDynamodb) GetAdminPointsEntries(ctx context.Context, consumer entity.Consumer) ([]*entity.AdminPointsEntry, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String(consumer.PK),
	}
	eav[":sk"] = &types.AttributeValueMemberS{
		Value: *aws.String("#ADMINENTRY#"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk and begins_with(SK, :sk)"),
		ExpressionAttributeValues: eav,
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.AdminPointsEntry

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	return decoded, nil
}

// GetPointsEntries returns the amoe code from DynamoDB
func (r *EntryDynamodb) GetPointsEntries(ctx context.Context, consumer entity.Consumer) ([]*entity.PointsEntry, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String(consumer.PK),
	}
	eav[":sk"] = &types.AttributeValueMemberS{
		Value: *aws.String("#ENTRY#"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk and begins_with(SK, :sk)"),
		ExpressionAttributeValues: eav,
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.PointsEntry

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	return decoded, nil
}

// GetSweepsEntries returns the amoe code from DynamoDB
func (r *EntryDynamodb) GetSweepsEntries(ctx context.Context, consumer entity.Consumer) ([]*entity.SweepsEntry, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String(consumer.PK),
	}
	eav[":sk"] = &types.AttributeValueMemberS{
		Value: *aws.String("#SWEEPS#"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk and begins_with(SK, :sk)"),
		ExpressionAttributeValues: eav,
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.SweepsEntry

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	return decoded, nil
}

// GetCouponCodeRedemptions returns the amoe code from DynamoDB
func (r *EntryDynamodb) GetCouponCodeRedemptions(ctx context.Context, consumer entity.Consumer) ([]*entity.CouponCodeRedemption, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String(consumer.PK),
	}
	eav[":sk"] = &types.AttributeValueMemberS{
		Value: *aws.String("#REDEMPTION#"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk and begins_with(SK, :sk)"),
		ExpressionAttributeValues: eav,
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.CouponCodeRedemption

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	return decoded, nil
}

// GetWinnersList returns the amoe code from DynamoDB
func (r *EntryDynamodb) GetWinnersList(ctx context.Context) ([]*entity.Winner, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String("WINNER"),
	}
	eav[":sk"] = &types.AttributeValueMemberS{
		Value: *aws.String("#ENTRY#"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk AND begins_with(SK, :sk)"),
		ExpressionAttributeValues: eav,
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.Winner

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	return decoded, nil
}

// GetCouponCode gets a coupon code
func (r *EntryDynamodb) GetCouponCode(ctx context.Context, tier int, status string, code string) (*entity.CouponCode, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "COUPON#" + strconv.Itoa(tier),
		SK: "#" + status + "#" + code,
	})

	if err != nil {
		return nil, entity.ErrInvalid
	}

	item := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	dbitem, err := r.db.GetItem(ctx, item)
	if dbitem.Item == nil || err != nil {
		return nil, entity.ErrNotFound
	}

	var decoded entity.CouponCode

	err = attributevalue.UnmarshalMap(dbitem.Item, &decoded)
	if err != nil {
		return nil, entity.ErrInvalid
	}

	return &decoded, nil
}

func (r *EntryDynamodb) runQuery(ctx context.Context, params *dynamodb.QueryInput, existingItems []map[string]types.AttributeValue) ([]map[string]types.AttributeValue, error) {
	output, err := r.db.Query(ctx, params)
	if err != nil {
		return nil, err
	}

	items := output.Items

	if len(existingItems) > 0 {
		copy(items, existingItems)
	}

	if params.Limit != nil && output.Count >= *params.Limit {
		return items, nil
	}

	if output.LastEvaluatedKey != nil {
		params.ExclusiveStartKey = output.LastEvaluatedKey
		return r.runQuery(ctx, params, items)
	}

	return items, nil
}
