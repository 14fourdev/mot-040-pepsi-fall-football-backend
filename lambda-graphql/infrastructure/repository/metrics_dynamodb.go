package repository

import (
	"context"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/bearbin/go-age"
	"github.com/sirupsen/logrus"
)

// MetricsDynamodb DynamoDB repo
type MetricsDynamodb struct {
	db    *dynamodb.Client
	table string
}

var Weekdays = []string{
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
}

var Divisions = []string{
	"North",
	"Central",
	"South",
	"West",
}

var RiskLevels = []string{
	"low",
	"medium",
	"high",
	"unknown",
}

var States = []string{
	"AK",
	"AL",
	"AR",
	"AZ",
	"CA",
	"CO",
	"CT",
	"DC",
	"DE",
	"FL",
	"GA",
	"HI",
	"IA",
	"ID",
	"IL",
	"IN",
	"KS",
	"KY",
	"LA",
	"MA",
	"MD",
	"ME",
	"MI",
	"MN",
	"MO",
	"MS",
	"MT",
	"NC",
	"ND",
	"NE",
	"NH",
	"NJ",
	"NM",
	"NV",
	"NY",
	"OH",
	"OK",
	"OR",
	"PA",
	"RI",
	"SC",
	"SD",
	"TN",
	"TX",
	"UT",
	"VA",
	"VT",
	"WA",
	"WI",
	"WV",
	"WY",
}

var EntryTeamList = []string{
	"National",
	"ArizonaCardinals",
	"AtlantaFalcons",
	"BaltimoreRavens",
	"BuffaloBills",
	"CarolinaPanthers",
	"ChicagoBears",
	"CincinnatiBengals",
	"ClevelandBrowns",
	"DallasCowboys",
	"DenverBroncos",
	"DetroitLions",
	"GreenBayPackers",
	"HoustonTexans",
	"IndianapolisColts",
	"JacksonvilleJaguars",
	"KansasCityChiefs",
	"LasVegasRaiders",
	"LosAngelesChargers",
	"LosAngelesRams",
	"MiamiDolphins",
	"MinnesotaVikings",
	"NewEnglandPatriots",
	"NewOrleansSaints",
	"NewYorkGiants",
	"NewYorkJets",
	"PhiladelphiaEagles",
	"PittsburghSteelers",
	"SanFrancisco49ers",
	"SeattleSeahawks",
	"TampaBayBuccaneers",
	"TennesseeTitans",
	"WashingtonFootballTeam",
}

// NewMetricsDynamodb create new repository
func NewMetricsDynamodb(db *dynamodb.Client, table string) *MetricsDynamodb {
	return &MetricsDynamodb{
		db:    db,
		table: table,
	}
}

// GetMetricsTotals gets the MetricsTotals from DynamoDB
func (r *MetricsDynamodb) GetMetricsTotals(ctx context.Context) (*entity.MetricsTotals, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "METRICS#",
		SK: "TOTALS",
	})

	if err != nil {
		return nil, entity.ErrInvalid
	}

	item := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	response, err := r.db.GetItem(ctx, item)
	if response.Item == nil || err != nil {
		return nil, entity.ErrNotFound
	}

	var decoded *entity.MetricsTotals

	err = attributevalue.UnmarshalMap(response.Item, &decoded)
	if err != nil {
		return nil, entity.ErrInvalid
	}

	return decoded, nil
}

// GetMetricsDaily gets the MetricsDaily from DynamoDB
func (r *MetricsDynamodb) GetMetricsDaily(ctx context.Context, start string, end string) ([]*entity.MetricsDaily, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String("METRICS#"),
	}
	eav[":skStart"] = &types.AttributeValueMemberS{
		Value: *aws.String(start),
	}
	eav[":skEnd"] = &types.AttributeValueMemberS{
		Value: *aws.String(end),
	}

	qryParams := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk AND SK BETWEEN :skStart AND :skEnd"),
		ExpressionAttributeValues: eav,
	}

	qry, err := r.db.Query(ctx, qryParams)
	if err != nil {
		return nil, nil
	}

	var decoded []*entity.MetricsDaily

	err = attributevalue.UnmarshalListOfMaps(qry.Items, &decoded)
	if err != nil {
		return nil, entity.ErrDatabase
	}

	return decoded, nil
}

// GetMetricsDomains gets the MetricsDomains from DynamoDB
func (r *MetricsDynamodb) GetMetricsDomains(ctx context.Context, start string, end string) ([]*entity.MetricsDomains, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String("METRICS#DOMAIN"),
	}
	eav[":skStart"] = &types.AttributeValueMemberS{
		Value: *aws.String("#" + start),
	}
	eav[":skEnd"] = &types.AttributeValueMemberS{
		Value: *aws.String("#" + end + "#Z"),
	}

	qryParams := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk AND SK BETWEEN :skStart AND :skEnd"),
		ExpressionAttributeValues: eav,
	}

	qry, err := r.db.Query(ctx, qryParams)
	if err != nil {
		return nil, nil
	}

	var decoded []*entity.MetricsDomains

	err = attributevalue.UnmarshalListOfMaps(qry.Items, &decoded)
	if err != nil {
		return nil, entity.ErrDatabase
	}

	return decoded, nil
}

// UpdateMetricsEntry updates metrics for an entry
func (r *MetricsDynamodb) UpdateMetricsEntry(ctx context.Context, count int, date string, weekday string, state model.State, team model.EntryTeam, winner bool, division string, points int, amoe bool) error {
	updateKeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "METRICS#",
		SK: date,
	})
	if err != nil {
		return err
	}

	eav := make(map[string]types.AttributeValue)
	eav[":scans"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(count)),
	}
	eav[":points"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(points)),
	}

	updateExpressionString := "ADD Scans :scans, "

	for _, v := range Weekdays {
		if strings.EqualFold(weekday, v) {
			updateExpressionString = updateExpressionString + "Scans" + v + " :scans, "
		}
	}

	for _, v := range Divisions {
		if strings.EqualFold(division, v) {
			updateExpressionString = updateExpressionString + "Scans" + v + " :scans, "
		}
	}

	for _, v := range States {
		if strings.EqualFold(state.String(), v) {
			updateExpressionString = updateExpressionString + "Scans" + v + " :scans, "
		}
	}

	for _, v := range EntryTeamList {
		if strings.EqualFold(team.String(), v) {
			updateExpressionString = updateExpressionString + "Scans" + v + " :scans, "
		}
	}

	if winner {
		updateExpressionString = updateExpressionString + "Winners :scans, "

		for _, v := range Weekdays {
			if strings.EqualFold(weekday, v) {
				updateExpressionString = updateExpressionString + "Winners" + v + " :scans, "
			}
		}

		for _, v := range Divisions {
			if strings.EqualFold(division, v) {
				updateExpressionString = updateExpressionString + "Winners" + v + " :scans, "
			}
		}

		for _, v := range States {
			if strings.EqualFold(state.String(), v) {
				updateExpressionString = updateExpressionString + "Winners" + v + " :scans, "
			}
		}

		for _, v := range EntryTeamList {
			if strings.EqualFold(team.String(), v) {
				updateExpressionString = updateExpressionString + "Winners" + v + " :scans, "
			}
		}
	}

	if amoe {
		updateExpressionString = updateExpressionString + "AmoeCodes :scans, "
	}

	updateExpressionString = updateExpressionString + "Points :points, "

	params := dynamodb.UpdateItemInput{
		Key:                       updateKeys,
		TableName:                 aws.String(r.table),
		ExpressionAttributeValues: eav,
		UpdateExpression:          aws.String(strings.TrimSuffix(updateExpressionString, ", ")),
	}

	_, err = r.db.UpdateItem(ctx, &params)
	if err != nil {
		return err
	}

	updateKeys, err = attributevalue.MarshalMap(&entity.SearchItem{
		PK: "METRICS#",
		SK: "TOTALS",
	})
	if err != nil {
		return err
	}

	eav = make(map[string]types.AttributeValue)
	eav[":scans"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(count)),
	}
	eav[":points"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(points)),
	}

	updateExpressionString = "ADD Scans :scans, Points :points, "

	if winner {
		updateExpressionString = updateExpressionString + "Winners :scans, "
	}

	if amoe {
		updateExpressionString = updateExpressionString + "AmoeCodes :scans, "
	}

	params = dynamodb.UpdateItemInput{
		Key:                       updateKeys,
		TableName:                 aws.String(r.table),
		ExpressionAttributeValues: eav,
		UpdateExpression:          aws.String(strings.TrimSuffix(updateExpressionString, ", ")),
	}

	_, err = r.db.UpdateItem(ctx, &params)
	if err != nil {
		return err
	}

	return nil
}

// UpdateMetricsAdminPoints updates the metrics for a admin point entry
func (r *MetricsDynamodb) UpdateMetricsAdminPoints(ctx context.Context, points int) error {
	updateKeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "METRICS#",
		SK: "TOTALS",
	})
	if err != nil {
		return err
	}

	eav := make(map[string]types.AttributeValue)
	eav[":points"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(points)),
	}

	updateExpressionString := "ADD AdminPoints :points"

	params := dynamodb.UpdateItemInput{
		Key:                       updateKeys,
		TableName:                 aws.String(r.table),
		ExpressionAttributeValues: eav,
		UpdateExpression:          aws.String(strings.TrimSuffix(updateExpressionString, ", ")),
	}

	_, err = r.db.UpdateItem(ctx, &params)
	if err != nil {
		return err
	}

	return nil
}

// UpdateMetricsUser updates metrics for a user
func (r *MetricsDynamodb) UpdateMetricsUser(ctx context.Context, date string, weekday string, consumer entity.Consumer) error {
	updateKeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "METRICS#",
		SK: date,
	})
	if err != nil {
		return err
	}

	ean := make(map[string]string)
	ean["#u"] = "Users"

	eav := make(map[string]types.AttributeValue)
	eav[":users"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(1)),
	}

	updateExpressionString := "ADD #u :users, "

	for _, v := range Weekdays {
		if strings.EqualFold(weekday, v) {
			updateExpressionString = updateExpressionString + "Users" + v + " :users, "
		}
	}

	for _, v := range Divisions {
		if strings.EqualFold(consumer.Division, v) {
			updateExpressionString = updateExpressionString + "Users" + v + " :users, "
		}
	}

	for _, v := range States {
		if strings.EqualFold(consumer.State.String(), v) {
			updateExpressionString = updateExpressionString + "Users" + v + " :users, "
		}
	}

	if consumer.Team != nil {
		for _, v := range EntryTeamList {
			if strings.EqualFold(consumer.Team.String(), v) {
				updateExpressionString = updateExpressionString + "Users" + v + " :users, "
			}
		}
	}

	for _, v := range RiskLevels {
		if strings.EqualFold(consumer.EmailValidation.Risk, v) {
			updateExpressionString = updateExpressionString + "Validations" + strings.Title(v) + " :users, "
		}
	}

	if consumer.Language.String() == "en" {
		updateExpressionString = updateExpressionString + "UsersEN :users, "
	} else if consumer.Language == "es" {
		updateExpressionString = updateExpressionString + "UsersES :users, "
	}

	dob, err := time.Parse("2006-01-02", consumer.Birthdate)
	if err != nil {
		return err
	}

	yearsOld := age.Age(dob)
	if yearsOld >= 18 && yearsOld <= 29 {
		updateExpressionString = updateExpressionString + "Users18 :users, "
	} else if yearsOld >= 30 && yearsOld <= 39 {
		updateExpressionString = updateExpressionString + "Users30 :users, "
	} else if yearsOld >= 40 && yearsOld <= 49 {
		updateExpressionString = updateExpressionString + "Users40 :users, "
	} else if yearsOld >= 50 && yearsOld <= 64 {
		updateExpressionString = updateExpressionString + "Users50 :users, "
	} else if yearsOld >= 65 {
		updateExpressionString = updateExpressionString + "Users65 :users, "
	} else {
		logrus.Errorln("Users age did not compile properly: " + strconv.Itoa(yearsOld))
		logrus.Errorln(consumer)
	}

	params := dynamodb.UpdateItemInput{
		Key:                       updateKeys,
		TableName:                 aws.String(r.table),
		ExpressionAttributeNames:  ean,
		ExpressionAttributeValues: eav,
		UpdateExpression:          aws.String(strings.TrimSuffix(updateExpressionString, ", ")),
	}

	_, err = r.db.UpdateItem(ctx, &params)
	if err != nil {
		return err
	}

	updateKeys, err = attributevalue.MarshalMap(&entity.SearchItem{
		PK: "METRICS#",
		SK: "TOTALS",
	})
	if err != nil {
		return err
	}

	eav = make(map[string]types.AttributeValue)
	eav[":users"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(1)),
	}

	updateExpressionString = "ADD #u :users, "

	if consumer.Language.String() == "en" {
		updateExpressionString = updateExpressionString + "UsersEN :users, "
	} else if consumer.Language.String() == "es" {
		updateExpressionString = updateExpressionString + "UsersES :users, "
	}

	for _, v := range RiskLevels {
		if strings.EqualFold(consumer.EmailValidation.Risk, v) {
			updateExpressionString = updateExpressionString + "Validations" + strings.Title(v) + " :users, "
		}
	}

	if consumer.Newsletter {
		updateExpressionString = updateExpressionString + "OptIns :users, "
	}

	params = dynamodb.UpdateItemInput{
		Key:                       updateKeys,
		TableName:                 aws.String(r.table),
		ExpressionAttributeNames:  ean,
		ExpressionAttributeValues: eav,
		UpdateExpression:          aws.String(strings.TrimSuffix(updateExpressionString, ", ")),
	}

	_, err = r.db.UpdateItem(ctx, &params)
	if err != nil {
		return err
	}

	val := consumer.EmailValidation
	av, err := attributevalue.MarshalMap(val)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(r.table),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		logrus.Error(err)
		// return err
	}

	components := strings.Split(consumer.Email, "@")
	timestamp, err := time.Parse("2006-01-02 15:04:05", consumer.CreatedAt)
	if err != nil {
		return err
	}

	updateKeys, err = attributevalue.MarshalMap(&entity.SearchItem{
		PK: "METRICS#DOMAIN",
		SK: "#" + timestamp.Format("2006-01-02") + "#" + components[1],
	})
	if err != nil {
		return err
	}

	ean = make(map[string]string)
	ean["#c"] = "Count"
	ean["#d"] = "Domain"
	ean["#da"] = "Date"

	eav = make(map[string]types.AttributeValue)
	eav[":c"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(1)),
	}
	eav[":d"] = &types.AttributeValueMemberS{
		Value: *aws.String(components[1]),
	}
	eav[":da"] = &types.AttributeValueMemberS{
		Value: *aws.String(timestamp.Format("2006-01-02")),
	}

	updateExpressionString = "ADD #c :c SET #d = :d, #da = :da"

	params = dynamodb.UpdateItemInput{
		Key:                       updateKeys,
		TableName:                 aws.String(r.table),
		ExpressionAttributeNames:  ean,
		ExpressionAttributeValues: eav,
		UpdateExpression:          aws.String(updateExpressionString),
	}

	_, err = r.db.UpdateItem(ctx, &params)
	if err != nil {
		return err
	}

	return nil
}

// UpdateMetricsRedemption updates metrics for a redemption
func (r *MetricsDynamodb) UpdateMetricsRedemption(ctx context.Context, date string, weekday string, state model.State, division string, redemption entity.CouponCodeRedemption) error {
	updateKeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "METRICS#",
		SK: date,
	})
	if err != nil {
		return err
	}

	eav := make(map[string]types.AttributeValue)
	eav[":r"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(1)),
	}
	eav[":p"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(redemption.Cost)),
	}

	updateExpressionString := "ADD Redemptions :r, RedemptionsPoints :p, "

	for _, v := range Weekdays {
		if strings.EqualFold(weekday, v) {
			updateExpressionString = updateExpressionString + "Redemptions" + v + " :r, "
		}
	}

	for _, v := range Divisions {
		if strings.EqualFold(division, v) {
			updateExpressionString = updateExpressionString + "Redemptions" + v + " :r, "
		}
	}

	for _, v := range States {
		if strings.EqualFold(state.String(), v) {
			updateExpressionString = updateExpressionString + "Redemptions" + v + " :r, "
		}
	}

	for i := 1; i <= 7; i++ {
		if redemption.Tier == i {
			updateExpressionString = updateExpressionString + "RedemptionsTier" + strconv.Itoa(i) + " :r, "
		}
	}

	params := dynamodb.UpdateItemInput{
		Key:                       updateKeys,
		TableName:                 aws.String(r.table),
		ExpressionAttributeValues: eav,
		UpdateExpression:          aws.String(strings.TrimSuffix(updateExpressionString, ", ")),
	}

	_, err = r.db.UpdateItem(ctx, &params)
	if err != nil {
		return err
	}

	updateKeys, err = attributevalue.MarshalMap(&entity.SearchItem{
		PK: "METRICS#",
		SK: "TOTALS",
	})
	if err != nil {
		return err
	}

	eav = make(map[string]types.AttributeValue)
	eav[":r"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(1)),
	}
	eav[":p"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(redemption.Cost)),
	}

	updateExpressionString = "ADD Redemptions :r, RedemptionsPoints :p"

	params = dynamodb.UpdateItemInput{
		Key:                       updateKeys,
		TableName:                 aws.String(r.table),
		ExpressionAttributeValues: eav,
		UpdateExpression:          aws.String(updateExpressionString),
	}

	_, err = r.db.UpdateItem(ctx, &params)
	if err != nil {
		return err
	}

	return nil
}

// UpdateMetricsSweeps updates metrics for a redemption
func (r *MetricsDynamodb) UpdateMetricsSweeps(ctx context.Context, count int, date string, weekday string) error {
	updateKeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "METRICS#",
		SK: date,
	})
	if err != nil {
		return err
	}

	eav := make(map[string]types.AttributeValue)
	eav[":s"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(count)),
	}

	updateExpressionString := "ADD Sweeps :s"

	params := dynamodb.UpdateItemInput{
		Key:                       updateKeys,
		TableName:                 aws.String(r.table),
		ExpressionAttributeValues: eav,
		UpdateExpression:          aws.String(updateExpressionString),
	}

	_, err = r.db.UpdateItem(ctx, &params)
	if err != nil {
		return err
	}

	updateKeys, err = attributevalue.MarshalMap(&entity.SearchItem{
		PK: "METRICS#",
		SK: "TOTALS",
	})
	if err != nil {
		return err
	}

	params = dynamodb.UpdateItemInput{
		Key:                       updateKeys,
		TableName:                 aws.String(r.table),
		ExpressionAttributeValues: eav,
		UpdateExpression:          aws.String(updateExpressionString),
	}

	_, err = r.db.UpdateItem(ctx, &params)
	if err != nil {
		return err
	}

	return nil
}

// GetValidationsList returns the validations from DynamoDB
func (r *MetricsDynamodb) GetValidationsList(ctx context.Context) ([]*entity.MGValidationResponse, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String("VALIDATION"),
	}
	eav[":sk"] = &types.AttributeValueMemberS{
		Value: *aws.String("#"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk AND begins_with(SK, :sk)"),
		ExpressionAttributeValues: eav,
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.MGValidationResponse

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	return decoded, nil
}

func (r *MetricsDynamodb) runQuery(ctx context.Context, params *dynamodb.QueryInput, existingItems []map[string]types.AttributeValue) ([]map[string]types.AttributeValue, error) {
	output, err := r.db.Query(ctx, params)
	if err != nil {
		return nil, err
	}

	items := output.Items

	if len(existingItems) > 0 {
		copy(items, existingItems)
	}

	if params.Limit != nil && output.Count >= *params.Limit {
		return items, nil
	}

	if output.LastEvaluatedKey != nil {
		params.ExclusiveStartKey = output.LastEvaluatedKey
		return r.runQuery(ctx, params, items)
	}

	return items, nil
}
