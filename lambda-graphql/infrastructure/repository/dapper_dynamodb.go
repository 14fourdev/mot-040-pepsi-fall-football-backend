package repository

import (
	"context"
	"strconv"
	"strings"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
)

// DapperDynamodb DynamoDB repo
type DapperDynamodb struct {
	db    *dynamodb.Client
	table string
}

// NewAdminDynamodb create new repository
func NewDapperDynamodb(db *dynamodb.Client, table string) *DapperDynamodb {
	return &DapperDynamodb{
		db:    db,
		table: table,
	}
}

// Get
func (r *DapperDynamodb) Get(ctx context.Context, email string) (*entity.DapperEntry, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "DAPPER",
		SK: "#" + email,
	})

	if err != nil {
		return nil, entity.ErrInvalid
	}

	item := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	results, err := r.db.GetItem(ctx, item)
	if results.Item == nil || err != nil {
		return nil, entity.ErrNotFound
	}

	var decoded entity.DapperEntry

	err = attributevalue.UnmarshalMap(results.Item, &decoded)
	if err != nil {
		return nil, entity.ErrInvalid
	}

	return &decoded, nil
}

// GetAll
func (r *DapperDynamodb) GetAll(ctx context.Context) ([]*entity.DapperEntry, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String("DAPPER"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk"),
		ExpressionAttributeValues: eav,
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.DapperEntry

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	return decoded, nil
}

// Available
func (r *DapperDynamodb) Available(ctx context.Context) (*entity.DapperKits, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "DAPPERKITS",
		SK: "#DETAILS",
	})

	if err != nil {
		return nil, entity.ErrInvalid
	}

	item := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	zipItem, err := r.db.GetItem(ctx, item)
	if zipItem.Item == nil || err != nil {
		return nil, entity.ErrNotFound
	}

	var decoded entity.DapperKits

	err = attributevalue.UnmarshalMap(zipItem.Item, &decoded)
	if err != nil {
		return nil, entity.ErrInvalid
	}

	return &decoded, nil
}

// Enter the entry into DynamoDB
func (r *DapperDynamodb) Enter(ctx context.Context, dapper entity.DapperEntry) (*string, error) {
	var writeRequests []types.TransactWriteItem
	table := aws.String(r.table)

	// Create the dapper put
	av, err := attributevalue.MarshalMap(dapper)
	if err != nil {
		return nil, err
	}

	entryItem := types.TransactWriteItem{
		Put: &types.Put{
			Item:                av,
			TableName:           table,
			ConditionExpression: aws.String("attribute_not_exists(PK)"),
		},
	}

	writeRequests = append(writeRequests, entryItem)

	// Create the update for the kits
	Updatekeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "DAPPERKITS",
		SK: "#DETAILS",
	})
	if err != nil {
		return nil, err
	}

	ean := make(map[string]string)
	ean["#availF"] = "AvailableFull"

	if dapper.Size == "S" {
		ean["#avail"] = "AvailableS"
	} else if dapper.Size == "M" {
		ean["#avail"] = "AvailableM"
	} else if dapper.Size == "L" {
		ean["#avail"] = "AvailableL"
	} else if dapper.Size == "XL" {
		ean["#avail"] = "AvailableXL"
	} else if dapper.Size == "XXL" {
		ean["#avail"] = "AvailableXXL"
	}

	eav := make(map[string]types.AttributeValue)
	eav[":zero"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(0)),
	}
	eav[":minus"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(-1)),
	}

	requestItem := types.TransactWriteItem{
		Update: &types.Update{
			Key:                       Updatekeys,
			TableName:                 table,
			UpdateExpression:          aws.String("ADD #avail :minus, #availF :minus"),
			ExpressionAttributeNames:  ean,
			ExpressionAttributeValues: eav,
			ConditionExpression:       aws.String("#avail > :zero AND #availF > :zero"),
		},
	}

	writeRequests = append(writeRequests, requestItem)

	// Transact them to the DB
	items := &dynamodb.TransactWriteItemsInput{
		TransactItems: writeRequests,
	}

	_, err = r.db.TransactWriteItems(ctx, items)
	if err != nil {
		if strings.Contains(err.Error(), "[ConditionalCheckFailed, ") {
			return nil, entity.ErrWinRateLimit
		} else if strings.Contains(err.Error(), ", ConditionalCheckFailed]") {
			var writeRequests []types.TransactWriteItem = nil
			writeRequests = append(writeRequests, entryItem)

			ean2 := make(map[string]string)

			if dapper.Size == "S" {
				ean2["#avail"] = "AvailableS"
			} else if dapper.Size == "M" {
				ean2["#avail"] = "AvailableM"
			} else if dapper.Size == "L" {
				ean2["#avail"] = "AvailableL"
			} else if dapper.Size == "XL" {
				ean2["#avail"] = "AvailableXL"
			} else if dapper.Size == "XXL" {
				ean2["#avail"] = "AvailableXXL"
			}

			eav2 := make(map[string]types.AttributeValue)
			eav2[":zero"] = &types.AttributeValueMemberN{
				Value: *aws.String(strconv.Itoa(0)),
			}
			eav2[":minus"] = &types.AttributeValueMemberN{
				Value: *aws.String(strconv.Itoa(-1)),
			}

			requestItem := types.TransactWriteItem{
				Update: &types.Update{
					Key:                       Updatekeys,
					TableName:                 table,
					UpdateExpression:          aws.String("ADD #avail :minus"),
					ExpressionAttributeNames:  ean2,
					ExpressionAttributeValues: eav2,
					ConditionExpression:       aws.String("#avail > :zero"),
				},
			}

			writeRequests = append(writeRequests, requestItem)

			items := &dynamodb.TransactWriteItemsInput{
				TransactItems: writeRequests,
			}

			_, err = r.db.TransactWriteItems(ctx, items)
			if err != nil {
				if strings.Contains(err.Error(), "[ConditionalCheckFailed, ") {
					return nil, entity.ErrWinRateLimit
				} else if strings.Contains(err.Error(), ", ConditionalCheckFailed]") {
					return nil, entity.ErrRateLimit
				}

				return nil, err
			}

			prizeType := "Partial"
			return &prizeType, nil
		}

		return nil, err
	}

	prizeType := "Full"
	return &prizeType, nil
}

// Upsert the dapper into DynamoDB
func (r *DapperDynamodb) Upsert(ctx context.Context, dapper entity.DapperEntry) error {
	av, err := attributevalue.MarshalMap(dapper)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(r.table),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

func (r *DapperDynamodb) runQuery(ctx context.Context, params *dynamodb.QueryInput, existingItems []map[string]types.AttributeValue) ([]map[string]types.AttributeValue, error) {
	output, err := r.db.Query(ctx, params)
	if err != nil {
		return nil, err
	}

	items := output.Items

	if len(existingItems) > 0 {
		copy(items, existingItems)
	}

	if params.Limit != nil && output.Count >= *params.Limit {
		return items, nil
	}

	if output.LastEvaluatedKey != nil {
		params.ExclusiveStartKey = output.LastEvaluatedKey
		return r.runQuery(ctx, params, items)
	}

	return items, nil
}
