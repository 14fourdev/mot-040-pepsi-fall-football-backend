package repository

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
)

// AdminDynamodb DynamoDB repo
type AdminDynamodb struct {
	db    *dynamodb.Client
	table string
}

// NewAdminDynamodb create new repository
func NewAdminDynamodb(db *dynamodb.Client, table string) *AdminDynamodb {
	return &AdminDynamodb{
		db:    db,
		table: table,
	}
}

// GetByEmail returns the admin in DynamoDB
func (r *AdminDynamodb) GetByEmail(ctx context.Context, email string) (*entity.Admin, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "ADMIN#" + email,
		SK: "#PROFILE",
	})

	if err != nil {
		return nil, entity.ErrInvalid
	}

	item := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	consumerItem, err := r.db.GetItem(ctx, item)
	if consumerItem.Item == nil || err != nil {
		return nil, entity.ErrNotFound
	}

	var decoded entity.Admin

	err = attributevalue.UnmarshalMap(consumerItem.Item, &decoded)
	if err != nil {
		return nil, entity.ErrInvalid
	}

	return &decoded, nil
}

// Put stores admin in DynamoDB
func (r *AdminDynamodb) Put(ctx context.Context, admin entity.Admin) error {
	av, err := attributevalue.MarshalMap(admin)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:                av,
		TableName:           aws.String(r.table),
		ConditionExpression: aws.String("attribute_not_exists(PK)"),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}
