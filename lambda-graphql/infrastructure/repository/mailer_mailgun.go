package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/kr/pretty"
	"github.com/mailgun/mailgun-go/v4"
	"github.com/sirupsen/logrus"
)

const batchSize = 1000

// MailerMailgun logger repo
type MailerMailgun struct {
	client        *mailgun.MailgunImpl
	baseUrl       string
	fromName      string
	fromEmail     string
	validationKey string
}

// NewMailerMailgun create new repository
func NewMailerMailgun(mgDomain string, mgKey string, baseUrl string, fromName string, fromEmail string, validationKey string) *MailerMailgun {
	mg := mailgun.NewMailgun(mgDomain, mgKey)

	return &MailerMailgun{
		client:        mg,
		baseUrl:       baseUrl,
		fromName:      fromName,
		fromEmail:     fromEmail,
		validationKey: validationKey,
	}
}

// Send sends an email
func (r *MailerMailgun) Send(ctx context.Context, email string, template string, subject string, data map[string]interface{}) error {
	msg := r.newMessage(template, subject, data)
	msg.AddRecipientAndData(email, map[string]interface{}{})
	return r.send(ctx, msg)
}

// ValidateEmail
func (r *MailerMailgun) ValidateEmail(ctx context.Context, email string) (*entity.MGValidationResponse, error) {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", "https://api.mailgun.net/v4/address/validate", nil)
	req.SetBasicAuth("api", r.validationKey)
	param := req.URL.Query()
	param.Add("address", email)
	req.URL.RawQuery = param.Encode()
	resp, err := client.Do(req)

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.Error("Error reading body. ", err)
		return nil, err
	}

	logrus.Infoln("Response body.", string(body))
	vr := &entity.MGValidationResponse{}

	err = json.Unmarshal(body, vr)
	if err != nil {
		return nil, err
	}

	return vr, nil
}

func (r *MailerMailgun) newMessage(template, subject string, data map[string]interface{}) entity.Message {
	data["fqdn"] = r.baseUrl
	data["baseUrl"] = r.baseUrl
	return entity.Message{
		Type:      template,
		FromEmail: r.fromEmail,
		FromName:  r.fromName,
		Subject:   subject,
		Data:      data,
		UserData:  map[string]map[string]interface{}{},
	}
}

func min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}

func (r *MailerMailgun) send(ctx context.Context, msg entity.Message) error {
	return r.batchSend(ctx, msg)
}

func (r *MailerMailgun) batchSend(ctx context.Context, msg entity.Message) error {
	// In order to use our pool of workers we need to send
	// them work and collect their results. We make 2
	// channels for this.
	numJobs := int(float64(len(msg.UserData)/1000) + 1)
	numWorkers := int(math.Min(float64(numJobs), 3))
	jobs := make(chan entity.Message, numJobs)
	results := make(chan bool, numJobs)

	logrus.Println("batch send start")

	// This starts up 3 workers, initially blocked
	// because there are no jobs yet.
	for w := 1; w <= numWorkers; w++ {
		go r.worker(ctx, jobs, results)
	}

	logrus.Println("batch send workers: " + strconv.Itoa(numWorkers))

	recipients := msg.UserData
	keys := []string{}
	for k := range msg.UserData {
		keys = append(keys, k)
	}

	logrus.Println(recipients)

	// Here we send 5 `jobs` and then `close` that
	// channel to indicate that's all the work we have.
	for i := 0; i < len(keys); i += batchSize {
		msg.UserData = map[string]map[string]interface{}{}

		k := keys[i:min(i+batchSize, len(keys))]
		for _, n := range k {
			msg.UserData[n] = recipients[n]
		}

		jobs <- msg
	}
	close(jobs)

	logrus.Println("batch send jobs closed")

	// Finally we collect all the results of the work.
	// This also ensures that the worker goroutines have
	// finished. An alternative way to wait for multiple
	// goroutines is to use a [WaitGroup](waitgroups).
	for a := 1; a <= numJobs; a++ {
		e := <-results
		if !e {
			pretty.Println("there was an error")
		} else {
			pretty.Println("no error")
		}
	}

	return nil
}

func (r *MailerMailgun) worker(ctx context.Context, jobs <-chan entity.Message, result chan<- bool) {
	for j := range jobs {
		logrus.Println("batch send job")

		from := j.FromName + "<" + j.FromEmail + ">"
		msg := r.client.NewMessage(from, j.Subject, "")

		if !j.SendTime.IsZero() {
			msg.SetDeliveryTime(j.SendTime)
		}

		msg.SetTemplate(j.Type)

		keys := map[string]string{}

		for email, vars := range j.UserData {
			if len(keys) == 0 {
				for k := range vars {
					keys[k] = fmt.Sprintf("%srecipient.%s%s", "%", k, "%")
				}
			}

			err := msg.AddRecipientAndVariables(email, vars)
			if err != nil {
				logrus.Errorln(err)
			}

			logrus.Println("added recipient: " + email)
		}

		for k, v := range j.Data {
			msg.AddVariable(k, v)
		}

		for k, v := range keys {
			msg.AddVariable(k, v)
		}

		logrus.Println("batch final message")
		logrus.Println(msg)

		resp, id, err := r.client.Send(ctx, msg)

		if err != nil {
			logrus.Errorln(err)
			result <- false
		} else {
			logrus.Printf("ID: %s Resp: %s\n", id, resp)
			result <- true
		}
		time.Sleep(1 * time.Second)
	}
}
