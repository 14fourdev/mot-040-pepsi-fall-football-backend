package repository

import (
	"time"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/sirupsen/logrus"
)

// RecaptchaLogger logger repo
type RecaptchaLogger struct {
	secret string
}

// NewRecaptchaLogger create new repository
func NewRecaptchaLogger(secret string) *RecaptchaLogger {
	return &RecaptchaLogger{
		secret: secret,
	}
}

// Put stores the requester in DynamoDB
func (r *RecaptchaLogger) Confirm(ip string, response string) (*entity.RecaptchaResponse, error) {
	logrus.Println("Recaptcha Logger: ", ip, response)
	return &entity.RecaptchaResponse{
		Success:     true,
		Score:       1,
		Action:      "logger",
		ChallengeTS: time.Now(),
		Hostname:    "logger hostname",
	}, nil
}
