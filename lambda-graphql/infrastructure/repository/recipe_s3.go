package repository

import (
	"bytes"
	"context"
	"strings"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"
)

// RecipeS3 S3 repo
type RecipeS3 struct {
	s3      *s3.Client
	bucket  string
	baseUrl string
}

// NewRecipeS3 create new repository
func NewRecipeS3(s3 *s3.Client, bucket string, baseUrl string) *RecipeS3 {
	return &RecipeS3{
		s3:      s3,
		bucket:  bucket,
		baseUrl: baseUrl,
	}
}

// PutImage
func (r *RecipeS3) PutImage(ctx context.Context, filename string, file []byte, contentType string) (*string, error) {
	reader := bytes.NewReader(file)

	params := &s3.PutObjectInput{
		Bucket:      &r.bucket,
		Key:         aws.String(filename),
		ACL:         types.ObjectCannedACLPublicRead,
		Body:        reader,
		ContentType: &contentType,
	}

	_, err := r.s3.PutObject(ctx, params)
	if err != nil {
		return nil, err
	}

	baseUrl := r.baseUrl
	if !strings.HasSuffix(baseUrl, "/") {
		baseUrl = baseUrl + "/"
	}

	fileUrl := baseUrl + filename

	return &fileUrl, nil
}
