package repository

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/sirupsen/logrus"
)

// RecaptchaGoogle logger repo
type RecaptchaGoogle struct {
	secret string
}

// NewRecaptchaGoogle create new repository
func NewRecaptchaGoogle(secret string) *RecaptchaGoogle {
	return &RecaptchaGoogle{
		secret: secret,
	}
}

// RecaptchaGoogle runs the recaptcha response through Google's API
func (r *RecaptchaGoogle) Confirm(ip string, captcha string) (*entity.RecaptchaResponse, error) {
	resp, err := r.request(ip, captcha)
	if err != nil {
		logrus.Println("Captcha Error: ", err)
		return nil, err
	}

	logrus.Println("Captcha IP: ", ip)
	logrus.Println("Captcha Response: ", resp)

	if !resp.Success {
		logrus.Println("Captcha Failed: ", ip, captcha)
		return &resp, entity.ErrInvalidCaptcha
	}

	return &resp, nil
}

func (r *RecaptchaGoogle) request(ip string, captcha string) (response entity.RecaptchaResponse, err error) {
	resp, err := http.PostForm("https://www.google.com/recaptcha/api/siteverify",
		url.Values{"secret": {r.secret}, "remoteip": {ip}, "response": {captcha}})
	if err != nil {
		logrus.Printf("Post error: %s\n", err)
		return
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		logrus.Println("Read error: could not read body: %s", err)
		return
	}
	err = json.Unmarshal(body, &response)
	if err != nil {
		logrus.Println("Read error: got invalid JSON: %s", err)
		return
	}
	return
}
