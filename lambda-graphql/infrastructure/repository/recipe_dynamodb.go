package repository

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/graph/model"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
)

// RecipeDynamodb DynamoDB repo
type RecipeDynamodb struct {
	db    *dynamodb.Client
	table string
}

// NewAdminDynamodb create new repository
func NewRecipeDynamodb(db *dynamodb.Client, table string) *RecipeDynamodb {
	return &RecipeDynamodb{
		db:    db,
		table: table,
	}
}

// Get
func (r *RecipeDynamodb) Get(ctx context.Context) ([]*entity.Recipe, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String("RECIPE"),
	}
	eav[":sk"] = &types.AttributeValueMemberS{
		Value: *aws.String("#"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk and begins_with(SK, :sk)"),
		ExpressionAttributeValues: eav,
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.Recipe

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	return decoded, nil
}

// GetAllFeaturedRecipes
func (r *RecipeDynamodb) GetAllFeaturedRecipes(ctx context.Context) ([]*entity.FeaturedRecipeWeekly, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String("FEATUREDRECIPE"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk"),
		ExpressionAttributeValues: eav,
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.FeaturedRecipeWeekly

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	return decoded, nil
}

// GetFeaturedRecipe
func (r *RecipeDynamodb) GetFeaturedRecipe(ctx context.Context, date string, team model.EntryTeam) (*entity.FeaturedRecipeWeekly, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "FEATUREDRECIPE",
		SK: "#" + team.String() + "#" + date,
	})

	if err != nil {
		return nil, entity.ErrInvalid
	}

	params := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	item, err := r.db.GetItem(ctx, params)
	if item.Item == nil || err != nil {
		return nil, entity.ErrNotFound
	}

	var decoded entity.FeaturedRecipeWeekly

	err = attributevalue.UnmarshalMap(item.Item, &decoded)
	if err != nil {
		return nil, entity.ErrInvalid
	}

	return &decoded, nil
}

// GetAllFSB
func (r *RecipeDynamodb) GetAllFSB(ctx context.Context) ([]*entity.FoodServiceBanner, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String("FSB"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk"),
		ExpressionAttributeValues: eav,
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.FoodServiceBanner

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	return decoded, nil
}

// GetFSB
func (r *RecipeDynamodb) GetFSB(ctx context.Context, team model.EntryTeam) (*entity.FoodServiceBanner, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "FSB",
		SK: "#" + team.String(),
	})

	if err != nil {
		return nil, entity.ErrInvalid
	}

	params := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	item, err := r.db.GetItem(ctx, params)
	if item.Item == nil || err != nil {
		return nil, entity.ErrNotFound
	}

	var decoded entity.FoodServiceBanner

	err = attributevalue.UnmarshalMap(item.Item, &decoded)
	if err != nil {
		return nil, entity.ErrInvalid
	}

	return &decoded, nil
}

// GetAllARM
func (r *RecipeDynamodb) GetAllARM(ctx context.Context) ([]*entity.ARModule, error) {
	eav := make(map[string]types.AttributeValue)
	eav[":pk"] = &types.AttributeValueMemberS{
		Value: *aws.String("ARM"),
	}

	params := &dynamodb.QueryInput{
		TableName:                 aws.String(r.table),
		KeyConditionExpression:    aws.String("PK = :pk"),
		ExpressionAttributeValues: eav,
	}

	items, err := r.runQuery(ctx, params, nil)
	if err != nil {
		return nil, err
	}

	var decoded []*entity.ARModule

	err = attributevalue.UnmarshalListOfMaps(items, &decoded)
	if err != nil {
		return nil, err
	}

	return decoded, nil
}

// GetARM
func (r *RecipeDynamodb) GetARM(ctx context.Context, team model.EntryTeam, week string) (*entity.ARModule, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "ARM",
		SK: "#" + team.String() + "#" + week,
	})

	if err != nil {
		return nil, entity.ErrInvalid
	}

	params := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	item, err := r.db.GetItem(ctx, params)
	if item.Item == nil || err != nil {
		return nil, entity.ErrNotFound
	}

	var decoded entity.ARModule

	err = attributevalue.UnmarshalMap(item.Item, &decoded)
	if err != nil {
		return nil, entity.ErrInvalid
	}

	return &decoded, nil
}

// UpsertFeaturedRecipe
func (r *RecipeDynamodb) UpsertFeaturedRecipe(ctx context.Context, fr entity.FeaturedRecipeWeekly) error {
	av, err := attributevalue.MarshalMap(fr)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(r.table),
		// ConditionExpression: aws.String("attribute_not_exists(PK)"),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// InsertFeaturedRecipe
func (r *RecipeDynamodb) InsertFeaturedRecipe(ctx context.Context, fr entity.FeaturedRecipeWeekly) error {
	av, err := attributevalue.MarshalMap(fr)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:                av,
		TableName:           aws.String(r.table),
		ConditionExpression: aws.String("attribute_not_exists(PK)"),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// UpsertFSB
func (r *RecipeDynamodb) UpsertFSB(ctx context.Context, fsb entity.FoodServiceBanner) error {
	av, err := attributevalue.MarshalMap(fsb)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(r.table),
		// ConditionExpression: aws.String("attribute_not_exists(PK)"),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// InsertFSB
func (r *RecipeDynamodb) InsertFSB(ctx context.Context, fsb entity.FoodServiceBanner) error {
	av, err := attributevalue.MarshalMap(fsb)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:                av,
		TableName:           aws.String(r.table),
		ConditionExpression: aws.String("attribute_not_exists(PK)"),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// UpsertARM
func (r *RecipeDynamodb) UpsertARM(ctx context.Context, arm entity.ARModule) error {
	av, err := attributevalue.MarshalMap(arm)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(r.table),
		// ConditionExpression: aws.String("attribute_not_exists(PK)"),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// InsertARM
func (r *RecipeDynamodb) InsertARM(ctx context.Context, arm entity.ARModule) error {
	av, err := attributevalue.MarshalMap(arm)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:                av,
		TableName:           aws.String(r.table),
		ConditionExpression: aws.String("attribute_not_exists(PK)"),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// Upsert
func (r *RecipeDynamodb) Upsert(ctx context.Context, recipe entity.Recipe) error {
	av, err := attributevalue.MarshalMap(recipe)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(r.table),
		// ConditionExpression: aws.String("attribute_not_exists(PK)"),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// Delete
func (r *RecipeDynamodb) Delete(ctx context.Context, slug string) error {
	// Create the coupon delete
	deleteKeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "RECIPE",
		SK: "#" + slug,
	})
	if err != nil {
		return err
	}

	item := &dynamodb.DeleteItemInput{
		Key:       deleteKeys,
		TableName: aws.String(r.table),
	}

	_, err = r.db.DeleteItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// DeleteFSB
func (r *RecipeDynamodb) DeleteFSB(ctx context.Context, team model.EntryTeam) error {
	// Create the coupon delete
	deleteKeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "FSB",
		SK: "#" + team.String(),
	})
	if err != nil {
		return err
	}

	item := &dynamodb.DeleteItemInput{
		Key:       deleteKeys,
		TableName: aws.String(r.table),
	}

	_, err = r.db.DeleteItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// DeleteARM
func (r *RecipeDynamodb) DeleteARM(ctx context.Context, team model.EntryTeam, week string) error {
	// Create the coupon delete
	deleteKeys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "ARM",
		SK: "#" + team.String() + "#" + week,
	})
	if err != nil {
		return err
	}

	item := &dynamodb.DeleteItemInput{
		Key:       deleteKeys,
		TableName: aws.String(r.table),
	}

	_, err = r.db.DeleteItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

func (r *RecipeDynamodb) runQuery(ctx context.Context, params *dynamodb.QueryInput, existingItems []map[string]types.AttributeValue) ([]map[string]types.AttributeValue, error) {
	output, err := r.db.Query(ctx, params)
	if err != nil {
		return nil, err
	}

	items := output.Items

	if len(existingItems) > 0 {
		copy(items, existingItems)
	}

	if output.LastEvaluatedKey != nil {
		params.ExclusiveStartKey = output.LastEvaluatedKey
		return r.runQuery(ctx, params, items)
	}

	return items, nil
}
