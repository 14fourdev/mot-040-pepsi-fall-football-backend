package repository

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/sirupsen/logrus"
)

// AuthDynamodb DynamoDB repo
type AuthDynamodb struct {
	db    *dynamodb.Client
	table string
}

// NewAuthDynamodb create new repository
func NewAuthDynamodb(db *dynamodb.Client, table string) *AuthDynamodb {
	return &AuthDynamodb{
		db:    db,
		table: table,
	}
}

// PutResetToken returns the admin in DynamoDB
func (r *AuthDynamodb) PutResetToken(ctx context.Context, reset entity.Reset) error {
	av, err := attributevalue.MarshalMap(reset)

	if err != nil {
		return entity.ErrInvalid
	}

	item := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(r.table),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// GetResetToken returns the reset by the token from DynamoDB
func (r *AuthDynamodb) GetResetByToken(ctx context.Context, token string, role string) (entity.Reset, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "RESET#" + token,
		SK: "#ROLE#" + role,
	})

	if err != nil {
		return entity.Reset{}, entity.ErrInvalid
	}

	item := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	reset, err := r.db.GetItem(ctx, item)
	if len(reset.Item) == 0 || err != nil {
		logrus.Println(item)
		return entity.Reset{}, entity.ErrNotFound
	}

	var decoded entity.Reset

	err = attributevalue.UnmarshalMap(reset.Item, &decoded)
	if err != nil {
		return entity.Reset{}, entity.ErrInvalid
	}

	return decoded, nil
}
