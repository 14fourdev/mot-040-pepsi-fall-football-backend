package repository

import (
	"context"
	"strconv"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
)

// BanlistDynamodb DynamoDB repo
type BanlistDynamodb struct {
	db    *dynamodb.Client
	table string
}

// NewBanlistDynamodb create new repository
func NewBanlistDynamodb(db *dynamodb.Client, table string) *BanlistDynamodb {
	return &BanlistDynamodb{
		db:    db,
		table: table,
	}
}

// Put stores the requester in DynamoDB
func (r *BanlistDynamodb) Update(ctx context.Context, bantype string, banobject string) error {
	keys, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "BANNED#" + bantype,
		SK: banobject,
	})
	if err != nil {
		return err
	}

	eav := make(map[string]types.AttributeValue)
	eav[":bc"] = &types.AttributeValueMemberN{
		Value: *aws.String(strconv.Itoa(1)),
	}

	item := &dynamodb.UpdateItemInput{
		Key:                       keys,
		TableName:                 aws.String(r.table),
		UpdateExpression:          aws.String("ADD BlockCount :bc"),
		ExpressionAttributeValues: eav,
	}

	_, err = r.db.UpdateItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}
