package repository

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/sirupsen/logrus"
)

// MailerLogger logger repo
type MailerLogger struct {
	baseUrl string
}

// NewMailerLogger create new repository
func NewMailerLogger(baseUrl string) *MailerLogger {
	return &MailerLogger{
		baseUrl: baseUrl,
	}
}

// Send
func (r *MailerLogger) Send(ctx context.Context, email string, template string, subject string, data map[string]interface{}) error {
	logrus.Println("MailerLogger Send Email: ", email)
	logrus.Println("MailerLogger Send Template: ", template)
	logrus.Println("MailerLogger Send Subject: ", subject)
	logrus.Println("MailerLogger Send Data: ", data)
	return nil
}

// ValidateEmail
func (r *MailerLogger) ValidateEmail(ctx context.Context, email string) (*entity.MGValidationResponse, error) {
	logrus.Println("MailerLogger Validate Email: ", email)
	return &entity.MGValidationResponse{
		PK:            "PK",
		SK:            "SK",
		Address:       email,
		IsDisposable:  false,
		IsRoleAddress: false,
		Reason:        []string{"reason"},
		Result:        "result",
		Risk:          "low",
	}, nil
}
