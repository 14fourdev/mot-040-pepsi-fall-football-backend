package repository

import (
	"context"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
)

// ConsumerDynamodb DynamoDB repo
type ConsumerDynamodb struct {
	db    *dynamodb.Client
	table string
}

// NewConsumerDynamodb create new repository
func NewConsumerDynamodb(db *dynamodb.Client, table string) *ConsumerDynamodb {
	return &ConsumerDynamodb{
		db:    db,
		table: table,
	}
}

// GetZip returns the consumer in DynamoDB
func (r *ConsumerDynamodb) GetZip(ctx context.Context, zip string) (*entity.Zipcode, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "ZIP#" + zip,
		SK: "#DETAILS",
	})

	if err != nil {
		return &entity.Zipcode{}, entity.ErrInvalid
	}

	item := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	zipItem, err := r.db.GetItem(ctx, item)
	if zipItem.Item == nil || err != nil {
		return &entity.Zipcode{}, entity.ErrNotFound
	}

	var decoded entity.Zipcode

	err = attributevalue.UnmarshalMap(zipItem.Item, &decoded)
	if err != nil {
		return &entity.Zipcode{}, entity.ErrInvalid
	}

	return &decoded, nil
}

// GetByEmail returns the consumer in DynamoDB
func (r *ConsumerDynamodb) GetByEmail(ctx context.Context, email string) (*entity.Consumer, error) {
	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "CONSUMER#" + email,
		SK: "#PROFILE",
	})

	if err != nil {
		return &entity.Consumer{}, entity.ErrInvalid
	}

	item := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(r.table),
	}

	consumerItem, err := r.db.GetItem(ctx, item)
	if consumerItem.Item == nil || err != nil {
		return &entity.Consumer{}, entity.ErrNotFound
	}

	var decoded entity.Consumer

	err = attributevalue.UnmarshalMap(consumerItem.Item, &decoded)
	if err != nil {
		return &entity.Consumer{}, entity.ErrInvalid
	}

	return &decoded, nil
}

// Put stores consumer in DynamoDB if they don't already exist
func (r *ConsumerDynamodb) Put(ctx context.Context, consumer entity.Consumer) error {
	av, err := attributevalue.MarshalMap(consumer)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:                av,
		TableName:           aws.String(r.table),
		ConditionExpression: aws.String("attribute_not_exists(PK)"),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}

// Upsert the consumer into DynamoDB
func (r *ConsumerDynamodb) Upsert(ctx context.Context, consumer entity.Consumer) error {
	av, err := attributevalue.MarshalMap(consumer)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(r.table),
	}

	_, err = r.db.PutItem(ctx, item)
	if err != nil {
		return err
	}

	return nil
}
