.PHONY: build

build:
	sam build

deploy:
	sam deploy --stack-name=mot-040-pepsi-fall-football-backend-dev --config-file=samconfig-dev.toml --profile=mot-040-pepsi-fall-football

deploy-nonp:
	sam deploy --stack-name=mot-040-pepsi-fall-football-backend-nonp --config-file=samconfig-nonp.toml --profile=mot-040-pepsi-fall-football

deploy-prod:
	sam deploy --stack-name=mot-040-pepsi-fall-football-backend-prod --config-file=samconfig-prod.toml --profile=mot-040-pepsi-fall-football

bd:
	make build && make deploy

bd-nonp:
	make build && make deploy-nonp

bd-prod:
	make build && make deploy-prod

start:
	make build && sam local start-api --docker-network local

get-domain-banlist:
	wget -O lambda-graphql/usecase/banlist/resources/domain-banlist.json https://raw.githubusercontent.com/ivolo/disposable-email-domains/master/wildcard.json

get-ip-banlist:
	wget -O lambda-graphql/usecase/banlist/resources/ip-banlist.txt https://raw.githubusercontent.com/stamparm/ipsum/master/levels/2.txt