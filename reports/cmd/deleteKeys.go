/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"errors"
	"fmt"
	"reports/infrastructure"
	"strconv"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/spf13/cobra"
)

// deleteKeysCmd represents the deleteKeys command
var deleteKeysCmd = &cobra.Command{
	Use:   "deleteKeys",
	Short: "Delete DynamoDB items by count and Partition Key",
	Run: func(cmd *cobra.Command, args []string) {
		var writeRequests []types.WriteRequest

		fmt.Println("deleteKeys called")
		ctx := context.TODO()

		count, err := cmd.Flags().GetInt32("count")
		if err != nil {
			panic(err)
		}

		if count > 1000 {
			panic(errors.New("maximum count is 1000"))
		}

		if count == 0 {
			panic(errors.New("count is required"))
		}

		partitionKey, err := cmd.Flags().GetString("partitionKey")
		if err != nil {
			panic(err)
		}

		if partitionKey == "" {
			panic(errors.New("partitionKey is required"))
		}

		sortKey, err := cmd.Flags().GetString("sortKey")
		if err != nil {
			panic(err)
		}

		if sortKey == "" {
			panic(errors.New("sortKey is required"))
		}

		region, err := cmd.Flags().GetString("region")
		if err != nil {
			panic(err)
		}

		table, err := cmd.Flags().GetString("table")
		if err != nil {
			panic(err)
		}

		dynamodbClient = infrastructure.NewDynamodbClient(ctx, region)

		eav := make(map[string]types.AttributeValue)
		eav[":pk"] = &types.AttributeValueMemberS{
			Value: *aws.String(partitionKey),
		}
		eav[":sk"] = &types.AttributeValueMemberS{
			Value: *aws.String(sortKey),
		}

		params := &dynamodb.QueryInput{
			TableName:                 aws.String(table),
			KeyConditionExpression:    aws.String("PK = :pk AND begins_with(SK, :sk)"),
			ExpressionAttributeValues: eav,
			Limit:                     &count,
		}

		items, err := runQuery(ctx, dynamodbClient, params, nil)
		if err != nil {
			panic(err)
		}

		var decoded []*entity.CouponCode

		err = attributevalue.UnmarshalListOfMaps(items, &decoded)
		if err != nil {
			panic(err)
		}

		totalDeleted := 0

		fmt.Println("Found " + strconv.Itoa(len(decoded)) + " Items")

		for index, item := range decoded {
			totalDeleted += 1

			av, err := attributevalue.MarshalMap(entity.SearchItem{
				PK: item.PK,
				SK: item.SK,
			})
			if err != nil {
				panic(err)
			}

			requestItem := types.WriteRequest{
				DeleteRequest: &types.DeleteRequest{
					Key: av,
				},
			}

			writeRequests = append(writeRequests, requestItem)
			nonZeroIndex := index + 1

			if nonZeroIndex == int(count) || nonZeroIndex%25 == 0 {
				requestItems := map[string][]types.WriteRequest{}
				requestItems[table] = writeRequests
				items := &dynamodb.BatchWriteItemInput{
					RequestItems: requestItems,
				}

				_, err := dynamodbClient.BatchWriteItem(ctx, items)
				if err != nil {
					panic(err)
				}
				writeRequests = []types.WriteRequest{}
				fmt.Println("Deleted Count: " + strconv.Itoa(totalDeleted))
			}
		}

		fmt.Println("Deleted Records")
	},
}

func init() {
	rootCmd.AddCommand(deleteKeysCmd)
	deleteKeysCmd.PersistentFlags().StringP("region", "r", "us-east-1", "AWS Region to use")
	deleteKeysCmd.PersistentFlags().StringP("table", "t", "prod-application", "DynamoDB table to use")
	deleteKeysCmd.PersistentFlags().StringP("partitionKey", "p", "", "Partition Key to delete")
	deleteKeysCmd.PersistentFlags().StringP("sortKey", "s", "", "Sort Key to delete")
	deleteKeysCmd.PersistentFlags().Int32P("count", "c", 0, "Count of items to delete")
}
