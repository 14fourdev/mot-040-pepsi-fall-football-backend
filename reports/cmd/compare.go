package cmd

import (
	"context"
	"fmt"
	"reports/infrastructure"
	"strconv"

	"bitbucket.org/14fourdev/mot-040-pepsi-fall-football-backend/entity"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/spf13/cobra"
)

var dynamodbClient *dynamodb.Client

// compareCmd represents the compare command
var compareCmd = &cobra.Command{
	Use:   "compare",
	Short: "Compare Dapper Entries",
	RunE: func(cmd *cobra.Command, args []string) error {
		fmt.Println("compare called")
		ctx := context.TODO()

		region, err := cmd.Flags().GetString("region")
		if err != nil {
			return err
		}

		table, err := cmd.Flags().GetString("table")
		if err != nil {
			return err
		}

		dynamodbClient = infrastructure.NewDynamodbClient(ctx, region)

		eav := make(map[string]types.AttributeValue)
		eav[":pk"] = &types.AttributeValueMemberS{
			Value: *aws.String("DAPPER"),
		}

		params := &dynamodb.QueryInput{
			TableName:                 &table,
			KeyConditionExpression:    aws.String("PK = :pk"),
			ExpressionAttributeValues: eav,
			Limit:                     aws.Int32(500),
		}

		items, err := runQuery(ctx, dynamodbClient, params, nil)
		if err != nil {
			return err
		}

		var decoded []*entity.DapperEntry

		err = attributevalue.UnmarshalListOfMaps(items, &decoded)
		if err != nil {
			return err
		}

		totalMatched := 0
		lastIndex := 0

		for index, i := range decoded {
			matched := getConsumer(ctx, dynamodbClient, table, i.Email)
			if matched {
				totalMatched += 1
			}

			lastIndex = index + 1
		}

		fmt.Println("Found matches: " + strconv.Itoa(totalMatched) + "/" + strconv.Itoa(lastIndex))

		return nil
	},
}

func init() {
	rootCmd.AddCommand(compareCmd)
	compareCmd.PersistentFlags().StringP("region", "r", "us-east-1", "AWS Region to use")
	compareCmd.PersistentFlags().StringP("table", "t", "prod-application", "DynamoDB table to use")
}

func getConsumer(ctx context.Context, client *dynamodb.Client, table string, email string) bool {
	fmt.Println("Getting Consumer: " + email)

	av, err := attributevalue.MarshalMap(&entity.SearchItem{
		PK: "CONSUMER#" + email,
		SK: "#PROFILE",
	})

	if err != nil {
		return false
	}

	item := &dynamodb.GetItemInput{
		Key:       av,
		TableName: aws.String(table),
	}

	consumerItem, err := client.GetItem(ctx, item)
	if consumerItem.Item == nil || err != nil {
		return false
	}

	var decoded entity.Consumer

	err = attributevalue.UnmarshalMap(consumerItem.Item, &decoded)
	return err == nil
}
