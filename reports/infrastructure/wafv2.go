package infrastructure

import (
	"context"
	"log"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/wafv2"
)

// NewWAFClient creeates a new DynamoDB client based on the environment
func NewWAFClient(ctx context.Context, env map[string]string) *wafv2.Client {
	cfg, err := config.LoadDefaultConfig(ctx,
		config.WithRegion(env["AWS_REGION"]),
	)
	if err != nil {
		log.Fatalf("unable to load SDK config, %v", err)
	}

	// Using the Config value, create the DynamoDB client
	client := wafv2.NewFromConfig(cfg)
	return client
}
