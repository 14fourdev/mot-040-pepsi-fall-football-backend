package infrastructure

import (
	"context"
	"log"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

// NewS3Client creeates a new DynamoDB client based on the environment
func NewS3Client(ctx context.Context, env map[string]string) *s3.Client {
	cfg, err := config.LoadDefaultConfig(ctx,
		config.WithRegion(env["AWS_REGION"]),
	)
	if err != nil {
		log.Fatalf("unable to load SDK config, %v", err)
	}

	// Using the Config value, create the DynamoDB client
	client := s3.NewFromConfig(cfg)
	return client
}
